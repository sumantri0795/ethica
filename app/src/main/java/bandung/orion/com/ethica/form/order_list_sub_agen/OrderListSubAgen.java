package bandung.orion.com.ethica.form.order_list_sub_agen;

import android.app.Application;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Filter;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.alamat_kirim.InputAlamatKirimCartActivity;
import bandung.orion.com.ethica.form.cart.HistoriAlamatKirim;
import bandung.orion.com.ethica.form.filter_customer.FilterCustomer;
import bandung.orion.com.ethica.form.order_list.OrderListFragment;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.EndOfTheMonthLong;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getSimpleDate;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getTglFormat;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getTglFormatMySql;
import static bandung.orion.com.ethica.utility.FungsiGeneral.hideSoftKeyboard;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowLong;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowStartOfTheMonthLong;
import static bandung.orion.com.ethica.utility.JConst.MODE_FILTER_CUSTOMER_CART;
import static bandung.orion.com.ethica.utility.JConst.MODE_FILTER_CUSTOMER_ORDER;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_APPROVE;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_APPROVE_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_PROSES;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_PROSES_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIHAPUS;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIHAPUS_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIPROSES;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIPROSES_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_KEEP;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_KEEP_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_SELESAI;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_SELESAI_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_TOLAK;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_TOLAK_TEXT;

public class OrderListSubAgen extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener{
    private TextInputEditText txtTglDari, txtTglSampai, txtSubAgen;
    public RecyclerView rcvData;
    private SwipeRefreshLayout swipe;
    private SearchView txtSearch;
    private OrderListSubAgenAdapter adapter;
    private Spinner spStatus;
    private ArrayList<String> ListStatus;
    public List<OrderListSubAgenModel> ListItems = new ArrayList<>();
    private ILoadMore loadMore;
    private String status;
    int lastVisibleItem, totalItemCount;
    boolean IsLoading;
    private ArrayAdapter<String> adapterStatus;
    private boolean isStart;

    private AppBarLayout appbarLayout;
    private int mMaxScrollSize;


    private LinearLayoutManager linearLayoutManager;
    int visibleThreshold = 4;
    private Long tgl_dari, tgl_Sampai;
    private long seqCustomer = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list_sub_agen);
        CreateView();
        InitClass();
        EventClass();
    }

    private void CreateView(){
        this.appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        this.rcvData = (RecyclerView) findViewById(R.id.RcvData);
        this.swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        txtTglDari   = (TextInputEditText) findViewById(R.id.txtTglDari);
        txtTglSampai = (TextInputEditText) findViewById(R.id.txtTglSampai);
        txtSubAgen   = (TextInputEditText) findViewById(R.id.txtSubAgen);

        //this.txtSearch = (SearchView) findViewById(R.id.txtSearch);
        this.adapter = new OrderListSubAgenAdapter(OrderListSubAgen.this, ListItems, OrderListSubAgen.this);
        rcvData.setAdapter(adapter);
        spStatus = (Spinner) findViewById(R.id.spStatus);

    }

    private void InitClass(){
        rcvData.setLayoutManager(new LinearLayoutManager(OrderListSubAgen.this));
        linearLayoutManager = (LinearLayoutManager)rcvData.getLayoutManager();
        this.adapter.removeAllModel();

        adapter.notifyDataSetChanged();
        status = "";
        ListStatus = new ArrayList<>();

        ListStatus.add("Semua");
        ListStatus.add(STATUS_ORDER_KEEP_TEXT);
        ListStatus.add(STATUS_ORDER_BELUM_APPROVE_TEXT);
        ListStatus.add(STATUS_ORDER_BELUM_PROSES_TEXT);
        ListStatus.add(STATUS_ORDER_DIPROSES_TEXT);
        ListStatus.add(STATUS_ORDER_TOLAK_TEXT);
        ListStatus.add(STATUS_ORDER_DIHAPUS_TEXT);
        ListStatus.add(STATUS_ORDER_SELESAI_TEXT);

        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        adapterStatus = new ArrayAdapter<String>(OrderListSubAgen.this, android.R.layout.simple_spinner_item, ListStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(adapterStatus);
        spStatus.setSelection(2);
        swipe.setRefreshing(false);

        tgl_dari   = serverNowStartOfTheMonthLong();
        tgl_Sampai = EndOfTheMonthLong(serverNowLong());

        txtTglDari.setText(getTglFormat(serverNowStartOfTheMonthLong()));
        txtTglSampai.setText(getTglFormat(serverNowLong()));
    }

    protected void EventClass(){
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                rcvData.scrollToPosition(0);
                adapter.removeAllModel();
                RefreshRecyclerView();
                swipe.setRefreshing(false);
            }
        });

        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0){
                    OrderListSubAgen.this.status = "";
                }else if (i == 1){
                    OrderListSubAgen.this.status = STATUS_ORDER_KEEP;
                }else if (i == 2){
                    OrderListSubAgen.this.status = STATUS_ORDER_BELUM_APPROVE;
                }else if (i == 3){
                    OrderListSubAgen.this.status = STATUS_ORDER_BELUM_PROSES;
                }else if (i == 4){
                    OrderListSubAgen.this.status = STATUS_ORDER_DIPROSES;
                }else if (i == 5){
                    OrderListSubAgen.this.status = STATUS_ORDER_TOLAK;
                }else if (i == 6){
                    OrderListSubAgen.this.status = STATUS_ORDER_DIHAPUS;
                }else if (i == 7){
                    OrderListSubAgen.this.status = STATUS_ORDER_SELESAI;
                }

                if (isStart) {
                    rcvData.scrollToPosition(0);
                    adapter.removeAllModel();
                    RefreshRecyclerView();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                adapter.addModel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.removeMoel(ListItems.size()-1);
                        load();
                    }
                },1000);

            }
        });

        rcvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

//        txtSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                adapter.removeAllModel();
//                RefreshRecyclerView();
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//        });

//        txtSearch.setOnCloseListener(new SearchView.OnCloseListener() {
//            @Override
//            public boolean onClose() {
//                adapter.removeAllModel();
//                RefreshRecyclerView();
//                return false;
//            }
//        });

        txtTglDari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(OrderListSubAgen.this, OrderListSubAgen.this.findViewById(android.R.id.content));
                if (txtTglDari.getText().toString().equals("")){
                    txtTglDari.setText(FungsiGeneral.serverNowFormated());
                }
                Long tgl = FungsiGeneral.getMillisDate(txtTglDari.getText().toString());
                int mYear = (Integer.parseInt(FungsiGeneral.getTahun(tgl)));
                int mMonth = (Integer.parseInt(FungsiGeneral.getBulan(tgl)))-1;
                int mDay = (Integer.parseInt(FungsiGeneral.getHari(tgl)));

                DatePickerDialog datePickerDialog = new DatePickerDialog(OrderListSubAgen.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth);
                                SimpleDateFormat format = new SimpleDateFormat("MMyy");
                                format = new SimpleDateFormat("dd-MM-yyyy");
                                txtTglDari.setText(format.format(calendar.getTime()));

                                rcvData.scrollToPosition(0);
                                adapter.removeAllModel();
                                RefreshRecyclerView();
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        txtTglSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(OrderListSubAgen.this, OrderListSubAgen.this.findViewById(android.R.id.content));
                if (txtTglSampai.getText().toString().equals("")){
                    txtTglSampai.setText(FungsiGeneral.serverNowFormated());
                }
                Long tgl = FungsiGeneral.getMillisDate(txtTglSampai.getText().toString());
                int mYear = (Integer.parseInt(FungsiGeneral.getTahun(tgl)));
                int mMonth = (Integer.parseInt(FungsiGeneral.getBulan(tgl)))-1;
                int mDay = (Integer.parseInt(FungsiGeneral.getHari(tgl)));

                DatePickerDialog datePickerDialog = new DatePickerDialog(OrderListSubAgen.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth);
                                SimpleDateFormat format = new SimpleDateFormat("MMyy");
                                format = new SimpleDateFormat("dd-MM-yyyy");
                                txtTglSampai.setText(format.format(calendar.getTime()));

                                rcvData.scrollToPosition(0);
                                adapter.removeAllModel();
                                RefreshRecyclerView();
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        txtSubAgen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(OrderListSubAgen.this, FilterCustomer.class);
                s.putExtra("MODE", MODE_FILTER_CUSTOMER_ORDER);
                startActivityForResult(s, 1);
            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0){
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        }
    }


    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }

    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }

    public void load(){
        tgl_dari   = getSimpleDate(txtTglDari.getText().toString());
        tgl_Sampai = getSimpleDate(txtTglSampai.getText().toString());

        String Filter = "";
        Filter += "?customer_seq=" + EthicaApplication.getInstance().getCustomerSeqGlobal() +"&search="+"&offset=" + Integer.toString(adapter.getItemCount())+"&status="+status+
                  "&tgl_dari="+getTglFormatMySql(tgl_dari)+"&tgl_sampai="+getTglFormatMySql(tgl_Sampai)+
                   "&sub_agen_seq="+seqCustomer;

        String url = Routes.url_load_pesanan_sub_agen + Filter;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<OrderListSubAgenModel> itemDataModels = new ArrayList<>();
                OrderListSubAgenModel Data;
                itemDataModels.clear();
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            Data = new OrderListSubAgenModel(
                                    obj.getInt("seq"),
                                    obj.getString("tanggal"),
                                    obj.getString("nomor"),
                                    obj.getDouble("total"),
                                    obj.getDouble("diskon"),
                                    obj.getDouble("subtotal"),
                                    obj.getString("status"),
                                    obj.getDouble("total_berat"),
                                    obj.getDouble("ongkos_kirim"),
                                    obj.getLong("customer_seq"),
                                    obj.getString("nama_customer")
                            );
                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(OrderListSubAgen.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(false);
                    }
                }
                adapter.addModels(itemDataModels);
                setLoaded(false);
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                swipe.setRefreshing(false);
                setLoaded(false);
                Toast.makeText(OrderListSubAgen.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    @Override
    public void onStart() {
        super.onStart();
        isStart = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extra = data.getExtras();
                seqCustomer     = extra.getLong("CUSTOMER_SEQ");
                txtSubAgen.setText(extra.getString("SUB_AGEN"));
            }
            rcvData.scrollToPosition(0);
            adapter.removeAllModel();
            RefreshRecyclerView();
        }else{
            if (requestCode == 1) {
                seqCustomer = 0;
                txtSubAgen.setText("");
                rcvData.scrollToPosition(0);
                adapter.removeAllModel();
                RefreshRecyclerView();
            }
        }
    }
}
