package bandung.orion.com.ethica.form.product_list;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import bandung.orion.com.ethica.R;

public class mCarouselAdapter extends RecyclerView.Adapter {
    Context context;
    List<String> items;
    ProductList fragment;

    public mCarouselAdapter(Context context, List<String> item, ProductList fragment) {
        this.context = context;
        this.fragment = fragment;
        this.items = item;
    }

    public void addModels(List<String> items) {
        int pos = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(pos, items.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.list_item_carousel, parent, false);
        return new mCarouselAdapter.ItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final String mCurrentItem = items.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;
            Picasso.get().load(mCurrentItem).into(itemHolder.imgCarousel);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        ImageView imgCarousel;

        public ItemHolder(View itemView) {
            super(itemView);
            imgCarousel = itemView.findViewById(R.id.imgCarousel);
        }
    }
}
