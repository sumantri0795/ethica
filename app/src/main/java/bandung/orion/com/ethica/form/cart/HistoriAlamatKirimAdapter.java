package bandung.orion.com.ethica.form.cart;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.ZoomImage.ZoomImageActivity;
import bandung.orion.com.ethica.form.product_detail_list.ProductDetailList;

import static android.app.Activity.RESULT_OK;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_SARIMBIT;

public class HistoriAlamatKirimAdapter extends RecyclerView.Adapter {
    Context context;
    List<HistoriAlamatKirimModel> HistoriAlamatKirimAdapter;
    private final int VIEW_TYVE_ITEM = 0, VIEW_TYVE_LOADING = 1;
    private ProgressDialog Loading;
    private int ViewAlamat;
    HistoriAlamatKirim Activity;

    public HistoriAlamatKirimAdapter(Context context, List<HistoriAlamatKirimModel> HistoriAlamatKirimAdapter, int view, HistoriAlamatKirim AppCompatActivity) {
        this.context = context;
        this.HistoriAlamatKirimAdapter = HistoriAlamatKirimAdapter;
        this.Loading = new ProgressDialog(context);
        this.ViewAlamat = view;
        this.Activity = AppCompatActivity;
    }

    public void addModels(List<HistoriAlamatKirimModel> HistoriAlamatKirimAdapter) {
        int pos = this.HistoriAlamatKirimAdapter.size();
        this.HistoriAlamatKirimAdapter.addAll(HistoriAlamatKirimAdapter);
        notifyItemRangeInserted(pos, HistoriAlamatKirimAdapter.size());
    }

    public void addMoel(HistoriAlamatKirimModel HistoriAlamatKirimModel) {
        this.HistoriAlamatKirimAdapter.add(HistoriAlamatKirimModel);
        notifyItemRangeInserted(HistoriAlamatKirimAdapter.size()-1,HistoriAlamatKirimAdapter.size()-1);
    }

    public void removeMoel(int idx) {
        if (HistoriAlamatKirimAdapter.size() > 0){
            this.HistoriAlamatKirimAdapter.remove(HistoriAlamatKirimAdapter.size()-1);
            notifyItemRemoved(HistoriAlamatKirimAdapter.size());
        }
    }

    public void removeAllModel(){
        int LastPosition = HistoriAlamatKirimAdapter.size();
        this.HistoriAlamatKirimAdapter.removeAll(HistoriAlamatKirimAdapter);
        notifyItemRangeRemoved(0, LastPosition);
    }

    public void SetJenisView(int view){
        this.ViewAlamat = view;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYVE_ITEM){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(ViewAlamat, parent, false);
            return new ItemHolder(row);
        }else if(viewType == VIEW_TYVE_LOADING){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemHolder){
            final HistoriAlamatKirimModel mCurrentItem = HistoriAlamatKirimAdapter.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;

            itemHolder.txtAlamat.setText(mCurrentItem.getAlamat_kirim().trim());

            itemHolder.crdView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Activity.SetResultAlamat(mCurrentItem);
                }
            });

        }else if (holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return HistoriAlamatKirimAdapter.get(position) == null ? VIEW_TYVE_LOADING : VIEW_TYVE_ITEM;
    }

    @Override
    public int getItemCount() {
        return HistoriAlamatKirimAdapter.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtAlamat;
        CardView crdView;


        public ItemHolder(View itemView) {
            super(itemView);
            txtAlamat = itemView.findViewById(R.id.txtAlamat);
            crdView = itemView.findViewById(R.id.crdView);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pg_loading);
        }
    }
}
