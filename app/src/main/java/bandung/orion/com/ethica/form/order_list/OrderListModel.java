package bandung.orion.com.ethica.form.order_list;

public class OrderListModel {
    private long seq;
    private String tanggal;
    private String nomor;
    private double total;
    private double subtotal;
    private double diskon;
    private double berat;
    private double ongkos_kirim;
    private String status;
    private int notif;
    private String jenis_so;
    private String is_selesai_chat;

    public OrderListModel(long seq, String tanggal, String nomor, double total, double diskon, double subtotal, String status, String jenis_so, double berat, double ongkos_kirim) {
        this.seq = seq;
        this.tanggal = tanggal;
        this.nomor = nomor;
        this.total = total;
        this.diskon = diskon;
        this.subtotal = subtotal;
        this.status = status;
        this.notif = 0;
        this.jenis_so = jenis_so;
        this.berat = berat;
        this.ongkos_kirim = ongkos_kirim;
        this.is_selesai_chat = "";
    }

    public OrderListModel() {
        this.seq = 0;
        this.tanggal = "";
        this.nomor = "";
        this.total = 0;
        this.diskon = 0;
        this.subtotal = 0;
        this.status = "";
        this.notif = 0;
        this.is_selesai_chat = "";
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getDiskon() {
        return diskon;
    }

    public void setDiskon(double diskon) {
        this.diskon = diskon;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNotif() {
        return notif;
    }

    public void setNotif(int notif) {
        this.notif = notif;
    }

    public String getJenis_so() {
        return jenis_so;
    }

    public void setJenis_so(String jenis_so) {
        this.jenis_so = jenis_so;
    }

    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }

    public double getOngkos_kirim() {
        return ongkos_kirim;
    }

    public void setOngkos_kirim(double ongkos_kirim) {
        this.ongkos_kirim = ongkos_kirim;
    }

    public String getIs_selesai_chat() {
        return is_selesai_chat;
    }

    public void setIs_selesai_chat(String is_selesai_chat) {
        this.is_selesai_chat = is_selesai_chat;
    }
}
