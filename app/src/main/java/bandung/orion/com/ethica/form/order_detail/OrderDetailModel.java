package bandung.orion.com.ethica.form.order_detail;

public class OrderDetailModel {
    private long seq;
    private long customer_seq;
    private long barang_seq;
    private double qty;
    //langsung ambil tanpa save word
    private String kode;
    private String nama;
    private double harga;
    private String gambar;
    private double harga_diskon;
    private double diskon;
    private String gambar_besar;
    private String alamat_kirim;
    private String alamat_pengirim;
    private int tipe_view;
    private String keterangan_gambar;

    private String ekspedisi;
    private String service;
    private String no_resi;
    private double berat_total;


    public OrderDetailModel(long customer_seq, long barang_seq, double qty, String kode, String nama, double harga, String gambar, String gambar_besar) {
        this.customer_seq = customer_seq;
        this.barang_seq = barang_seq;
        this.qty = qty;
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
        this.gambar = gambar;
        this.gambar_besar = gambar_besar;
        this.alamat_kirim = "";
        this.tipe_view = 0;
        this.alamat_pengirim = "";
        this.keterangan_gambar = "";
        this.berat_total = 0.0;
    }


    public OrderDetailModel() {
        this.customer_seq = 0;
        this.barang_seq = 0;
        this.qty = 0;
        this.kode = "";
        this.nama = "";
        this.harga = 0;
        this.gambar = "";
        this.gambar_besar = "";
        this.alamat_kirim = "";
        this.tipe_view = 0;
        this.alamat_pengirim = "";
        this.keterangan_gambar = "";
        this.berat_total = 0.0;
    }

    public long getCustomer_seq() {
        return customer_seq;
    }

    public void setCustomer_seq(long customer_seq) {
        this.customer_seq = customer_seq;
    }

    public long getBarang_seq() {
        return barang_seq;
    }

    public void setBarang_seq(long barang_seq) {
        this.barang_seq = barang_seq;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getHarga() {
        return harga;
    }

    public String getGambar() {
        return gambar.replace(" ", "%20");
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public double getHarga_diskon() {
        return harga_diskon;
    }

    public void setHarga_diskon(double harga_diskon) {
        this.harga_diskon = harga_diskon;
    }

    public double getDiskon() {
        return diskon;
    }

    public void setDiskon(double diskon) {
        this.diskon = diskon;
    }

    public String getGambar_besar() {
        return gambar_besar.replace(" ", "%20");
    }

    public void setGambar_besar(String gambar_besar) {
        this.gambar_besar = gambar_besar;
    }

    public String getAlamat_kirim() {
        return alamat_kirim;
    }

    public void setAlamat_kirim(String alamat_kirim) {
        this.alamat_kirim = alamat_kirim;
    }

    public int getTipe_view() {
        return tipe_view;
    }

    public void setTipe_view(int tipe_view) {
        this.tipe_view = tipe_view;
    }

    public String getAlamat_pengirim() {
        return alamat_pengirim;
    }

    public void setAlamat_pengirim(String alamat_pengirim) {
        this.alamat_pengirim = alamat_pengirim;
    }

    public String getKeterangan_gambar() {
        return keterangan_gambar;
    }

    public void setKeterangan_gambar(String keterangan_gambar) {
        this.keterangan_gambar = keterangan_gambar;
    }

    public String getEkspedisi() {
        return ekspedisi;
    }

    public void setEkspedisi(String ekspedisi) {
        this.ekspedisi = ekspedisi;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getNo_resi() {
        return no_resi;
    }

    public void setNo_resi(String no_resi) {
        this.no_resi = no_resi;
    }

    public double getBerat_total() {
        return berat_total;
    }

    public void setBerat_total(double berat_total) {
        this.berat_total = berat_total;
    }
}
