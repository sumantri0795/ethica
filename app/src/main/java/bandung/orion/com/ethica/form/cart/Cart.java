package bandung.orion.com.ethica.form.cart;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.product_list.ProductList;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

public class Cart extends AppCompatActivity {

    private Button btnOrder;
    private RecyclerView rcvData;
    private TextView txtTotal;
    private ArrayList<CartModel> datas;
    private CartAdapter adapter;
    private BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent in = new Intent(Cart.this, ProductList.class);
                    startActivity(in);
                case R.id.navigation_account:
                    btnOrder.setText("Account");
                    return true;
                case R.id.navigation_chart:
                    btnOrder.setText("Cart");
                    return true;
                case R.id.navigation_order_list:
                    btnOrder.setText("Order List");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        CreateView();
        InitClass();
        EventClass();
        load();
    }

    private void CreateView(){
        btnOrder = (Button) findViewById(R.id.btnOrder);
        txtTotal = (TextView) findViewById(R.id.txtTotalMst);
        rcvData = (RecyclerView) findViewById(R.id.rcvData);
        rcvData.setLayoutManager(new LinearLayoutManager(this));

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        datas = new ArrayList<CartModel>();
        //this.adapter = new CartAdapter(this);
        rcvData.setAdapter(adapter);
    }

    private void InitClass(){
        txtTotal.setText("Total : 0 ");
        navigation.setSelectedItemId(R.id.navigation_chart);
    }


    // untuk menampilkan semua data pada listview
    private void load(){
        datas.clear();
        adapter.notifyDataSetChanged();

        // membuat request JSON
        String url = Routes.url_load_cart+"/"+String.valueOf(((EthicaApplication)getApplicationContext()).getCustomerSeqGlobal())+"/"+((EthicaApplication)getApplicationContext()).getUserIdGlobal();
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                datas.clear();

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        CartModel item = new CartModel();
                        item.setBarang_seq(obj.getLong("barang_seq"));
                        item.setCustomer_seq(obj.getLong("customer_seq"));
                        item.setHarga(obj.getDouble("harga"));
                        item.setKode(obj.getString("kode"));
                        item.setNama(obj.getString("nama"));
                        item.setQty(obj.getDouble("qty"));
                        item.setGambar(obj.getString("gambar"));
                        item.setSeq(obj.getLong("seq"));

                        datas.add(item);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                hitung();

                Cart.this.adapter.addModels(datas);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
            }
        });

        // menambah request ke request queue
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    private void EventClass(){
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (datas.size() == 0) {
                    FungsiGeneral.inform(Cart.this, "Belum ada data yang diinput.","");
                    return;
                }


                AlertDialog dialog = new AlertDialog.Builder(Cart.this).create();
                dialog.setTitle("Confirmation");
                dialog.setMessage("Apakah data sudah benar");
                dialog.setCancelable(false);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {
                        Cart.this.KirimDataMst();
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {

                    }
                });
                dialog.setIcon(android.R.drawable.ic_dialog_alert);
                dialog.show();;
            }
        });
    }


    private void KirimDataMst(){
        String url;
        url = Routes.url_keep_pesanan_master;
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    Toast.makeText(Cart.this, "Data berhasil disimpan.", Toast.LENGTH_SHORT).show();
                    load();
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(Cart.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Cart.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();

                params.put("customer_seq", String.valueOf(((EthicaApplication)getApplicationContext()).getCustomerSeqGlobal()));


                return params;
            }
        };

        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);

    }



    public void hitung(){
        double total = 0;
        for (int i = 0; i < datas.size(); i++){
            total += datas.get(i).getQty() * datas.get(i).getHarga();
        }
        txtTotal.setText("Total : "+FungsiGeneral.FloatToStrFmt(total));
    }

    public void delete_list(int pos){
        datas.remove(pos);
    }

    @Override
    protected void onStart() {
        super.onStart();
        navigation.setSelectedItemId(R.id.navigation_chart);
    }
}
//rubah