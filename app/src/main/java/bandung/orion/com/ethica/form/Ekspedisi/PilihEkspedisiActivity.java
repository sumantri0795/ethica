package bandung.orion.com.ethica.form.Ekspedisi;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;

public class PilihEkspedisiActivity extends AppCompatActivity {
    public RecyclerView RcvData;
    public PilihEkspedisiAdapter mAdapter;

    boolean IsLoading;
    ILoadMore loadMore;

    private LinearLayoutManager linearLayoutManager;
    public List<PilihEkspedisiModel> ListItems = new ArrayList<>();
    public List<List<PilihEkspedisiModel>> ListChild = new ArrayList<>();

    private int NoOfColumns;
    final List<View> pageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_ekspedisi);
        CreateView();
        InitClass();
        EventClass();
        LoadData();
    }

    private void CreateView(){
        this.RcvData = (RecyclerView) findViewById(R.id.RcvData);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void InitClass(){
        NoOfColumns = 1;
        SetJenisTampilan();
        Toolbar toolbar = (Toolbar) findViewById(R.id.ToolbarAct);
        this.setTitle("Pilih Kurir");
    }

    protected void EventClass(){
        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                mAdapter.addMoel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.removeMoel(ListItems.size()-1);
                        LoadData();
                    }
                },1000);
            }
        });

        RcvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

    }

    public void LoadData(){
        String Filter = "";
        String url = Routes.url_load_ekspedisi;
        url = url.replace(" ", "%20");
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<PilihEkspedisiModel> itemDataModels = new ArrayList<>();
                itemDataModels.clear();

                List<PilihEkspedisiModel> itemDataChild = new ArrayList<>();
                itemDataChild.clear();
                ListChild.clear();

                PilihEkspedisiModel Data;
                int SeqTmp = 0;
                int idxChild = 0;

                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            if (SeqTmp != obj.getInt("seq") ){
                                Data = new PilihEkspedisiModel(
                                        obj.getInt("seq"),
                                        obj.getString("kode"),
                                        obj.getString("nama"),
                                        "",
                                        ""
                                );
                                itemDataModels.add(Data);

                                if (SeqTmp > 0){
                                    ListChild.add(idxChild, itemDataChild);
                                    idxChild += 1;
                                    itemDataChild = new ArrayList<>();
                                }
                            }

                            Data = new PilihEkspedisiModel(
                                    obj.getInt("seq"),
                                    obj.getString("kode"),
                                    obj.getString("nama"),
                                    obj.getString("service"),
                                    obj.getString("deskripsi")
                            );
                            itemDataChild.add(Data);

                            SeqTmp = obj.getInt("seq");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    }
                }
                ListChild.add(idxChild, itemDataChild);
                mAdapter.addModels(itemDataModels);
                setLoaded(false);
                if (ListItems.size() <= 0){Toast.makeText(getApplicationContext(), "Tidak ditemukan", Toast.LENGTH_SHORT).show();}
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                setLoaded(false);
                Toast.makeText(getApplicationContext(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }


    public void RefreshRecyclerView(){
        /*
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
        */
    }

    private void SetJenisTampilan(){
        mAdapter = new PilihEkspedisiAdapter(PilihEkspedisiActivity.this, ListItems, ListChild, getIntent(), PilihEkspedisiActivity.this);
        RcvData.setLayoutManager(new GridLayoutManager(PilihEkspedisiActivity.this, NoOfColumns, GridLayoutManager.VERTICAL, false));
        linearLayoutManager = (LinearLayoutManager)RcvData.getLayoutManager();
        RcvData.setAdapter(mAdapter);
    }
}
