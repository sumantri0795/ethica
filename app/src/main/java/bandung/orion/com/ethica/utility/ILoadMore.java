package bandung.orion.com.ethica.utility;

public interface ILoadMore {
    void onLoadMore();
}
