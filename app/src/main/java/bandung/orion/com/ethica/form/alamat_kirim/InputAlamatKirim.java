package bandung.orion.com.ethica.form.alamat_kirim;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.cart.CartFragment;
import bandung.orion.com.ethica.form.cart.CartModel;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.order_detail.OrderDetail;
import bandung.orion.com.ethica.form.product_list.filter.FilterProductList;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.StrToIntDef;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowLong;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ITEM;
import static bandung.orion.com.ethica.utility.Routes.ANDROID_KEY_RAJA_ONGKIR;
import static bandung.orion.com.ethica.utility.Routes.API_KEY_RAJA_ONGKIR;

public class InputAlamatKirim extends AppCompatActivity {
    private Spinner spnProvinsi, spnKota, spnKecamatan, spnKelurahan;
    private Button btnCek;
    private TextView txtHarga;

    private List<String> ListProvinsi;
    private HashMap<Integer, Integer> HashProvinsi;
    private ArrayAdapter<String> AdapterProvinsi;

    private List<String> ListKota;
    private HashMap<Integer, Integer> HashKota;
    private ArrayAdapter<String> AdapterKota;

    private List<String> ListKecamatan;
    private ArrayAdapter<String> AdapterKecamatan;

    private List<String> ListKelurahan;
    private ArrayAdapter<String> AdapterKelurahan;
    private ProgressDialog Loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_alamat_kirim);
        CreateVew();
        InitClass();
        EventClass();
        SetProvinsi();
    }

    private void CreateVew(){
        this.spnProvinsi = (Spinner) findViewById(R.id.spnProvinsi);
        this.spnKota = (Spinner) findViewById(R.id.spnKota);
        this.spnKecamatan = (Spinner) findViewById(R.id.spnKecamatan);
        this.btnCek = (Button) findViewById(R.id.btnCek);
        this.txtHarga = (TextView) findViewById(R.id.txtHarga);
    }

    private void InitClass(){
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListProvinsi = new ArrayList<>();
        HashProvinsi = new HashMap<Integer, Integer>();
        AdapterProvinsi = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListProvinsi);
        AdapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProvinsi.setAdapter(AdapterProvinsi);

        ListKota = new ArrayList<>();
        HashKota = new HashMap<Integer, Integer>();
        AdapterKota = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListKota);
        AdapterKota.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnKota.setAdapter(AdapterKota);

        ListKecamatan = new ArrayList<>();
        AdapterKecamatan = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListKecamatan);
        AdapterKecamatan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnKecamatan.setAdapter(AdapterKecamatan);

        ListKelurahan = new ArrayList<>();
        AdapterKelurahan = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListKelurahan);
        AdapterKelurahan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnKelurahan.setAdapter(AdapterKelurahan);

        Bundle extra   = this.getIntent().getExtras();
//        this.BRAND     = extra.getString("BRAND");
//        this.SUB_BRAND = extra.getString("SUB_BRAND");
//        this.Kecamatan     = extra.getString("Kecamatan");
//        this.Kelurahan     = extra.getString("Kelurahan");
//
//        this.BRAND_TMP     = this.BRAND;
//        this.SUB_BRAND_TMP = this.SUB_BRAND;
//        this.Kecamatan_TMP     = this.Kecamatan;
//        this.Kelurahan_TMP     = this.Kelurahan;

        this.Loading = new ProgressDialog(InputAlamatKirim.this);
        Loading.setMessage("Loading...");
        Loading.setCancelable(false);
        Loading.show();

        SetProvinsi();
        SetKota(0);
        setKelurahan();
        Loading.dismiss();

        spnKelurahan.setVisibility(View.INVISIBLE);
    }


    private void EventClass(){
        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                BRAND_TMP = parent.getItemAtPosition(position).toString();
//                if (!BRAND_TMP.equals(TEXT_SEMUA)){
//                    SetKota(Integer.valueOf(parent.getItemAtPosition(position).toString()));
//                }else{
//                    ListSubBrand.clear();
//                    ListSubBrand.add(TEXT_SEMUA);
//                }
                SetKota(HashProvinsi.get(position));
                AdapterKota.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //AdapterSubBrand.clear();
            }
        });

//        spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                SUB_BRAND_TMP = parent.getItemAtPosition(position).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                AdapterSubBrand.clear();
//            }
//        });
//
//        spnKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Kecamatan_TMP = parent.getItemAtPosition(position).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                AdapterSubBrand.clear();
//            }
//        });
//
//        spnKelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Kelurahan_TMP = parent.getItemAtPosition(position).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                AdapterSubBrand.clear();
//            }
//        });
//
        btnCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetHarga(152, HashKota.get(spnKota.getSelectedItemPosition()));
                //SetProvinsi();
//                BRAND     = BRAND_TMP;
//                SUB_BRAND = SUB_BRAND_TMP;
//                Kecamatan     = Kecamatan_TMP;
//                Kelurahan     = Kelurahan_TMP;
//
//                Intent intent = getIntent();
//                intent.putExtra("BRAND", BRAND);
//                intent.putExtra("SUB_BRAND", SUB_BRAND);
//                intent.putExtra("Kecamatan", Kecamatan);
//                intent.putExtra("Kelurahan", Kelurahan);
//                setResult(RESULT_OK, intent);
//                finish();
            }
        });
    }

    public void SetProvinsi(){
        String url = Routes.url_get_provinsi;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ListProvinsi.clear();
                        HashProvinsi.clear();
                        try {
                            JSONArray ArrResults = new JSONObject(response).getJSONObject("rajaongkir").getJSONArray("results");
                            for (int i = 0; i < ArrResults.length(); i++) {
                                try {
                                    JSONObject obj = ArrResults.getJSONObject(i);
                                    ListProvinsi.add(obj.getString("province"));
                                    HashProvinsi.put(i, obj.getInt("province_id"));
                                    AdapterProvinsi.notifyDataSetChanged();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        EthicaApplication.getInstance().addToRequestQueue(stringRequest);
    }



    public void SetKota(int id_provinsi){
        if (id_provinsi > 0 ){

            Loading.setMessage("Loading...");
            Loading.setCancelable(false);
            Loading.show();

            String url = Routes.url_get_kota+"&province="+id_provinsi;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            ListKota.clear();
                            HashKota.clear();
                            try {
                                JSONArray ArrResults = new JSONObject(response).getJSONObject("rajaongkir").getJSONArray("results");
                                for (int i = 0; i < ArrResults.length(); i++) {
                                    try {
                                        JSONObject obj = ArrResults.getJSONObject(i);
                                        ListKota.add(obj.getString("city_name"));
                                        HashKota.put(i, obj.getInt("city_id"));
                                        AdapterKota.notifyDataSetChanged();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                Loading.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Loading.dismiss();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            EthicaApplication.getInstance().addToRequestQueue(stringRequest);
        }
    }

    private void SetHarga(final int id_kota_asal, final int id_kota_tujuan){
        if ((id_kota_asal > 0) || (id_kota_tujuan > 0) ){

            Loading.setMessage("Loading...");
            Loading.setCancelable(false);
            Loading.show();

            String url = Routes.url_get_harga;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONArray ArrResults = new JSONObject(response).getJSONObject("rajaongkir").getJSONArray("results");

                        JSONObject objr = ArrResults.getJSONObject(0);
                        JSONArray ArrobjCosts = objr.getJSONArray("costs");

                        for (int i = 0; i < ArrobjCosts.length(); i++) {
                            JSONObject ObjCosts = ArrobjCosts.getJSONObject(i);
                            JSONArray ArroCosts = ObjCosts.getJSONArray("cost");
                            String service = ObjCosts.getString("service");

                                for (int j = 0; j < ArroCosts.length(); j++) {
                                    JSONObject ObjCost = ArroCosts.getJSONObject(j);
                                    txtHarga.setText(txtHarga.getText() + service +" - "+ObjCost.getString("value")+ "  "+ObjCost.getString("etd")+"\n");
                                }
                        }
                        Loading.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Loading.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Loading.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters ke post url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("key", String.valueOf(API_KEY_RAJA_ONGKIR));
                    params.put("android-key", String.valueOf(ANDROID_KEY_RAJA_ONGKIR));
                    params.put("origin", String.valueOf(id_kota_asal));
                    params.put("destination", String.valueOf(id_kota_tujuan));
                    params.put("weight", String.valueOf(2));
                    params.put("courier", "jne");
                    return params;
                }
            };

            EthicaApplication.getInstance().addToRequestQueue(stringRequest);
        }
    }


    public void setKelurahan(){
//        ListKelurahan.add(TEXT_SEMUA);
//        String Kelurahan = getKelurahan(serverNowLong());
//        for (int i = 2000; i <= StrToIntDef(Kelurahan,0)+5 ; i++) {
//            ListKelurahan.add(Integer.toString(i));
//        }
//
//        if (!Kelurahan_TMP.equals(TEXT_SEMUA)){
//            spnKelurahan.setSelection(ListKelurahan.indexOf(Kelurahan_TMP));
//        }
//        AdapterKelurahan.notifyDataSetChanged();
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public class IdValue{
        public String id,name;
    }
}



