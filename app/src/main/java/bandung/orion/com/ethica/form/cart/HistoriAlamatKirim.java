package bandung.orion.com.ethica.form.cart;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.calculateWidth.calculateNoOfColumns;
import static java.lang.Math.round;

public class HistoriAlamatKirim extends AppCompatActivity {
    public RecyclerView RcvData;
    public HistoriAlamatKirimAdapter mAdapter;
    private SwipeRefreshLayout swipe;
    private SearchView txtSearch;

    int visibleThreshold = 6;
    int lastVisibleItem, totalItemCount;
    boolean IsLoading;
    ILoadMore loadMore;

    private LinearLayoutManager linearLayoutManager;
    public List<HistoriAlamatKirimModel> ListItems = new ArrayList<>();

    private int NoOfColumns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histori_alamat_kirim);
        CreateView();
        InitClass();
        EventClass();
        LoadData();
    }

    private void CreateView(){
        this.RcvData = (RecyclerView) findViewById(R.id.RcvData);
        this.swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        this.txtSearch = (SearchView) findViewById(R.id.txtSearch);
    }

    private void InitClass(){
        NoOfColumns = 1;
        SetJenisTampilan();
        Toolbar toolbar = (Toolbar) findViewById(R.id.ToolbarAct);
        this.setSupportActionBar(toolbar);

        Bundle extra   = this.getIntent().getExtras();
    }

    protected void EventClass(){
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!IsLoading){
                    mAdapter.removeAllModel();
                    RefreshRecyclerView();
                }
                swipe.setRefreshing(false);
            }
        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                mAdapter.addMoel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.removeMoel(ListItems.size()-1);
                        LoadData();
                    }
                },1000);
            }
        });

        RcvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

        txtSearch.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        txtSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }
        });
    }

    public void LoadData(){
        String Filter = "";
        Filter += "?customer_seq="+EthicaApplication.getInstance().getCustomerSeqGlobal()+"&user_id="+EthicaApplication.getInstance().getUserIdGlobal()+"&search=" + txtSearch.getQuery() + "&offset=" + Integer.toString(mAdapter.getItemCount());
        String url = Routes.url_get_pesanan_histori_alamat + Filter;
        url = url.replace(" ", "%20");
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<HistoriAlamatKirimModel> itemDataModels = new ArrayList<>();
                HistoriAlamatKirimModel Data;
                itemDataModels.clear();
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (!obj.getString("alamat_kirim").equals("")){
                            Data = new HistoriAlamatKirimModel(
                                    obj.getString("nama_kirim"),
                                    obj.getString("kelurahan"),
                                    obj.getString("provinsi"),
                                    obj.getString("kota"),
                                    obj.getString("kecamatan"),
                                    obj.getString("alamat_kirim_lengkap"),
                                    obj.getString("no_telepon_kirim"),
                                    obj.getInt("id_provinsi"),
                                    obj.getInt("id_kota"),
                                    obj.getInt("id_kecamatan"),
                                    obj.getString("alamat_kirim")
                            );
                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(false);
                    }
                }
                mAdapter.addModels(itemDataModels);
                setLoaded(false);
                swipe.setRefreshing(false);
                if (ListItems.size() <= 0){Toast.makeText(getApplicationContext(), "Tidak ditemukan", Toast.LENGTH_SHORT).show();}
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                swipe.setRefreshing(false);
                setLoaded(false);
                Toast.makeText(getApplicationContext(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }


    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }

    private void SetJenisTampilan(){
        mAdapter = new HistoriAlamatKirimAdapter(HistoriAlamatKirim.this, ListItems,  R.layout.list_item_histori_alamat_kirim, this);
        RcvData.setLayoutManager(new GridLayoutManager(HistoriAlamatKirim.this, NoOfColumns, GridLayoutManager.VERTICAL, false));
        linearLayoutManager = (LinearLayoutManager)RcvData.getLayoutManager();
        RcvData.setAdapter(mAdapter);
    }

    public void SetResultAlamat(HistoriAlamatKirimModel Data){
        Intent intent = getIntent();
        intent.putExtra("NAMA_KIRIM", Data.getNama_kirim());
        intent.putExtra("KELURAHAN", Data.getKelurahan());
        intent.putExtra("PROVINSI", Data.getProvinsi());
        intent.putExtra("KOTA", Data.getKota());
        intent.putExtra("KECAMATAN", Data.getKecamatan());
        intent.putExtra("ALAMAT_KIRIM_LENGKAP", Data.getAlamat_kirim_lengkap());
        intent.putExtra("NO_TELPON_KIRIM", Data.getNo_telepon_kirim());
        intent.putExtra("ID_PROVINSI", Data.getId_provinsi());
        intent.putExtra("ID_KOTA", Data.getId_kota());
        intent.putExtra("ID_KECAMATAN", Data.getId_kecamatan());
        intent.putExtra("ALAMAT_KIRIM", Data.getAlamat_kirim());
        setResult(RESULT_OK, intent);
        finish();
    }
}
