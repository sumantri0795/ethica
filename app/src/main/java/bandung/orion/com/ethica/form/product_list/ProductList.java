package bandung.orion.com.ethica.form.product_list;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.pre_order.PreOrderList;
import bandung.orion.com.ethica.form.product_detail_list.ProductDetailList;
import bandung.orion.com.ethica.form.product_list.filter.FilterProductList;
import bandung.orion.com.ethica.form.product_list.size_pack.SizePack;
import bandung.orion.com.ethica.form.sarimbit.product_sarimbit;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static android.app.Activity.RESULT_OK;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_ETHICA_HIJAB;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_NEW_PRODUCT;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_PROMO;
import static bandung.orion.com.ethica.utility.JConst.KLA_NOT_DIAMOND;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.calculateWidth.calculateNoOfColumns;
import static java.lang.Math.round;
import static java.nio.file.Files.copy;

public class ProductList extends Fragment implements AppBarLayout.OnOffsetChangedListener{
    public Dialog DialogFilter;
    public RecyclerView RcvProductList;
    private RecyclerView RcvUrutkan;
    public ProductListAdapter mAdapter;
    private UrutkanAdapter mAdapterUrutkan;

    private ImageButton btnFilter, btnUrutkan, btnJenisTampilan, btnSizePack;
    private int mMaxScrollSize;
    private AppBarLayout appbarLayout;
    private SwipeRefreshLayout swipe;
    public EditText txtTmp;
    public static TextView txtFilter, txtUrutkan, txtJenisTampilan, txtSizePack;
    private SearchView txtSearch;
    private DotsIndicator dotsIndicator;
    private ViewPager viewPager;
    private CollapsingToolbarLayout collapsing;

    int visibleThreshold = 6;
    int lastVisibleItem, totalItemCount;
    boolean IsLoading;
    ILoadMore loadMore;

    private LinearLayoutManager linearLayoutManager;
    public List<ProductListModel> ListItems = new ArrayList<>();
    private List<UrutkanModel> ListItemsUrutkan = new ArrayList<>();
    private View v;

    private final String TEXT_URUTKAN_TANGGAL_NAMA = "tgl_release%20DESC%20,%20Nama%20ASC";
    private final String TEXT_URUTKAN_NAMA     = "Nama";
    private final String TEXT_URUTKAN_TERBARU  = "Terbaru";
    private final String TEXT_URUTKAN_TERMURAH = "Termurah";
    private final String TEXT_URUTKAN_TERMAHAL = "Termahal";
    private final String TEXT_URUTKAN_TERBANYAK = "Terbanyak";

    private final String TEXT_SEMUA = "SEMUA";

    public String OrderBy = "";

    private final String MODE_TAMPILAN_GRID = "G";
    private final String MODE_TAMPILAN_LIST = "L";

    //Filter
    String BRAND, SUB_BRAND, WARNA, TAHUN;
    
    private FragmentActivity ThisActivity;
    private String ModeTampilan;
    private int NoOfColumns;
    private DotIndicatorPagerAdapter adapter;
    private int CountAllVew;
    final List<View> pageList = new ArrayList<>();

    private ImageView imgSarimbit, imgPreorder, imgPromo, imgNewProduct, imgHijab;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        if (EthicaApplication.getInstance().viewTempProduct == null) {
            view = inflater.inflate(R.layout.product_list_fragment, container, false);
            v = view;
            CreateView();
            InitClass();
            EventClass();
            createPageList();
            LoadData();
            EthicaApplication.getInstance().viewTempProduct = view;
        }else{
            view = EthicaApplication.getInstance().viewTempProduct;
            v = view;
        }
        return view;
    }

    private void CreateView(){
        this.btnFilter = (ImageButton) v.findViewById(R.id.btnFilter);
        this.btnUrutkan = (ImageButton) v.findViewById(R.id.btnUrutkan);
        this.RcvProductList = (RecyclerView) v.findViewById(R.id.rcvLoad);
        this.appbarLayout = (AppBarLayout) v.findViewById(R.id.materialup_appbar);
        this.swipe = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        this.txtTmp = (EditText) v.findViewById(R.id.txtTmp);
        this.txtFilter = (TextView) v.findViewById(R.id.txtFilter);
        this.txtUrutkan = (TextView) v.findViewById(R.id.txtUrutkan);
        this.txtSearch = (SearchView) v.findViewById(R.id.txtSearch);
        this.btnJenisTampilan = (ImageButton) v.findViewById(R.id.btnJenisTampilan);
        this.txtJenisTampilan = (TextView) v.findViewById(R.id.txtJenisTampilan);
        this.btnSizePack = (ImageButton) v.findViewById(R.id.btnSizePack);
        this.txtSizePack = (TextView) v.findViewById(R.id.txtSizePack);
        this.dotsIndicator = v.findViewById(R.id.dots_indicator);
        this.viewPager = v.findViewById(R.id.view_pager);
        this.collapsing = v.findViewById(R.id.collapsing);
        this.imgSarimbit = v.findViewById(R.id.imgSarimbit);
        this.imgPreorder = v.findViewById(R.id.imgPreorder);
        this.imgPromo = v.findViewById(R.id.imgPromo);
        this.imgNewProduct = v.findViewById(R.id.imgNewProduct);
        this.imgHijab = v.findViewById(R.id.imgHijab);
    }

    private void InitClass(){
        ThisActivity = getActivity();
        ModeTampilan = MODE_TAMPILAN_GRID;
        NoOfColumns = calculateNoOfColumns(ThisActivity);
        SetJenisTampilan();
        ListItemsUrutkan.clear();
        ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_NAMA, true));
//        ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_TERBARU, false));
        ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_TERMURAH, false));
        ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_TERMAHAL, false));
        //ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_TERBANYAK, false));


        this.DialogFilter = new Dialog(ThisActivity);
        this.DialogFilter.setContentView(R.layout.urutkan_product_list);
        this.RcvUrutkan = (RecyclerView) DialogFilter.findViewById(R.id.RcvUrutkan);

        RcvUrutkan.setLayoutManager(new LinearLayoutManager(ThisActivity));
        mAdapterUrutkan = new UrutkanAdapter(ThisActivity, ListItemsUrutkan, this);
        RcvUrutkan.setAdapter(mAdapterUrutkan);

        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.ToolbarAct);
        ((AppCompatActivity)ThisActivity).setSupportActionBar(toolbar);
        ((AppCompatActivity)ThisActivity).setSupportActionBar(toolbar);
        txtTmp.setVisibility(View.INVISIBLE);

        BRAND     = TEXT_SEMUA;
        SUB_BRAND = TEXT_SEMUA;
        WARNA     = TEXT_SEMUA;
        TAHUN     = TEXT_SEMUA;

        OrderBy   = TEXT_URUTKAN_TANGGAL_NAMA;
        CountAllVew = 0;
        SetHeightCarousel();
    }

    protected void EventClass(){
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!IsLoading){
                    mAdapter.removeAllModel();
                    RefreshRecyclerView();
                }
                swipe.setRefreshing(false);
            }
        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
//                if (ListItems.size() <=100 ){//100 adalah jumlah maksimal, bisa diisi kondisi juga nantinya
                    mAdapter.addMoel(null);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.removeMoel(ListItems.size()-1);
                            LoadData();
//                            setLoaded(false);
                        }
                    },1000);
//                }else {

//                }

            }
        });

        RcvProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

        txtSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }


        });

        txtSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }
        });


        btnUrutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFilter.show();
            }
        });

        txtUrutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFilter.show();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent s = new Intent(ThisActivity, FilterProductList.class);
                s.putExtra("BRAND", BRAND);
                s.putExtra("SUB_BRAND", SUB_BRAND);
                s.putExtra("WARNA", WARNA);
                s.putExtra("TAHUN", TAHUN);
                startActivityForResult(s, 1);
            }
        });

        txtFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent s = new Intent(ThisActivity, FilterProductList.class);
                s.putExtra("BRAND", BRAND);
                s.putExtra("SUB_BRAND", SUB_BRAND);
                s.putExtra("WARNA", WARNA);
                s.putExtra("TAHUN", TAHUN);
                startActivityForResult(s, 1);
            }
        });

        btnJenisTampilan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ModeTampilan == MODE_TAMPILAN_GRID){
                    ModeTampilan = MODE_TAMPILAN_LIST;
                }else if (ModeTampilan == MODE_TAMPILAN_LIST){
                    ModeTampilan = MODE_TAMPILAN_GRID;
                }
                SetJenisTampilan();
                mAdapter.notifyDataSetChanged();
            }
        });

        txtJenisTampilan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ModeTampilan == MODE_TAMPILAN_GRID){
                    ModeTampilan = MODE_TAMPILAN_LIST;
                }else if (ModeTampilan == MODE_TAMPILAN_LIST){
                    ModeTampilan = MODE_TAMPILAN_GRID;
                }
                SetJenisTampilan();
                mAdapter.notifyDataSetChanged();
            }
        });

        btnSizePack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(ThisActivity, SizePack.class);
                startActivity(s);
            }
        });

        txtSizePack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(ThisActivity, SizePack.class);
                startActivity(s);
            }
        });

        imgSarimbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EthicaApplication.getInstance().IsReload = false;
                Intent s = new Intent(ThisActivity, product_sarimbit.class);
                startActivityForResult(s, 2);
            }
        });

        imgPreorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(ThisActivity, PreOrderList.class);
                startActivityForResult(s, 5);
            }
        });

        imgPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EthicaApplication.getInstance().IsReload = false;
                Intent s = new Intent(ThisActivity, ProductDetailList.class);
                s.putExtra("JENIS_MENU", JENIS_MENU_PROMO);
                startActivityForResult(s, 3);
            }
        });

        imgNewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(ThisActivity, ProductDetailList.class);
                s.putExtra("JENIS_MENU", JENIS_MENU_NEW_PRODUCT);
                startActivityForResult(s, 6);
            }
        });

        imgHijab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = Routes.url_get_keagenan+"?customer_seq="+EthicaApplication.getInstance().getCustomerSeqGlobal()+"&user_id="+EthicaApplication.getInstance().getUserIdGlobal();
                JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try { JSONObject obj = response.getJSONObject(0);
                            if (obj.getInt("seq") > 0 ){
                                if (obj.getString("is_ethica_hijab").equals("T")) {
                                    EthicaApplication.getInstance().setIsEthicaHijabGlobal("T");
                                    EthicaApplication.getInstance().IsReload = false;
                                }else {
                                    EthicaApplication.getInstance().setIsEthicaHijabGlobal("F");
                                    inform(ThisActivity, "Anda belum terdaftar sebagai agen ethica hijab", "");
                                    return;
                                }
                            }
                            Intent s = new Intent(ThisActivity, ProductDetailList.class);
                            s.putExtra("JENIS_MENU", JENIS_MENU_ETHICA_HIJAB);
                            startActivityForResult(s, 4);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ThisActivity, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();

                        Toast.makeText(ThisActivity, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
                    }
                });
                EthicaApplication.getInstance().addToRequestQueue(jArr);
            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0){
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        }
    }

    public void LoadData(){
        String Filter = "";
        Filter += "?brand=" + BRAND + "&sub_brand=" + SUB_BRAND + "&warna=" + WARNA + "&tahun=" + TAHUN +
                  "&order_by=" + OrderBy+ "&search=" + txtSearch.getQuery() + "&offset=" + Integer.toString(mAdapter.getItemCount())+
                  "&klasifikasi="+KLA_NOT_DIAMOND+
                  "&is_ethica=" + EthicaApplication.getInstance().getIsEthicaGlobal()+
                  "&is_seply=" + EthicaApplication.getInstance().getIsSeplyGlobal()+
                  "&is_ethica_hijab=" + EthicaApplication.getInstance().getIsEthicaHijabGlobal();
        Filter  = Filter.replace(TEXT_SEMUA,"");

        String url = Routes.url_load_barang + Filter;
        url = url.replace(" ", "%20");
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<ProductListModel> itemDataModels = new ArrayList<>();
                ProductListModel Data;
                itemDataModels.clear();
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            Data = new ProductListModel(
                                    obj.getInt("seq"),
                                    obj.getString("barcode"),
                                    obj.getString("nama"),
                                    obj.getString("brand"),
                                    obj.getString("sub_brand"),
                                    obj.getString("warna"),
                                    obj.getString("ukuran"),
                                    obj.getString("katalog"),
                                    getMillisDate(obj.getString("tahun")),
                                    obj.getString("tipe_brg"),
                                    obj.getString("gambar"),
                                    obj.getDouble("harga"),
                                    getMillisDate(obj.getString("tgl_hapus")),
                                    obj.getDouble("stok"),
                                    0,
                                    obj.getString("gambar_besar"),
                                    obj.getString("keterangan")
                            );
                            Data.setDiskon_pct(obj.getDouble("diskon_pct"));
                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ThisActivity, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(false);
                    }
                }
                mAdapter.addModels(itemDataModels);
                setLoaded(false);
                swipe.setRefreshing(false);
                if (ListItems.size() <= 0){Toast.makeText(ThisActivity, "Tidak ditemukan", Toast.LENGTH_SHORT).show();}
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                swipe.setRefreshing(false);
                setLoaded(false);
                Toast.makeText(ThisActivity, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
        ((Home)ThisActivity).updateJumlahPesanOrderList();
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extra = data.getExtras();
                BRAND     = extra.getString("BRAND");
                SUB_BRAND = extra.getString("SUB_BRAND");
                WARNA     = extra.getString("WARNA");
                TAHUN     = extra.getString("TAHUN");
                RcvProductList.scrollToPosition(0);
                mAdapter.removeAllModel();
                RefreshRecyclerView();
            }
        }

        if (EthicaApplication.getInstance().KeCart == true){
            ((Home)ThisActivity).AktifkanCart();
            EthicaApplication.getInstance().KeCart = false;
        }

        //Jika ada yang menambahkan ke cart reload ulang tampilan
        if (EthicaApplication.getInstance().IsReload == true){
            RcvProductList.scrollToPosition(0);
            mAdapter.removeAllModel();
            RefreshRecyclerView();
        }
    }

    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }

    public void SetTextUrutkan(String Text){
        txtUrutkan.setText(Text);
    }

    public void SetTextStyle(int style){
        txtUrutkan.setTypeface(null, style);
    }

    private void SetJenisTampilan(){
        if (ModeTampilan.equals(MODE_TAMPILAN_GRID)){
            mAdapter = new ProductListAdapter(ThisActivity, ListItems, this, R.layout.list_item_product_list_grid);
            RcvProductList.setLayoutManager(new GridLayoutManager(ThisActivity, NoOfColumns, GridLayoutManager.VERTICAL, false));
            linearLayoutManager = (LinearLayoutManager)RcvProductList.getLayoutManager();
            RcvProductList.setAdapter(mAdapter);

            txtJenisTampilan.setText("List");
            btnJenisTampilan.setImageResource(R.drawable.ic_list);
        }else if (ModeTampilan.equals(MODE_TAMPILAN_LIST)){
            mAdapter = new ProductListAdapter(ThisActivity, ListItems, this, R.layout.list_item_product_list);
            RcvProductList.setLayoutManager(new LinearLayoutManager(ThisActivity));
            linearLayoutManager = (LinearLayoutManager)RcvProductList.getLayoutManager();
            RcvProductList.setAdapter(mAdapter);

            txtJenisTampilan.setText("Grid");
            btnJenisTampilan.setImageResource(R.drawable.ic_grid);
        }
    }

    @NonNull
    private List<View> createPageList() {
        String url = Routes.url_load_images_carousel;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                int CountView = 0;
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            pageList.add(createPageView(0, obj.getString("path")));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ThisActivity, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    }
                    CountView += 1;
                }
                CountAllVew = CountView;
                CurrentIdx  = CountView;

                adapter = new DotIndicatorPagerAdapter();
                adapter.setData(pageList);
                viewPager.setAdapter(adapter);
                viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
                dotsIndicator.setViewPager(viewPager);
                pageSwitcher(4);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                Toast.makeText(ThisActivity, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
        return pageList;
    }

    @NonNull
    private View createPageView(int color, String path) {
        ViewGroup container = null;
        View view = View.inflate(ThisActivity,R.layout.layout_carousel,container);
        ImageView ImgCarousel = view.findViewById(R.id.ImgCarousel);
        Picasso.get().load(path).into(ImgCarousel);
        return view;
    }

    public void pageSwitcher(int seconds) {
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000);
    }

    Timer timer;
    int CurrentIdx = 0;
    class RemindTask extends TimerTask {
        @Override
        public void run() {
            ThisActivity.runOnUiThread(new Runnable() {
                public void run() {
                    if (CountAllVew > 0){
                        if (CurrentIdx == CountAllVew){
                            adapter = new DotIndicatorPagerAdapter();
                            adapter.setData(pageList);
                            viewPager.setAdapter(adapter);
                            viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
                            dotsIndicator.setViewPager(viewPager);
                            CurrentIdx = 0;
                        }else{
                            CurrentIdx = viewPager.getCurrentItem() + 1;
                        }
                        viewPager.setCurrentItem(CurrentIdx);
                    }
                }
            });
        }
    }

    private void SetHeightCarousel(){
        DisplayMetrics displayMetrics = ThisActivity.getResources().getDisplayMetrics();
        AppBarLayout.LayoutParams layoutParams = (AppBarLayout.LayoutParams) collapsing.getLayoutParams();
        Integer height = (int) (long) round(displayMetrics.heightPixels * 0.30) ;
        layoutParams.height = height + 150;
        collapsing.setLayoutParams(layoutParams);
    }
}


