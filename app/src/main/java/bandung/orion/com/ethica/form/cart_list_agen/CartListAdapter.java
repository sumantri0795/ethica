package bandung.orion.com.ethica.form.cart_list_agen;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.product_list.ProductListAdapter;
import bandung.orion.com.ethica.form.product_list.ProductListModel;

import static bandung.orion.com.ethica.EthicaApplication.fmt;

public class CartListAdapter extends RecyclerView.Adapter {
    Context context;
    List<CartListModel> Datas;
    private final int VIEW_TYVE_ITEM = 0, VIEW_TYVE_LOADING = 1;
    private ProgressDialog Loading;
    private int ViewAlamat;

    public CartListAdapter(Context context, List<CartListModel> Datas, int view) {
        this.context = context;
        this.Datas = Datas;
        this.Loading = new ProgressDialog(context);
        this.ViewAlamat = view;
    }

    public void addModels(List<CartListModel> Datas) {
        int pos = this.Datas.size();
        this.Datas.addAll(Datas);
        notifyItemRangeInserted(pos, Datas.size());
    }

    public void addMoel(CartListModel CartListModel) {
        this.Datas.add(CartListModel);
        notifyItemRangeInserted(Datas.size()-1,Datas.size()-1);
    }

    public void removeMoel(int idx) {
        if (Datas.size() > 0){
            this.Datas.remove(Datas.size()-1);
            notifyItemRemoved(Datas.size());
        }
    }

    public void removeAllModel(){
        int LastPosition = Datas.size();
        this.Datas.removeAll(Datas);
        notifyItemRangeRemoved(0, LastPosition);
    }

    public void SetJenisView(int view){
        this.ViewAlamat = view;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYVE_ITEM){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(ViewAlamat, parent, false);
            return new ItemHolder(row);
        }else if(viewType == VIEW_TYVE_LOADING){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemHolder){
            final CartListModel mCurrentItem = Datas.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;

            if (!mCurrentItem.getGambar().equals("") ){
                Picasso.get().load(mCurrentItem.getGambar()).into(itemHolder.imgGambar);

                if (itemHolder.imgGambar.getDrawable() == null){
                    itemHolder.imgGambar.setImageResource(R.drawable.gambar_tidak_tersedia);
                }
            }else{
                itemHolder.imgGambar.setImageResource(R.drawable.gambar_tidak_tersedia);
            }

            itemHolder.txtNama.setText(mCurrentItem.getNama());
            itemHolder.txtHarga.setText("Rp." + fmt.format(mCurrentItem.getHarga()));
            itemHolder.txtStok.setText("Stok "+  fmt.format(mCurrentItem.getStok()));
            itemHolder.txtJumlah.setText("Jml. Barang "+fmt.format(mCurrentItem.getJumlah()));

            itemHolder.imgGambar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    android.content.Intent s = new android.content.Intent(context, bandung.orion.com.ethica.form.ZoomImage.ZoomImageActivity.class);
                    s.putExtra("GAMBAR", mCurrentItem.getGambar());
                    s.putExtra("NAMA", mCurrentItem.getNama());
                    s.putExtra("KETERANGAN", mCurrentItem.getKeterangan());
                    context.startActivity(s);
                }
            });

        }else if (holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return Datas.get(position) == null ? VIEW_TYVE_LOADING : VIEW_TYVE_ITEM;
    }

    @Override
    public int getItemCount() {
        return Datas.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtNama, txtHarga, txtStok, txtJumlah;
        ImageView imgGambar;
        public ItemHolder(View itemView) {
            super(itemView);
            txtNama   = itemView.findViewById(R.id.txtNama);
            txtHarga  = itemView.findViewById(R.id.txtHarga);
            txtStok   = itemView.findViewById(R.id.txtStok);
            txtJumlah = itemView.findViewById(R.id.txtJumlah);
            imgGambar = itemView.findViewById(R.id.imgGambar);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pg_loading);
        }
    }
}
