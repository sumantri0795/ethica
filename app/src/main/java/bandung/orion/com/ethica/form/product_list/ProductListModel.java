package bandung.orion.com.ethica.form.product_list;

public class ProductListModel {
    private long seq;
    private String barcode;
    private String nama;
    private String brand;
    private String sub_brand;
    private String warna;
    private String ukuran;
    private String katalog;
    private long tahun;
    private String tipe_brg;
    private String gambar;
    private Double harga;
    private long tgl_hapus;
    private Double stok;
    private long qty;
    private String gambar_besar;
    private String keterangan;
    private String is_ehijab;
    private String is_preorder;
    private long berlaku_dari;
    private long berlaku_sampai;
    private int jumlah_pesanan;
    private long tgl_release;
    private double berat;
    private double diskon_pct;


    public ProductListModel(long seq, String barcode, String nama, String brand, String sub_brand, String warna, String ukuran, String katalog, long tahun, String tipe_brg, String gambar, Double harga, long tgl_hapus, double stok, long qty, String gambar_besar, String keterangan) {
        this.seq = seq;
        this.barcode = barcode;
        this.nama = nama;
        this.brand = brand;
        this.sub_brand = sub_brand;
        this.warna = warna;
        this.ukuran = ukuran;
        this.katalog = katalog;
        this.tahun = tahun;
        this.tipe_brg = tipe_brg;
        this.gambar = gambar;
        this.harga = harga;
        this.tgl_hapus = tgl_hapus;
        this.stok = stok;
        this.qty = qty;
        this.gambar_besar = gambar_besar;
        this.keterangan = keterangan;
        this.is_ehijab = "";
        this.is_preorder = "";
        this.berlaku_dari = 0;
        this.berlaku_sampai = 0;
        this.jumlah_pesanan = 0;
        this.tgl_release = 0;
        this.berat = 0.0;
        this.diskon_pct = 0.0;
    }

    public ProductListModel() {
        this.seq = 0;
        this.barcode = "";
        this.nama = "";
        this.brand = "";
        this.sub_brand = "";
        this.warna = "";
        this.ukuran = "";
        this.katalog = "";
        this.tahun = 0;
        this.tipe_brg = "";
        this.gambar = "";
        this.harga = 0.0;
        this.tgl_hapus = 0;
        this.stok = 0.0;
        this.qty = 0;
        this.gambar_besar = "";
        this.keterangan = "";
        this.is_ehijab = "";
        this.is_preorder = "";
        this.berlaku_dari = 0;
        this.berlaku_sampai = 0;
        this.jumlah_pesanan = 0;
        this.tgl_release = 0;
        this.diskon_pct = 0.0;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setsub_brand(String sub_brand) {
        this.sub_brand = sub_brand;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public void setUkuran(String ukuran) {
        this.ukuran = ukuran;
    }

    public void setKatalog(String katalog) {
        this.katalog = katalog;
    }

    public void setTahun(long tahun) {
        this.tahun = tahun;
    }

    public void setTipe_brg(String tipe_brg) {
        this.tipe_brg = tipe_brg;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public void setTgl_hapus(long tgl_hapus) {
        this.tgl_hapus = tgl_hapus;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    public void setSub_brand(String sub_brand) {
        this.sub_brand = sub_brand;
    }

    public void setStok(Double stok) {
        this.stok = stok;
    }

    public void setGambar_besar(String gambar_besar) {
        this.gambar_besar = gambar_besar;
    }

    public long getSeq() {
        return seq;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getNama() {
        return nama;
    }

    public String getBrand() {
        return brand;
    }

    public String getsub_brand() {
        return sub_brand;
    }

    public String getWarna() {
        return warna;
    }

    public String getUkuran() {
        return ukuran;
    }

    public String getKatalog() {
        return katalog;
    }

    public long getTahun() {
        return tahun;
    }

    public String getTipe_brg() {
        return tipe_brg;
    }

    public String getGambar() {
        return gambar.replace(" ", "%20");
    }

    public Double getHarga() {
        return harga;
    }

    public long getTgl_hapus() {
        return tgl_hapus;
    }

    public long getQty() {
        return qty;
    }

    public String getSub_brand() {
        return sub_brand;
    }

    public Double getStok() {
        return stok;
    }

    public String getGambar_besar() {
        return gambar_besar.replace(" ", "%20");
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getIs_ehijab() {
        return is_ehijab;
    }

    public void setIs_ehijab(String is_ehijab) {
        this.is_ehijab = is_ehijab;
    }

    public String getIs_preorder() {
        return is_preorder;
    }

    public void setIs_preorder(String is_preorder) {
        this.is_preorder = is_preorder;
    }

    public long getBerlaku_dari() {
        return berlaku_dari;
    }

    public void setBerlaku_dari(long berlaku_dari) {
        this.berlaku_dari = berlaku_dari;
    }

    public long getBerlaku_sampai() {
        return berlaku_sampai;
    }

    public void setBerlaku_sampai(long berlaku_sampai) {
        this.berlaku_sampai = berlaku_sampai;
    }

    public int getJumlah_pesanan() {
        return jumlah_pesanan;
    }

    public void setJumlah_pesanan(int jumlah_pesanan) {
        this.jumlah_pesanan = jumlah_pesanan;
    }

    public long getTgl_release() {
        return tgl_release;
    }

    public void setTgl_release(long tgl_release) {
        this.tgl_release = tgl_release;
    }

    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }

    public double getDiskon_pct() {
        return diskon_pct;
    }

    public void setDiskon_pct(double diskon_pct) {
        this.diskon_pct = diskon_pct;
    }
}
