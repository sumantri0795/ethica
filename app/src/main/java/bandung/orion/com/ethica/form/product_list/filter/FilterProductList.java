package bandung.orion.com.ethica.form.product_list.filter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.order_detail.OrderDetailModel;
import bandung.orion.com.ethica.form.product_list.ProductListModel;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.StrToIntDef;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getTahun;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowLong;

public class FilterProductList extends AppCompatActivity {
    private Spinner spnBrand, spnSubBrand, spnWarna, spnTahun;
    private Button btnReset, btnFilter;
    private TextView txtTahun;

    private List<String> ListBrand;
    private ArrayAdapter<String> AdapterBrand;
    
    private List<String> ListSubBrand;
    private ArrayAdapter<String> AdapterSubBrand;

    private List<String> ListWarna;
    private ArrayAdapter<String> AdapterWarna;

    private List<String> ListTahun;
    private ArrayAdapter<String> AdapterTahun;

    String BRAND, SUB_BRAND, WARNA, TAHUN;
    String BRAND_TMP, SUB_BRAND_TMP, WARNA_TMP, TAHUN_TMP;

    private final String TEXT_SEMUA = "SEMUA";

    private ProgressDialog Loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_product_list);
        CreateVew();
        InitClass();
        EventClass();
    }


    private void CreateVew(){
        this.spnBrand = (Spinner) findViewById(R.id.spnBrand);
        this.spnSubBrand = (Spinner) findViewById(R.id.spnSubBrand);
        this.spnWarna = (Spinner) findViewById(R.id.spnWarna);
        this.spnTahun = (Spinner) findViewById(R.id.spnTahun);
        this.btnReset = (Button) findViewById(R.id.btnReset);
        this.btnFilter = (Button) findViewById(R.id.btnFilter);
        this.txtTahun = (TextView) findViewById(R.id.txtTahun);
    }

    private void InitClass(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListBrand = new ArrayList<>();
        AdapterBrand = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListBrand);
        AdapterBrand.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnBrand.setAdapter(AdapterBrand);

        ListSubBrand = new ArrayList<>();
        AdapterSubBrand = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListSubBrand);
        AdapterSubBrand.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSubBrand.setAdapter(AdapterSubBrand);

        ListWarna = new ArrayList<>();
        AdapterWarna = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListWarna);
        AdapterWarna.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnWarna.setAdapter(AdapterWarna);

        ListTahun = new ArrayList<>();
        AdapterTahun = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListTahun);
        AdapterTahun.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTahun.setAdapter(AdapterTahun);

        Bundle extra   = this.getIntent().getExtras();
        this.BRAND     = extra.getString("BRAND");
        this.SUB_BRAND = extra.getString("SUB_BRAND");
        this.WARNA     = extra.getString("WARNA");
        this.TAHUN     = extra.getString("TAHUN");

        this.BRAND_TMP     = this.BRAND;
        this.SUB_BRAND_TMP = this.SUB_BRAND;
        this.WARNA_TMP     = this.WARNA;
        this.TAHUN_TMP     = this.TAHUN;

        this.Loading = new ProgressDialog(FilterProductList.this);
        Loading.setMessage("Loading...");
        Loading.setCancelable(false);
        Loading.show();

        SetBrand();
        SetWarna();
        setTahun();
        Loading.dismiss();

        spnTahun.setVisibility(View.INVISIBLE);
        txtTahun.setVisibility(View.INVISIBLE);
    }
    private void EventClass(){
        spnBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BRAND_TMP = parent.getItemAtPosition(position).toString();
                if (!BRAND_TMP.equals(TEXT_SEMUA)){
                    setSubBrand(BRAND_TMP);
                }else{
                    ListSubBrand.clear();
                    ListSubBrand.add(TEXT_SEMUA);
                }
                AdapterSubBrand.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                AdapterSubBrand.clear();
            }
        });

        spnSubBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SUB_BRAND_TMP = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                AdapterSubBrand.clear();
            }
        });

        spnWarna.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                WARNA_TMP = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                AdapterSubBrand.clear();
            }
        });

        spnTahun.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TAHUN_TMP = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                AdapterSubBrand.clear();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BRAND     = BRAND_TMP;
                SUB_BRAND = SUB_BRAND_TMP;
                WARNA     = WARNA_TMP;
                TAHUN     = TAHUN_TMP;

                Intent intent = getIntent();
                intent.putExtra("BRAND", BRAND);
                intent.putExtra("SUB_BRAND", SUB_BRAND);
                intent.putExtra("WARNA", WARNA);
                intent.putExtra("TAHUN", TAHUN);
                setResult(RESULT_OK, intent);
                finish();
            }
        });


        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BRAND_TMP     = TEXT_SEMUA;
                SUB_BRAND_TMP = TEXT_SEMUA;
                WARNA_TMP     = TEXT_SEMUA;
                TAHUN_TMP     = TEXT_SEMUA;

                spnBrand.setSelection(ListBrand.indexOf(BRAND_TMP));
                spnWarna.setSelection(ListWarna.indexOf(WARNA_TMP));
                spnTahun.setSelection(ListTahun.indexOf(TAHUN_TMP));
                ListSubBrand.clear();
                ListSubBrand.add(TEXT_SEMUA);
                spnSubBrand.setSelection(ListBrand.indexOf(SUB_BRAND_TMP));

                AdapterBrand.notifyDataSetChanged();
                AdapterSubBrand.notifyDataSetChanged();
                AdapterWarna .notifyDataSetChanged();
                AdapterTahun.notifyDataSetChanged();
            }
        });
    }
    public void SetBrand(){
        String url = Routes.url_load_brand_barang+"?tipe_cust="+EthicaApplication.getInstance().getTipeCust();
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ListBrand.clear();
                ListBrand.add(TEXT_SEMUA);
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        ListBrand.add(obj.getString("brand"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                AdapterBrand.notifyDataSetChanged();

                if (!BRAND_TMP.equals(TEXT_SEMUA)){
                    spnBrand.setSelection(ListBrand.indexOf(BRAND_TMP));
                    AdapterBrand.notifyDataSetChanged();
                }else{
                    SUB_BRAND_TMP = TEXT_SEMUA;
                    ListSubBrand.clear();
                    ListSubBrand.add(TEXT_SEMUA);
                    spnSubBrand.setSelection(ListBrand.indexOf(BRAND_TMP));
                    AdapterSubBrand.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void setSubBrand(String Brand){
        if (!Brand.equals("")){

            Loading.setMessage("Loading...");
            Loading.setCancelable(false);
            Loading.show();

            String url = Routes.url_load_sub_brand_barang+"/"+Brand;
            url = url.replace(" ", "%20");
            JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    ListSubBrand.clear();
                    ListSubBrand.add(TEXT_SEMUA);
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            ListSubBrand.add(obj.getString("sub_brand"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    spnSubBrand.setSelection(ListSubBrand.indexOf(SUB_BRAND_TMP));
                    AdapterSubBrand.notifyDataSetChanged();
                    Loading.dismiss();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError e) {
                    e.printStackTrace();
                    Loading.dismiss();
                }
            });
            EthicaApplication.getInstance().addToRequestQueue(jArr);
        }
    }

    public void SetWarna(){
        String url = Routes.url_load_warna_barang;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ListWarna.clear();
                ListWarna.add(TEXT_SEMUA);
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        ListWarna.add(obj.getString("warna"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (!WARNA_TMP.equals(TEXT_SEMUA)){
                    spnWarna.setSelection(ListWarna.indexOf(WARNA_TMP));
                }
                AdapterWarna.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void setTahun(){
        ListTahun.add(TEXT_SEMUA);
        String Tahun = getTahun(serverNowLong());
        for (int i = 2000; i <= StrToIntDef(Tahun,0)+5 ; i++) {
            ListTahun.add(Integer.toString(i));
        }

        if (!TAHUN_TMP.equals(TEXT_SEMUA)){
            spnTahun.setSelection(ListTahun.indexOf(TAHUN_TMP));
        }
        AdapterTahun.notifyDataSetChanged();
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}


