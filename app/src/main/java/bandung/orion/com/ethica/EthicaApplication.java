package bandung.orion.com.ethica;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.text.NumberFormat;

import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.profile.login.LoginTable;
import bandung.orion.com.ethica.utility.DBConn;

public class EthicaApplication extends Application {
    public DBConn dbConn;
    private long customerSeqGlobal;
    private String namaCustGlobal;
    private String userIdGlobal;
    private String IsEthicaGlobal;
    private String IsSeplyGlobal;
    private String IsEthicaHijabGlobal;
    private String JenisCustGlobal;

    public static final String TAG = EthicaApplication.class.getSimpleName();
    public boolean isRefreshChat;
    private RequestQueue mRequestQueue;
    private static EthicaApplication mInstance;
    public final static NumberFormat fmt = NumberFormat.getInstance();
    public String ApiKey;
    public LoginTable loginTable;
    private String tipeCust;
    public boolean isLogOn;
    public View viewTempProduct = null;
    public Home HomeGlobal;
    public Boolean IsReload = false;
    private String VersionName;
    private int VersionCode;
    public boolean KeCart = false;

    @Override
    public void onCreate() {
        super.onCreate();
        this.dbConn = new DBConn(getApplicationContext());
        this.namaCustGlobal = "";
        this.userIdGlobal = "";
        this.JenisCustGlobal = "";
        this.userIdGlobal = "";
        this.IsEthicaGlobal = "";
        this.IsSeplyGlobal = "";
        this.IsEthicaHijabGlobal = "";
        this.isRefreshChat = false;
        this.ApiKey = "";
        this.isLogOn = false;
        mInstance = this;
        this.loginTable = new LoginTable(getApplicationContext(), this.dbConn);
        GetVersion();
    }

    private void GetVersion(){
        PackageInfo pinfo = null;
        try {
            this.VersionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            this.VersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static synchronized EthicaApplication getInstance() {
        return mInstance;
    }

    public synchronized Home getHomeGlobal() {
        return HomeGlobal;
    }

    public synchronized void setHomeGlobal(Home HomeGlobal) {
        this.HomeGlobal = HomeGlobal;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public long getCustomerSeqGlobal() {
        return customerSeqGlobal;
    }

    public void setCustomerSeqGlobal(long customerSeqGlobal) {
        this.customerSeqGlobal = customerSeqGlobal;
    }

    public String getNamaCustGlobal() {
        return namaCustGlobal;
    }

    public void setNamaCustGlobal(String namaCustGlobal) {
        this.namaCustGlobal = namaCustGlobal;
    }


    public String getTipeCust() {
        return tipeCust;
    }

    public void setTipeCust(String tipeCust) {
        this.tipeCust = tipeCust;
    }

    public String getUserIdGlobal() {
        return userIdGlobal;
    }

    public void setUserIdGlobal(String userIdGlobal) {
        this.userIdGlobal = userIdGlobal;
    }

    public String getIsEthicaGlobal() {
        return IsEthicaGlobal;
    }

    public void setIsEthicaGlobal(String isEthicaGlobal) {
        IsEthicaGlobal = isEthicaGlobal;
    }

    public String getIsSeplyGlobal() {
        return IsSeplyGlobal;
    }

    public void setIsSeplyGlobal(String isSeplyGlobal) {
        IsSeplyGlobal = isSeplyGlobal;
    }

    public String getIsEthicaHijabGlobal() {
        return IsEthicaHijabGlobal;
    }

    public void setIsEthicaHijabGlobal(String isEthicaHijabGlobal) {
        IsEthicaHijabGlobal = isEthicaHijabGlobal;
    }

    public String getJenisCustGlobal() {
        return JenisCustGlobal;
    }

    public void setJenisCustGlobal(String jenisCustGlobal) {
        JenisCustGlobal = jenisCustGlobal;
    }

    public String getVersionName() {
        return VersionName;
    }

    public void setVersionName(String versionName) {
        VersionName = versionName;
    }

    public int getVersionCode() {
        return VersionCode;
    }

    public void setVersionCode(int versionCode) {
        VersionCode = versionCode;
    }
}
