package bandung.orion.com.ethica.form.chat_order;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.product_list.ProductListAdapter;

import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDateFmt;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getTglFmt;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowFormated;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CHAT_ANDROID;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CHAT_DESKTOP;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CHAT_TANGGAL;
import static bandung.orion.com.ethica.utility.JConst.TYPE_LEFT;
import static bandung.orion.com.ethica.utility.JConst.TYPE_RIGHT;
import static bandung.orion.com.ethica.utility.JConst.TYPE_TANGGAL;

public class ChatOrderAdapter extends RecyclerView.Adapter {
    Context context;
    List<ChatOrderModel> itemDataModels;
    private final int TYVE_LOADING = 9;
    private Long now;
    private int CountPesan = 0;

    public ChatOrderAdapter(Context context) {
        this.context = context;
        itemDataModels = new ArrayList<>();
        now = getMillisDateFmt(serverNowFormated(), "yyyy-MM-dd");
    }

    public void addModels(List<ChatOrderModel> itemDataModels) {
        int pos = this.itemDataModels.size();
        this.itemDataModels.addAll(itemDataModels);
        notifyItemRangeInserted(pos, itemDataModels.size());

//        for (int i = 0; i < itemDataModels.size(); i++) {
//            if (!itemDataModels.get(i).getTipe_pesan().equals(TIPE_CHAT_TANGGAL)){
//                CountPesan += 1;
//            }
//        }
    }

    public void addModel(ChatOrderModel data) {
        int pos = this.itemDataModels.size();
        this.itemDataModels.add(data);
        notifyItemRangeInserted(pos, itemDataModels.size());
//        if (!(data == null)){
//            if (!data.getTipe_pesan().equals(TIPE_CHAT_TANGGAL)){
//                CountPesan += 1;
//            }
//        }
    }

    public void removeAllModel(){
        int LastPosition = itemDataModels.size();
        this.itemDataModels.removeAll(itemDataModels);
        notifyItemRangeRemoved(0, LastPosition);
        CountPesan = 0;
    }

    public void removeMoel(int idx) {
        if (itemDataModels.size() > 0){
            this.itemDataModels.remove(itemDataModels.size()-1);
            notifyItemRemoved(itemDataModels.size());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_RIGHT) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_chat_right, parent, false);
            return new ItemHolderRight(row);
        } else if (viewType == TYPE_LEFT) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_chat_left, parent, false);
            return new ItemHolderLeft(row);
        }else if(viewType == TYVE_LOADING){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(row);
        }else if(viewType == TYPE_TANGGAL){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_chat_tanggal, parent, false);
            return new ItemHolderTanggal(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ItemHolderRight) {
            final ChatOrderModel mCurrentItem = itemDataModels.get(i);
            final ItemHolderRight itemHolder = (ItemHolderRight) viewHolder;
            itemHolder.txtChat.setText(mCurrentItem.getIsi_pesan());
            itemHolder.txtTanggal.setText(getTglFmt(mCurrentItem.getTanggal(),"kk:mm"));
        } else if (viewHolder instanceof ItemHolderLeft) {
            final ChatOrderModel mCurrentItem = itemDataModels.get(i);
            final ItemHolderLeft itemHolder = (ItemHolderLeft) viewHolder;
            itemHolder.txtChat.setText(mCurrentItem.getIsi_pesan());
            itemHolder.txtTanggal.setText(getTglFmt(mCurrentItem.getTanggal(),"kk:mm"));
        }else if (viewHolder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewHolder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        } else if (viewHolder instanceof ItemHolderTanggal) {
            final ChatOrderModel mCurrentItem = itemDataModels.get(i);
            final ItemHolderTanggal itemHolder = (ItemHolderTanggal) viewHolder;
            if (!now.equals(mCurrentItem.getTanggal())){
                itemHolder.txtTanggal.setText(getTglFmt(mCurrentItem.getTanggal(),"EEE dd MMMM"));
            }else{
                itemHolder.txtTanggal.setText("Hari ini");
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemDataModels.size();
    }

    public int getCountPesan(){
        int count = 0;
        for (int i = 0; i < itemDataModels.size(); i++) {
            if (!(itemDataModels.get(i) == null)){
                if (!itemDataModels.get(i).getTipe_pesan().equals(TIPE_CHAT_TANGGAL)){
                    count += 1;
                }
            }
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if(itemDataModels.get(position) == null) {
            return TYVE_LOADING;
        }else {
            if(itemDataModels.get(position).getTipe_pesan().equals(TIPE_CHAT_ANDROID)){
                return TYPE_RIGHT;
            }else if (itemDataModels.get(position).getTipe_pesan().equals(TIPE_CHAT_DESKTOP)){
                return TYPE_LEFT;
            }else if (itemDataModels.get(position).getTipe_pesan().equals(TIPE_CHAT_TANGGAL)){
                return TYPE_TANGGAL;
            }
        }
        return 0;
    }

    private class ItemHolderRight extends RecyclerView.ViewHolder {
        TextView txtChat, txtTanggal;

        public ItemHolderRight(View itemView) {
            super(itemView);
            txtChat = itemView.findViewById(R.id.txtChat);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
        }
    }

    private class ItemHolderLeft extends RecyclerView.ViewHolder {
        TextView txtChat, txtTanggal;

        public ItemHolderLeft(View itemView) {
            super(itemView);
            txtChat = itemView.findViewById(R.id.txtChat);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pg_loading);
        }
    }

    private class ItemHolderTanggal extends RecyclerView.ViewHolder {
        TextView txtTanggal;

        public ItemHolderTanggal(View itemView) {
            super(itemView);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
        }
    }

}


