package bandung.orion.com.ethica.form.chat_order;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.product_list.ProductList;
import bandung.orion.com.ethica.form.product_list.UrutkanModel;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDateFmt;
import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNow;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowFormated;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowLong;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CHAT_ANDROID;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CHAT_TANGGAL;
import static bandung.orion.com.ethica.utility.Routes.url_save_chat_box;
import static bandung.orion.com.ethica.utility.Routes.url_update_baca_chat_box;

public class ChatOrder extends AppCompatActivity{
    private TextView txtIsiPesan;
    private TextView txtTglOrder, txtNoOrder, txtTotal, txtDiskon, txtSubtotal, txtRPTotal, txtRPDiskon, txtRPSubtotal, txtLabelSubtotal, txtLabelDiskon;
    private RecyclerView RcvChat;
    private ImageButton btnKirim;
    private ChatOrderAdapter mAdapter;
    //private SwipeRefreshLayout swipe;

    private LinearLayoutManager linearLayoutManager;
    public List<ChatOrderModel> ListItems = new ArrayList<>();
    private List<UrutkanModel> ListItemsUrutkan = new ArrayList<>();
    boolean IsLoading;
    ILoadMore loadMore;
    int lastVisibleItem, totalItemCount;
    int visibleThreshold = 6;

    long pesana_seq;

    Long tglTmp = Long.valueOf(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_order);
        InitView();
        InitClass();
        EventClass();
        LoadMaster();
        //RefreshRecyclerView();
        UpdateBaca();
        ChatAutoReload(2);
    }

    private void UpdateBaca() {
        String url;
        url = Routes.url_update_baca_chat_box + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                        inform(ChatOrder.this, MSG_HARUS_UPDATE, "");
                        return;
                    }

                    if (jObj.getString("status").equals("success")) {

                    } else {
                        Toast.makeText(ChatOrder.this, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ChatOrder.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChatOrder.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pesanan_seq", String.valueOf(pesana_seq));
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };
        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
    }

    private void InitView() {
        txtTotal = (TextView) findViewById(R.id.txtTotalMst);
        txtNoOrder = (TextView) findViewById(R.id.txtNoOrder);
        txtTglOrder = (TextView) findViewById(R.id.txtTglOrder);
        txtDiskon = (TextView) findViewById(R.id.txtDiskon);
        txtSubtotal = (TextView) findViewById(R.id.txtSubtotal);
        txtLabelDiskon = (TextView) findViewById(R.id.txtLabelDiskon);
        txtLabelSubtotal = (TextView) findViewById(R.id.txtLabelSubtotal);
        txtRPTotal = (TextView) findViewById(R.id.txtRPTotalMst);
        txtRPDiskon = (TextView) findViewById(R.id.txtRPDiskon);
        txtRPSubtotal = (TextView) findViewById(R.id.txtRPSubtotal);
        txtIsiPesan = (TextView) findViewById(R.id.txtIsiPesan);
        btnKirim = (ImageButton) findViewById(R.id.btnKirim);
        RcvChat = (RecyclerView) findViewById(R.id.RcvChat);
        //swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
    }

    private void InitClass() {
        setTitle("Percakapan");
        Bundle extra   = this.getIntent().getExtras();
        pesana_seq     = extra.getLong("PESANAN_SEQ");

        RcvChat.setLayoutManager(new LinearLayoutManager(this));
        linearLayoutManager = (LinearLayoutManager)RcvChat.getLayoutManager();

        ListItems = new ArrayList<ChatOrderModel>();
        this.mAdapter = new ChatOrderAdapter(this);
        RcvChat.setAdapter(mAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtDiskon.setVisibility(View.GONE);
        txtSubtotal.setVisibility(View.GONE);
        txtLabelDiskon.setVisibility(View.GONE);
        txtLabelSubtotal.setVisibility(View.GONE);
        txtRPDiskon.setVisibility(View.GONE);
        txtRPSubtotal.setVisibility(View.GONE);
    }

    private void EventClass() {
//        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if (!IsLoading){
//                    mAdapter.removeAllModel();
//                    RefreshRecyclerView();
//                }
//                swipe.setRefreshing(false);
//            }
//        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                //mAdapter.addModel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //mAdapter.removeMoel(ListItems.size()-1);
                        LoadData();
                    }
                },1000);
            }
        });

        RcvChat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!txtIsiPesan.getText().toString().trim().equals("")){
                    KirimPesan();
                }
            }
        });
    }

    private void KirimPesan(){
        String url;
        url = Routes.url_save_chat_box + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                        inform(ChatOrder.this, MSG_HARUS_UPDATE, "");
                        return;
                    }

                    if (jObj.getString("status").equals("chat selesai")){
                        inform(ChatOrder.this, "Pesan tidak bisa dikirim karena sudah diselesaikan", "");
                        return;
                    }

                    if (jObj.getString("status").equals("success")) {
                        //TambahkanPesan();
                        txtIsiPesan.setText("");
                        //RcvChat.scrollToPosition(mAdapter.getItemCount()-1);
                    } else {
                        //mAdapter.removeMoel(ListItems.size()-1);
                        Toast.makeText(ChatOrder.this, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ChatOrder.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChatOrder.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pesanan_seq", String.valueOf(pesana_seq));
                params.put("isi_pesan", String.valueOf(txtIsiPesan.getText().toString()));
                params.put("tipe_pesan", String.valueOf(TIPE_CHAT_ANDROID));
                params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                params.put("is_baca", "F");
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };
        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
    }

    private void TambahkanPesan(){
        ChatOrderModel Data;
        Data = new ChatOrderModel(
                0,
                Integer.valueOf( String.valueOf(pesana_seq)),
                serverNowLong(),
                txtIsiPesan.getText().toString(),
                TIPE_CHAT_ANDROID,
                EthicaApplication.getInstance().getUserIdGlobal(),
                "F",
                "F"
        );
        ListItems.add(Data);
        mAdapter.addModel(Data);
        RcvChat.scrollToPosition(mAdapter.getItemCount()-1);
    }

    public void LoadData(){
        String Filter = "";
        Filter += "?pesanan_seq=" + pesana_seq + "&offset=" + Integer.toString(mAdapter.getCountPesan());
        String url = Routes.url_load_chat_box + Filter;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<ChatOrderModel> itemDataModels = new ArrayList<>();
                ChatOrderModel Data;
                itemDataModels.clear();
                //Long now = serverNowLong();

                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            Long tgl = getMillisDateFmt(obj.getString("tanggal"), "yyyy-MM-dd");

                            if(!tglTmp.equals(tgl)){
                                Data = new ChatOrderModel();
                                Data.setTanggal(tgl);
                                Data.setTipe_pesan(TIPE_CHAT_TANGGAL);
                                tglTmp = tgl;
                                itemDataModels.add(Data);
                            }

                            Data = new ChatOrderModel(
                                    obj.getInt("seq"),
                                    obj.getInt("pesanan_seq"),
                                    getMillisDateFmt(obj.getString("tanggal"), "yyyy-MM-dd hh:mm:ss"),
                                    obj.getString("isi_pesan"),
                                    obj.getString("tipe_pesan"),
                                    obj.getString("user_id"),
                                    obj.getString("is_baca"),
                                    obj.getString("is_execute")
                            );
                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ChatOrder.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    }
                }
                mAdapter.addModels(itemDataModels);
                if (itemDataModels.size() > 0){
                    RcvChat.scrollToPosition(mAdapter.getItemCount()-1);
                }
                UpdateBaca();
                setLoaded(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                setLoaded(false);
                Toast.makeText(ChatOrder.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    private void LoadMaster(){

        // membuat request JSON
        String url = Routes.url_get_pesanan_master2+"/"+String.valueOf(pesana_seq);
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        if (i == 0){
                            txtTglOrder.setText(": "+obj.getString("tanggal"));
                            txtNoOrder.setText(": "+obj.getString("nomor"));
                            txtTotal.setText(FungsiGeneral.FloatToStrFmt(obj.getDouble("totalmst"), false));
//                            double disk, subtotal;
//                            disk = obj.getDouble("diskonmst");
//                            subtotal = obj.getDouble("subtotalmst");
                            //if (disk != 0){
                                //txtDiskon.setVisibility(View.VISIBLE);
                                //txtSubtotal.setVisibility(View.VISIBLE);

//                                txtLabelDiskon.setVisibility(View.VISIBLE);
//                                txtLabelSubtotal.setVisibility(View.VISIBLE);
//                                txtDiskon.setText(FungsiGeneral.FloatToStrFmt(disk, false));
//                                txtSubtotal.setText(FungsiGeneral.FloatToStrFmt(subtotal, false));
//                                txtRPDiskon.setVisibility(View.VISIBLE);
//                                txtRPSubtotal.setVisibility(View.VISIBLE);
                            //}else{
//                                txtDiskon.setVisibility(View.GONE);
//                                txtSubtotal.setVisibility(View.GONE);

//                                txtLabelDiskon.setVisibility(View.GONE);
//                                txtLabelSubtotal.setVisibility(View.GONE);
//                                txtRPDiskon.setVisibility(View.GONE);
//                                txtRPSubtotal.setVisibility(View.GONE);
                            //}
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }

    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }

    public void ChatAutoReload(int seconds) {
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000);
    }

    Timer timer;
    class RemindTask extends TimerTask {
        @Override
        public void run() {
            ChatOrder.this.runOnUiThread(new Runnable() {
                public void run() {
                    //LoadData();
                    RefreshRecyclerView();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        timer.cancel();
        finish();
    }


    @Override
    public boolean onSupportNavigateUp(){
        timer.cancel();
        finish();
        return true;
    }
}