package bandung.orion.com.ethica.form.order_list_sub_agen;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.chat_order.ChatOrder;
import bandung.orion.com.ethica.form.order_detail.OrderDetail;
import bandung.orion.com.ethica.form.order_list.OrderListFragment;
import bandung.orion.com.ethica.utility.FungsiGeneral;

import static bandung.orion.com.ethica.utility.JConst.MODE_DETAIL_ORDER_APPROVE;
import static bandung.orion.com.ethica.utility.JConst.MODE_DETAIL_ORDER_DETAIL;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_APPROVE;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_APPROVE_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_PROSES;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_PROSES_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIHAPUS;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIHAPUS_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIPROSES;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIPROSES_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_KEEP;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_KEEP_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_SELESAI;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_SELESAI_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_TOLAK;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_TOLAK_TEXT;

public class OrderListSubAgenAdapter extends RecyclerView.Adapter {
    Context context;
    List<OrderListSubAgenModel> arrData;
    private final int VIEW_TYVE_ITEM = 0, VIEW_TYVE_LOADING = 1;
    private OrderListSubAgen Activity;

    public OrderListSubAgenAdapter(Context context, List<OrderListSubAgenModel> arrData, OrderListSubAgen Activity) {
        this.context = context;
        this.arrData = arrData;
        this.Activity = Activity;
    }

    public void addModels(List<OrderListSubAgenModel> arrData) {
        int pos = this.arrData.size();
        this.arrData.addAll(arrData);
        notifyItemRangeInserted(pos, arrData.size());
    }

    public void addModel(OrderListSubAgenModel data) {
        this.arrData.add(data);
        notifyItemRangeInserted(arrData.size()-1,arrData.size()-1);
    }

    public void removeMoel(int idx) {
        if (arrData.size() > 0) {
            this.arrData.remove(arrData.size() - 1);
            notifyItemRemoved(arrData.size());
        }
    }

    public void removeAllModel(){
        int LastPosition = arrData.size();
        this.arrData.removeAll(arrData);
        notifyItemRangeRemoved(0, LastPosition);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYVE_ITEM){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.list_item_order_list_sub_agen, parent, false);
            return new ItemHolder(row);
        }else if(viewType == VIEW_TYVE_LOADING){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder){
            final OrderListSubAgenModel mCurrentItem = arrData.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;

            itemHolder.txtCustomer.setText(mCurrentItem.getNama_customer());
            itemHolder.txtNoOrder.setText(":  "+mCurrentItem.getNomor());
            itemHolder.txtTglOrder.setText(":  "+mCurrentItem.getTanggal());
            itemHolder.txtTotal.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getTotal(), false));
            itemHolder.txtBerat.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getTotal_berat(), false)+" Kg");
            itemHolder.txtOngkir.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getOngkos_kirim(), false));

            itemHolder.txtStatus.setTextColor(Color.parseColor("#F5F5F5"));
            if (mCurrentItem.getStatus().equals(STATUS_ORDER_KEEP)){
                itemHolder.txtStatus.setText(STATUS_ORDER_KEEP_TEXT);
                itemHolder.txtStatus.setBackgroundResource(R.drawable.style_status_order_keep);
            }else if (mCurrentItem.getStatus().equals(STATUS_ORDER_BELUM_APPROVE)){
                itemHolder.txtStatus.setText(STATUS_ORDER_BELUM_APPROVE_TEXT);
                itemHolder.txtStatus.setBackgroundResource(R.drawable.style_status_order_belum_approve);
            }else if (mCurrentItem.getStatus().equals(STATUS_ORDER_BELUM_PROSES)){
                itemHolder.txtStatus.setText(STATUS_ORDER_BELUM_PROSES_TEXT);
                itemHolder.txtStatus.setBackgroundResource(R.drawable.style_status_order_belum_proses);
            }else if (mCurrentItem.getStatus().equals(STATUS_ORDER_DIPROSES)){
                itemHolder.txtStatus.setText(STATUS_ORDER_DIPROSES_TEXT);
                itemHolder.txtStatus.setBackgroundResource(R.drawable.style_status_order_diproses);
            }else if (mCurrentItem.getStatus().equals(STATUS_ORDER_TOLAK)) {
                itemHolder.txtStatus.setText(STATUS_ORDER_TOLAK_TEXT);
                itemHolder.txtStatus.setBackgroundResource(R.drawable.style_status_order_ditolak);
            }else if (mCurrentItem.getStatus().equals(STATUS_ORDER_DIHAPUS)) {
                itemHolder.txtStatus.setText(STATUS_ORDER_DIHAPUS_TEXT);
                itemHolder.txtStatus.setBackgroundResource(R.drawable.style_status_order_dihapus);
            }else if (mCurrentItem.getStatus().equals(STATUS_ORDER_SELESAI)) {
                itemHolder.txtStatus.setText(STATUS_ORDER_SELESAI_TEXT);
                itemHolder.txtStatus.setBackgroundResource(R.drawable.style_status_selesai);
            }


            itemHolder.crdView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(context.getApplicationContext(), OrderDetail.class);
                    in.putExtra("seq", mCurrentItem.getSeq());
                    in.putExtra("mode", MODE_DETAIL_ORDER_APPROVE);
                    Activity.startActivityForResult(in, 2);
                }
            });

            if (mCurrentItem.getDiskon() != 0){
                itemHolder.txtDiskon.setVisibility(View.VISIBLE);
                itemHolder.txtSubtotal.setVisibility(View.VISIBLE);


                itemHolder.txtRpDiskon.setVisibility(View.VISIBLE);
                itemHolder.txtRpSubtotal.setVisibility(View.VISIBLE);

                itemHolder.txtLabelDiskon.setVisibility(View.VISIBLE);
                itemHolder.txtLabelSubtotal.setVisibility(View.VISIBLE);
                itemHolder.txtDiskon.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getDiskon(), false));
                itemHolder.txtSubtotal.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getSubtotal(), false));
            }else{
                itemHolder.txtDiskon.setVisibility(View.GONE);
                itemHolder.txtSubtotal.setVisibility(View.GONE);

                itemHolder.txtRpDiskon.setVisibility(View.GONE);
                itemHolder.txtRpSubtotal.setVisibility(View.GONE);

                itemHolder.txtLabelDiskon.setVisibility(View.GONE);
                itemHolder.txtLabelSubtotal.setVisibility(View.GONE);

            }
        }else if (holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return arrData.get(position) == null ? VIEW_TYVE_LOADING:VIEW_TYVE_ITEM;
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtTglOrder, txtNoOrder, txtTotal, txtSubtotal, txtDiskon, txtRpTotal, txtRpSubtotal, txtRpDiskon, txtLabelDiskon, txtLabelSubtotal, txtStatus, txtBerat, txtOngkir, txtCustomer;
        CardView crdView;

        public ItemHolder(View itemView) {
            super(itemView);
            txtTotal = (TextView) itemView.findViewById(R.id.txtTotalMst);
            txtSubtotal = (TextView) itemView.findViewById(R.id.txtSubtotal);
            txtDiskon = (TextView) itemView.findViewById(R.id.txtDiskon);

            txtRpTotal = (TextView) itemView.findViewById(R.id.txtRPTotalMst);
            txtRpSubtotal = (TextView) itemView.findViewById(R.id.txtRPSubtotal);
            txtRpDiskon = (TextView) itemView.findViewById(R.id.txtRPDiskon);
            txtNoOrder = (TextView) itemView.findViewById(R.id.txtNoOrder);
            txtTglOrder = (TextView) itemView.findViewById(R.id.txtTglOrder);
            txtLabelDiskon = (TextView) itemView.findViewById(R.id.txtLabelDiskon);
            txtLabelSubtotal = (TextView) itemView.findViewById(R.id.txtLabelSubtotal);
            crdView = (CardView) itemView.findViewById(R.id.crdView);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtBerat = (TextView) itemView.findViewById(R.id.txtBerat);
            txtOngkir = (TextView) itemView.findViewById(R.id.txtOngkir);
            txtCustomer = (TextView) itemView.findViewById(R.id.txtCustomer);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pg_loading);
        }
    }
}