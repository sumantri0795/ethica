package bandung.orion.com.ethica.form.sarimbit;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.ZoomImage.ZoomImageActivity;
import bandung.orion.com.ethica.form.product_detail_list.ProductDetailList;

import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_SARIMBIT;

public class SarimbitListAdapter extends RecyclerView.Adapter {
    Context context;
    product_sarimbit Activity;
    List<SarimbitListModel> SarimbitListModels;
    private final int VIEW_TYVE_ITEM = 0, VIEW_TYVE_LOADING = 1;
    private ProgressDialog Loading;
    private int ViewSarimbitList;


    public SarimbitListAdapter(Context context, List<SarimbitListModel> SarimbitListModels, product_sarimbit AppCompatActivity, int view) {
        this.context = context;
        this.Activity = AppCompatActivity;
        this.SarimbitListModels = SarimbitListModels;
        this.Loading = new ProgressDialog(context);
        this.ViewSarimbitList = view;
    }

    public void addModels(List<SarimbitListModel> SarimbitListModels) {
        int pos = this.SarimbitListModels.size();
        this.SarimbitListModels.addAll(SarimbitListModels);
        notifyItemRangeInserted(pos, SarimbitListModels.size());
    }

    public void addMoel(SarimbitListModel SarimbitListModel) {
        this.SarimbitListModels.add(SarimbitListModel);
        notifyItemRangeInserted(SarimbitListModels.size()-1,SarimbitListModels.size()-1);
    }

    public void removeMoel(int idx) {
        if (SarimbitListModels.size() > 0){
            this.SarimbitListModels.remove(SarimbitListModels.size()-1);
            notifyItemRemoved(SarimbitListModels.size());
        }
    }

    public void removeAllModel(){
        int LastPosition = SarimbitListModels.size();
        this.SarimbitListModels.removeAll(SarimbitListModels);
        notifyItemRangeRemoved(0, LastPosition);
    }

    public void SetJenisView(int view){
        this.ViewSarimbitList = view;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYVE_ITEM){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(ViewSarimbitList, parent, false);
            return new ItemHolder(row);
        }else if(viewType == VIEW_TYVE_LOADING){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemHolder){
            final boolean IsSudahLogin = EthicaApplication.getInstance().getCustomerSeqGlobal() > 0;

            final SarimbitListModel mCurrentItem = SarimbitListModels.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;

            if (ViewSarimbitList == R.layout.list_item_sarimbit_list_grid){
                if (!mCurrentItem.getGambar_sedang().equals("") ){
                    Picasso.get().load(mCurrentItem.getGambar_sedang()).into(itemHolder.imgBarang);

                    if (itemHolder.imgBarang.getDrawable() == null){
                        itemHolder.imgBarang.setImageResource(R.drawable.gambar_tidak_tersedia);
                    }
                }else{
                    itemHolder.imgBarang.setImageResource(R.drawable.gambar_tidak_tersedia);
                }
            }

            itemHolder.txtNama.setText(mCurrentItem.getNama());

            itemHolder.btnLihat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent s = new Intent(context, ProductDetailList.class);
                    s.putExtra("JENIS_MENU", JENIS_MENU_SARIMBIT);
                    s.putExtra("SARIMBIT_SEQ", mCurrentItem.getSeq());
                    s.putExtra("PATH_IMG_SARIMBIT", mCurrentItem.getGambar_besar());
//                    context.startActivity(s);
                    Activity.startActivityForResult(s, 1);
                }
            });

            itemHolder.imgBarang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent s = new Intent(context, ZoomImageActivity.class);
                    s.putExtra("GAMBAR", mCurrentItem.getGambar_besar());
                    s.putExtra("NAMA", mCurrentItem.getNama());
                    s.putExtra("KETERANGAN", mCurrentItem.getKeterangan());
                    s.putExtra("SEQ", 0);
                    context.startActivity(s);
                }
            });

        }else if (holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return SarimbitListModels.get(position) == null ? VIEW_TYVE_LOADING : VIEW_TYVE_ITEM;
    }

    @Override
    public int getItemCount() {
        return SarimbitListModels.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtNama;
        ImageView imgBarang;
        Button btnLihat;

        public ItemHolder(View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            imgBarang = itemView.findViewById(R.id.ImgBarang);
            btnLihat = itemView.findViewById(R.id.btnLihat);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pg_loading);
        }
    }

}
