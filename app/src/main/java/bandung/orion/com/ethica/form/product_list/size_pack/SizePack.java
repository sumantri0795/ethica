package bandung.orion.com.ethica.form.product_list.size_pack;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.product_list.DotIndicatorPagerAdapter;
import bandung.orion.com.ethica.form.product_list.ZoomOutPageTransformer;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;

public class SizePack extends AppCompatActivity {
    private PhotoView imgSizePack;
    private Button btnTutup;
    private ProgressDialog Loading;
    private ImageButton btnDownload;

    final List<View> pageList = new ArrayList<>();
    private List<String> ListPath = new ArrayList<>();
    private int CountAllVew;
    private DotIndicatorSizePackPagerAdapter adapter;
    private ViewPager viewPager;
    private DotsIndicator dotsIndicator;

    private boolean IsAdaGambar;
    private int ResultGambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_size_pack);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        CreateView();
        //LoadData();
        createPageList();
        EventClass();
    }

    private void EventClass() {
        btnTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsAdaGambar){
                    ResultGambar = 0;
                    String Path = CreateGetDir();

                    String gambar = ListPath.get(viewPager.getCurrentItem());
                    String nama = "Size_pack_"+String.valueOf(viewPager.getCurrentItem()+1);
                    if (!gambar.equals("")){
                        Picasso.get().load(gambar).into(picassoImageTarget(Path, nama+".jpg"));

                        int i = 0;
                        do {
                            i = 0;
                        }while (ResultGambar == 0);
                        if (ResultGambar == 1) {
                            Toast.makeText(getApplicationContext(), "Gambar berhasil disimpan di : "+Path+"/"+nama, Toast.LENGTH_SHORT).show();

                            //Koding agar setelah download langsung masuk ke galeri
                            MediaScannerConnection.scanFile(getApplicationContext(), new String[]{Path+"/"+nama+".jpg"},null, new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {
                                    // now visible in gallery
                                }
                            });
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Gambar tidak tersedia", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Gambar tidak tersedia", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

//    private void LoadData() {
//            Loading.setMessage("Loading...");
//            Loading.setCancelable(false);
//            Loading.show();
//
//            String url = Routes.url_get_size_pack;
//            JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
//                @Override
//                public void onResponse(JSONArray response) {
//                    Picasso.get().load(R.drawable.gambar_tidak_tersedia).into(imgSizePack);
//                    for (int i = 0; i < response.length(); i++) {
//                        try { JSONObject obj = response.getJSONObject(i);
//                            if (obj.getInt("seq") > 0){
//                                String gambar = obj.getString("path");
//                                if (!gambar.equals("")){
//                                    Picasso.get().load(gambar).into(imgSizePack);
//                                }else {
//                                    Picasso.get().load(R.drawable.gambar_tidak_tersedia).into(imgSizePack);
//                                }
//                            }
//                            Loading.dismiss();
//                        } catch (JSONException e) {
//                            Loading.dismiss();
//                            e.printStackTrace();
//                            Toast.makeText(SizePack.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//            }, new Response.ErrorListener() {
//
//                @Override
//                public void onErrorResponse(VolleyError e) {
//                    Loading.dismiss();
//                    e.printStackTrace();
//                    Toast.makeText(SizePack.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
//                }
//            });
//                EthicaApplication.getInstance().addToRequestQueue(jArr);
//
//            }

    private void CreateView() {
        imgSizePack = findViewById(R.id.imgSizePack);
        btnTutup = findViewById(R.id.btnTutup);
        this.Loading = new ProgressDialog(SizePack.this);
        this.viewPager = findViewById(R.id.view_pager);
        this.dotsIndicator = findViewById(R.id.dots_indicator);
        btnDownload = findViewById(R.id.btnDownload);

        CountAllVew = 0;
    }


    @NonNull
    private List<View> createPageList() {
        IsAdaGambar = false;
        ListPath.clear();
        String url = Routes.url_get_size_pack;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                int CountView = 0;
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            pageList.add(createPageView(0, obj.getString("path")));
                            IsAdaGambar = true;
                            ListPath.add(obj.getString("path"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(SizePack.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    }
                    CountView += 1;
                }

//                CountAllVew = CountView;
//                CurrentIdx  = CountView;

                adapter = new DotIndicatorSizePackPagerAdapter();
                adapter.setData(pageList);
                viewPager.setAdapter(adapter);
                viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
                dotsIndicator.setViewPager(viewPager);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                Toast.makeText(SizePack.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
        return pageList;
    }

    @NonNull
    private View createPageView(int color, String path) {
        ViewGroup container = null;
        View view = View.inflate(SizePack.this,R.layout.layout_carousel_sizepack,container);
        ImageView photoView = view.findViewById(R.id.imgSizePack);
        Picasso.get().load(path).into(photoView);
        return view;
    }

    private String CreateGetDir() {
        File direct = new File("/storage/emulated/0/Download/ethica");
        if (!direct.exists()) {
            direct.mkdirs();
        }
        return direct.getPath();
    }

    private Target picassoImageTarget(final String imageDir, final String imageName) {
        final File directory = new File(imageDir);
        return new Target() {

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (bitmap!= null){
                            final File myImageFile = new File(directory, imageName); // Create image file
                            FileOutputStream fos = null;
                            try {
                                fos = new FileOutputStream(myImageFile);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                ResultGambar = 1;
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            ResultGambar = 2;
                        }
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                ResultGambar = 2;
                Toast.makeText(getApplicationContext(), "Gambar gagal disimpan", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {}
            }
        };
    }
}

