package bandung.orion.com.ethica.form.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.filter_customer.FilterCustomer;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.order_list_sub_agen.OrderListSubAgen;
import bandung.orion.com.ethica.form.product_list.ProductListModel;
import bandung.orion.com.ethica.form.profile.login.Login;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.JConst.JENIS_CUST_AGEN;
import static bandung.orion.com.ethica.utility.JConst.MODE_FILTER_CUSTOMER_CART;
import static bandung.orion.com.ethica.utility.JConst.MODE_FILTER_CUSTOMER_ORDER;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_APPROVE;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_KEEP;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CUSTOMER_ETHICA;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CUSTOMER_SEPLY;

public class ProfileFragment extends Fragment {
    private TextView txtNama, txtUserId, txtAlamat, txtEmail, txtNoTelpon, txtJmlOrderList;
    private Button btnChangePassword, btnLogOut;
    private View v;
    private CardView CardAgen;
    private ConstraintLayout clOrderSubAgen, clCartSubAgen;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.activity_profile_fragment, container, false);
        v = view;
        CreateView();
        InitClass();
        EventClass();
        //get_count_pesanan_sub_agen();
        LoadData();
        return view;
    }

    private void CreateView(){
        this.txtNama = (TextView) v.findViewById(R.id.txtNama);
        this.txtUserId = (TextView) v.findViewById(R.id.txtUserId);
        this.txtAlamat = (TextView) v.findViewById(R.id.txtAlamat);
        this.txtEmail = (TextView) v.findViewById(R.id.txtEmail);
        this.txtNoTelpon = (TextView) v.findViewById(R.id.txtNoTelpon);
        this.btnChangePassword = (Button) v.findViewById(R.id.btnChangePassword);
        this.btnLogOut = (Button) v.findViewById(R.id.btnLogOut);
        this.CardAgen = (CardView) v.findViewById(R.id.CardAgen);
        this.clOrderSubAgen = (ConstraintLayout) v.findViewById(R.id.clOrderSubAgen);
        this.clCartSubAgen = (ConstraintLayout) v.findViewById(R.id.clCartSubAgen);
        this.txtJmlOrderList = (TextView) v.findViewById(R.id.txtJmlOrderList);
        this.CardAgen.setVisibility(View.GONE);
    }

    private void InitClass(){
        this.txtNama.setText("");
        this.txtUserId.setText("");
        this.txtAlamat.setText("");
        this.txtEmail.setText("");
        this.txtNoTelpon.setText("");

        if (!EthicaApplication.getInstance().getJenisCustGlobal().equals(JENIS_CUST_AGEN)){
            CardAgen.setVisibility(View.GONE);
        }else{
            CardAgen.setVisibility(View.VISIBLE);
        }
    }
    protected void EventClass(){
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.content.Intent s = new android.content.Intent(getActivity(), bandung.orion.com.ethica.form.profile.change_password.ChangePassword.class);
                startActivity(s);
            }
        });

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
                dialog.setMessage("Anda yakin akan keluar?");
                dialog.setCancelable(true);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {
                        EthicaApplication.getInstance().loginTable.deleteAll();
                        EthicaApplication.getInstance().ApiKey = "";
                        EthicaApplication.getInstance().setTipeCust("");
                        EthicaApplication.getInstance().setCustomerSeqGlobal(0);
                        ((Home)getActivity()).backHome();
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {

                    }
                });
                dialog.show();
            }
        });

        clOrderSubAgen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(getActivity(), OrderListSubAgen.class);
                startActivity(s);
            }
        });

        clCartSubAgen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(getActivity(), FilterCustomer.class);
                s.putExtra("MODE", MODE_FILTER_CUSTOMER_CART);
                startActivity(s);
            }
        });
    }

    public void LoadData(){
        if (EthicaApplication.getInstance().getCustomerSeqGlobal() > 0){
            String url = Routes.url_get_master_customer+"/"+EthicaApplication.getInstance().getCustomerSeqGlobal();
            JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    for (int i = 0; i < response.length(); i++) {
                        try { JSONObject obj = response.getJSONObject(i);
                            if (obj.getInt("seq") > 0){
                                txtNama.setText(obj.getString("nama"));
//                                if (EthicaApplication.getInstance().getTipeCust().equals(TIPE_CUSTOMER_ETHICA)){
//                                    txtUserId.setText(obj.getString("user_id_ethica"));
//                                }else if (EthicaApplication.getInstance().getTipeCust().equals(TIPE_CUSTOMER_SEPLY)){
//                                    txtUserId.setText(obj.getString("user_id_seply"));
//                                }
                                txtUserId.setText(EthicaApplication.getInstance().getUserIdGlobal());
                                String Alamat = "";
                                if (!obj.getString("alamat").equals("")) {Alamat = Alamat +obj.getString("alamat");}
                                if (!obj.getString("kecamatan").equals("")) {Alamat = Alamat +", "+obj.getString("kecamatan");}
                                if (!obj.getString("kota").equals("")) {Alamat = Alamat +", "+obj.getString("kota");}
                                if (!obj.getString("provinsi").equals("")) {Alamat = Alamat +", "+obj.getString("provinsi");}
                                txtAlamat.setText(Alamat);
                                txtEmail.setText(obj.getString("email"));
                                txtNoTelpon.setText(obj.getString("no_telp"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
                }
            });
            EthicaApplication.getInstance().addToRequestQueue(jArr);
        }

    }

    public void get_count_pesanan_sub_agen() {
        if (CardAgen.getVisibility() == View.VISIBLE){
            if (EthicaApplication.getInstance().getCustomerSeqGlobal() > 0) {
                String url = Routes.url_get_count_pesanan_sub_agen + "?customer_seq=" + EthicaApplication.getInstance().getCustomerSeqGlobal()+"&status="+STATUS_ORDER_BELUM_APPROVE;
                JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        txtJmlOrderList.setVisibility(View.VISIBLE);
                        for (int i = 0; i < response.length(); i++) {
                            try { JSONObject obj = response.getJSONObject(i);
                                if (obj.getInt("jumlah") > 0){
                                    txtJmlOrderList.setText(" "+obj.getString("jumlah")+" ");
                                }else{
                                    txtJmlOrderList.setText("");
                                    txtJmlOrderList.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                txtJmlOrderList.setText("");
                                txtJmlOrderList.setVisibility(View.GONE);
                            }
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        txtJmlOrderList.setText("");
                        txtJmlOrderList.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
                    }
                });
                EthicaApplication.getInstance().addToRequestQueue(jArr);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        get_count_pesanan_sub_agen();
    }
}
