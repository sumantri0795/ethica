package bandung.orion.com.ethica.form.pre_order;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.product_list.DotIndicatorPagerAdapter;
import bandung.orion.com.ethica.form.product_list.ProductListModel;
import bandung.orion.com.ethica.form.product_list.UrutkanModel;
import bandung.orion.com.ethica.form.product_list.filter.FilterProductList;
import bandung.orion.com.ethica.form.product_list.filter.UrutkanPreOrderAdapter;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDateFmt;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.calculateWidth.calculateNoOfColumns;
import static java.lang.Math.round;

public class PreOrderList extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener{
    public Dialog DialogFilter;
    public RecyclerView RcvProductList;
    private RecyclerView RcvUrutkan;
    public PreOrderListAdapter mAdapter;
    private UrutkanPreOrderAdapter mAdapterUrutkan;
    private ImageView imgCart;

    private ImageButton btnFilter, btnUrutkan;
    private int mMaxScrollSize;
    private AppBarLayout appbarLayout;
    private SwipeRefreshLayout swipe;
    public static TextView txtFilter, txtUrutkan, txtJmlCart;
    private SearchView txtSearch;

    int visibleThreshold = 6;
    int lastVisibleItem, totalItemCount;
    boolean IsLoading;
    ILoadMore loadMore;

    private LinearLayoutManager linearLayoutManager;
    public List<ProductListModel> ListItems = new ArrayList<>();
    private List<UrutkanModel> ListItemsUrutkan = new ArrayList<>();

    private final String TEXT_URUTKAN_TANGGAL_NAMA = "tgl_release%20DESC%20,%20Nama%20ASC";
    private final String TEXT_URUTKAN_NAMA     = "Nama";
    private final String TEXT_URUTKAN_TERBARU  = "Terbaru";
    private final String TEXT_URUTKAN_TERMURAH = "Termurah";
    private final String TEXT_URUTKAN_TERMAHAL = "Termahal";
    private final String TEXT_URUTKAN_TERBANYAK = "Terbanyak";

    private final String TEXT_SEMUA = "SEMUA";

    public String OrderBy = "";

    private final String MODE_TAMPILAN_GRID = "G";
    private final String MODE_TAMPILAN_LIST = "L";

    //Filter
    String BRAND, SUB_BRAND, WARNA, TAHUN;
    private String ModeTampilan;
    private int NoOfColumns;
    private DotIndicatorPagerAdapter adapter;
    private int CountAllVew;
    final List<View> pageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_order_list);
        CreateView();
        InitClass();
        EventClass();
        updateJumlahKeranjang();
        LoadData();
    }

    private void CreateView(){
        this.btnFilter = (ImageButton) findViewById(R.id.btnFilter);
        this.btnUrutkan = (ImageButton) findViewById(R.id.btnUrutkan);
        this.RcvProductList = (RecyclerView) findViewById(R.id.rcvLoad);
        this.appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        this.swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        this.txtFilter = (TextView) findViewById(R.id.txtFilter);
        this.txtUrutkan = (TextView) findViewById(R.id.txtUrutkan);
        this.txtSearch = (SearchView) findViewById(R.id.txtSearch);
        this.imgCart = (ImageView) findViewById(R.id.imgCart);
        this.txtJmlCart = (TextView) findViewById(R.id.txtJmlCart);
    }

    private void InitClass(){
        ModeTampilan = MODE_TAMPILAN_GRID;
        NoOfColumns = calculateNoOfColumns(PreOrderList.this);
        SetJenisTampilan();
        ListItemsUrutkan.clear();
        ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_NAMA, true));
        ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_TERMURAH, false));
        ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_TERMAHAL, false));
        //ListItemsUrutkan.add(new UrutkanModel(TEXT_URUTKAN_TERBANYAK, false));

        this.DialogFilter = new Dialog(PreOrderList.this);
        this.DialogFilter.setContentView(R.layout.urutkan_product_list);
        this.RcvUrutkan = (RecyclerView) DialogFilter.findViewById(R.id.RcvUrutkan);

        RcvUrutkan.setLayoutManager(new LinearLayoutManager(PreOrderList.this));
        mAdapterUrutkan = new UrutkanPreOrderAdapter(PreOrderList.this, ListItemsUrutkan, this);
        RcvUrutkan.setAdapter(mAdapterUrutkan);

        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();
        Toolbar toolbar = (Toolbar) findViewById(R.id.ToolbarAct);
        this.setSupportActionBar(toolbar);

        BRAND     = TEXT_SEMUA;
        SUB_BRAND = TEXT_SEMUA;
        WARNA     = TEXT_SEMUA;
        TAHUN     = TEXT_SEMUA;

        OrderBy   = TEXT_URUTKAN_TANGGAL_NAMA;
        CountAllVew = 0;
    }

    protected void EventClass(){
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!IsLoading){
                    mAdapter.removeAllModel();
                    RefreshRecyclerView();
                }
                swipe.setRefreshing(false);
            }
        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                mAdapter.addMoel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.removeMoel(ListItems.size()-1);
                        LoadData();
                    }
                },1000);
            }
        });

        RcvProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

        txtSearch.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        txtSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }
        });

        btnUrutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFilter.show();
            }
        });

        txtUrutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFilter.show();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent s = new Intent(PreOrderList.this, FilterProductList.class);
                s.putExtra("BRAND", BRAND);
                s.putExtra("SUB_BRAND", SUB_BRAND);
                s.putExtra("WARNA", WARNA);
                s.putExtra("TAHUN", TAHUN);
                startActivityForResult(s, 1);
            }
        });

        txtFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent s = new Intent(PreOrderList.this, FilterProductList.class);
                s.putExtra("BRAND", BRAND);
                s.putExtra("SUB_BRAND", SUB_BRAND);
                s.putExtra("WARNA", WARNA);
                s.putExtra("TAHUN", TAHUN);
                startActivityForResult(s, 1);
            }
        });

        imgCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EthicaApplication.getInstance().getCustomerSeqGlobal() > 0 ){
                    EthicaApplication.getInstance().KeCart = true;
                    finish();
                }
            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0){
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        }
    }

    public void LoadData(){
        String Filter = "";

        Filter += "?brand=" + BRAND + "&sub_brand=" + SUB_BRAND + "&warna=" + WARNA + "&tahun=" + TAHUN +
                "&order_by=" + OrderBy+ "&search=" + txtSearch.getQuery() + "&offset=" + Integer.toString(mAdapter.getItemCount())+
                "&tipe_cust=" + EthicaApplication.getInstance().getTipeCust()+"&is_pre_order=T"+
                "&is_ethica=" + EthicaApplication.getInstance().getIsEthicaGlobal()+
                "&is_seply=" + EthicaApplication.getInstance().getIsSeplyGlobal()+
                "&is_ethica_hijab=" + EthicaApplication.getInstance().getIsEthicaHijabGlobal();

        Filter  = Filter.replace(TEXT_SEMUA,"");
        String url = Routes.url_load_barang + Filter;
        url = url.replace(" ", "%20");
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<ProductListModel> itemDataModels = new ArrayList<>();
                ProductListModel Data;
                itemDataModels.clear();
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            Data = new ProductListModel(
                                    obj.getInt("seq"),
                                    obj.getString("barcode"),
                                    obj.getString("nama"),
                                    obj.getString("brand"),
                                    obj.getString("sub_brand"),
                                    obj.getString("warna"),
                                    obj.getString("ukuran"),
                                    obj.getString("katalog"),
                                    getMillisDate(obj.getString("tahun")),
                                    obj.getString("tipe_brg"),
                                    obj.getString("gambar"),
                                    obj.getDouble("harga"),
                                    getMillisDate(obj.getString("tgl_hapus")),
                                    obj.getDouble("stok"),
                                    0,
                                    obj.getString("gambar_besar"),
                                    obj.getString("keterangan")
                            );

                            Data.setIs_ehijab(obj.getString("is_ehijab"));
                            Data.setIs_preorder(obj.getString("is_preorder"));
                            Data.setBerlaku_dari(getMillisDateFmt(obj.getString("berlaku_dari"), "yyyy-MM-dd"));
                            Data.setBerlaku_sampai(getMillisDateFmt(obj.getString("berlaku_sampai"), "yyyy-MM-dd"));
                            Data.setTgl_release(getMillisDateFmt(obj.getString("tgl_release"), "yyyy-MM-dd"));

                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(PreOrderList.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(false);
                    }
                }
                mAdapter.addModels(itemDataModels);
                setLoaded(false);
                swipe.setRefreshing(false);
                if (ListItems.size() <= 0){Toast.makeText(PreOrderList.this, "Tidak ditemukan", Toast.LENGTH_SHORT).show();}
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                swipe.setRefreshing(false);
                setLoaded(false);
                Toast.makeText(PreOrderList.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void updateJumlahKeranjang(){
        if (EthicaApplication.getInstance().getCustomerSeqGlobal() > 0){
            String url = Routes.url_get_count_cart+"/"+EthicaApplication.getInstance().getCustomerSeqGlobal()+
                    "?tipe_customer="+EthicaApplication.getInstance().getTipeCust()+
                    "&user_id="+EthicaApplication.getInstance().getUserIdGlobal()+
                    "&versi="+EthicaApplication.getInstance().getVersionCode();
            JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    for (int i = 0; i < response.length(); i++) {
                        try { JSONObject obj = response.getJSONObject(i);
                            if (obj.getInt("total") > 0){
                                txtJmlCart.setText(" "+obj.getString("total")+" ");
                                txtJmlCart.setVisibility(View.VISIBLE);
                            }else{
                                txtJmlCart.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            txtJmlCart.setVisibility(View.GONE);
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError e) {
                    txtJmlCart.setVisibility(View.GONE);
                }
            });
            EthicaApplication.getInstance().addToRequestQueue(jArr);
        }else{
            txtJmlCart.setVisibility(View.GONE);
        }
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extra = data.getExtras();
                BRAND     = extra.getString("BRAND");
                SUB_BRAND = extra.getString("SUB_BRAND");
                WARNA     = extra.getString("WARNA");
                TAHUN     = extra.getString("TAHUN");
                RcvProductList.scrollToPosition(0);
                mAdapter.removeAllModel();
                RefreshRecyclerView();
            }
        }
    }

    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }

    public void SetTextUrutkan(String Text){
        txtUrutkan.setText(Text);
    }

    public void SetTextStyle(int style){
        txtUrutkan.setTypeface(null, style);
    }

    private void SetJenisTampilan(){
        if (ModeTampilan.equals(MODE_TAMPILAN_GRID)){
            mAdapter = new PreOrderListAdapter(PreOrderList.this, ListItems, this, R.layout.list_item_pre_order_list_grid);
            RcvProductList.setLayoutManager(new GridLayoutManager(PreOrderList.this, NoOfColumns, GridLayoutManager.VERTICAL, false));
            linearLayoutManager = (LinearLayoutManager)RcvProductList.getLayoutManager();
            RcvProductList.setAdapter(mAdapter);
        }
    }
}