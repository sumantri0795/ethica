package bandung.orion.com.ethica.form.cart;

public class CartModel {
    private long seq;
    private long customer_seq;
    private long barang_seq;
    private double qty;
    //langsung ambil tanpa save word
    private String kode;
    private String nama;
    private double harga;
    private String gambar;
    private double harga_diskon;
    private String alamat_kirim;
    private String alamat_pengirim;
    private int tipe_view;

    private String nama_pengirim;
    private String alamat_pengirim_lengkap;
    private String no_telepon_pengirim;

    //field baru
    private double qty_order, berat, berat_total;
    private String is_preorder;

    private String user_id;
    private String nama_kirim, kelurahan, provinsi, kota, kecamatan, alamat_kirim_lengkap, no_telepon_kirim;
    private int id_kota, id_provinsi, id_kecamatan;
    private Double ongkos_kirim;
    private String ekspedisi;
    private String service;
    private long ekspedisi_seq;
    private Double subTotal;
    private Double diskon;
    private Double totalBerat;
    private Double total;
    private String ada_setting;
    private Double max_diskon_setting;
    private String NoResi;



    public CartModel(long customer_seq, long barang_seq, double qty, String kode, String nama, double harga, String gambar, double harga_diskon,
                     String user_id, String nama_kirim, String kelurahan, String provinsi, String kota, String kecamatan, String alamat_kirim_lengkap,
                     String no_telepon_kirim, Double ongkos_kirim, String ekspedisi, String service, long ekspedisi_seq) {
        this.customer_seq = customer_seq;
        this.barang_seq = barang_seq;
        this.qty = qty;
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
        this.gambar = gambar;
        this.harga_diskon = harga_diskon;
        this.alamat_kirim = "";
        this.alamat_pengirim = "";
        this.tipe_view = 0;
        this.nama_pengirim = "";
        this.alamat_pengirim_lengkap = "";
        this.no_telepon_pengirim = "";
        this.qty_order = 0;
        this.berat = 0;
        this.berat_total = 0;
        this.ada_setting = "";
        this.max_diskon_setting = 0.0;
        this.NoResi = "";
    }


    public CartModel() {
        this.customer_seq = 0;
        this.barang_seq = 0;
        this.qty = 0;
        this.kode = "";
        this.nama = "";
        this.harga = 0;
        this.gambar = "";
        this.harga_diskon = 0;
        this.alamat_kirim = "";
        this.alamat_pengirim = "";
        this.tipe_view = 0;
        this.nama_pengirim = "";
        this.alamat_pengirim_lengkap = "";
        this.no_telepon_pengirim = "";
        this.qty_order = 0;
        this.berat = 0;
        this.berat_total = 0;
        this.is_preorder = "";
        this.ada_setting = "";
        this.max_diskon_setting = 0.0;
        this.NoResi = "";
    }

    public long getCustomer_seq() {
        return customer_seq;
    }

    public void setCustomer_seq(long customer_seq) {
        this.customer_seq = customer_seq;
    }

    public long getBarang_seq() {
        return barang_seq;
    }

    public void setBarang_seq(long barang_seq) {
        this.barang_seq = barang_seq;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setHarga_diskon(double harga_diskon) {
        this.harga_diskon = harga_diskon;
    }

    public double getHarga() {
        return harga;
    }

    public String getGambar() {
        return gambar.replace(" ", "%20");
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public void setHarga(double harga) {
        this.harga = harga;


    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public double getHarga_diskon() {
        return harga_diskon;
    }

    public String getAlamat_kirim() {
        return alamat_kirim;
    }

    public void setAlamat_kirim(String alamat_kirim) {
        this.alamat_kirim = alamat_kirim;
    }

    public int getTipe_view() {
        return tipe_view;
    }

    public void setTipe_view(int tipe_view) {
        this.tipe_view = tipe_view;
    }

    public String getAlamat_pengirim() {
        return alamat_pengirim;
    }

    public void setAlamat_pengirim(String alamat_pengirim) {
        this.alamat_pengirim = alamat_pengirim;
    }

    public String getNama_pengirim() {
        return nama_pengirim;
    }

    public void setNama_pengirim(String nama_pengirim) {
        this.nama_pengirim = nama_pengirim;
    }

    public String getAlamat_pengirim_lengkap() {
        return alamat_pengirim_lengkap;
    }

    public void setAlamat_pengirim_lengkap(String alamat_pengirim_lengkap) {
        this.alamat_pengirim_lengkap = alamat_pengirim_lengkap;
    }

    public String getNo_telepon_pengirim() {
        return no_telepon_pengirim;
    }

    public void setNo_telepon_pengirim(String no_telepon_pengirim) {
        this.no_telepon_pengirim = no_telepon_pengirim;
    }

    public double getQty_order() {
        return qty_order;
    }

    public void setQty_order(double qty_order) {
        this.qty_order = qty_order;
    }

    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }

    public double getBerat_total() {
        return berat_total;
    }

    public void setBerat_total(double berat_total) {
        this.berat_total = berat_total;
    }

    public String getIs_preorder() {
        return is_preorder;
    }

    public void setIs_preorder(String is_preorder) {
        this.is_preorder = is_preorder;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNama_kirim() {
        return nama_kirim;
    }

    public void setNama_kirim(String nama_kirim) {
        this.nama_kirim = nama_kirim;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getAlamat_kirim_lengkap() {
        return alamat_kirim_lengkap;
    }

    public void setAlamat_kirim_lengkap(String alamat_kirim_lengkap) {
        this.alamat_kirim_lengkap = alamat_kirim_lengkap;
    }

    public String getNo_telepon_kirim() {
        return no_telepon_kirim;
    }

    public void setNo_telepon_kirim(String no_telepon_kirim) {
        this.no_telepon_kirim = no_telepon_kirim;
    }

    public int getId_kota() {
        return id_kota;
    }

    public void setId_kota(int id_kota) {
        this.id_kota = id_kota;
    }

    public int getId_provinsi() {
        return id_provinsi;
    }

    public void setId_provinsi(int id_provinsi) {
        this.id_provinsi = id_provinsi;
    }

    public int getId_kecamatan() {
        return id_kecamatan;
    }

    public void setId_kecamatan(int id_kecamatan) {
        this.id_kecamatan = id_kecamatan;
    }

    public Double getOngkos_kirim() {
        return ongkos_kirim;
    }

    public void setOngkos_kirim(Double ongkos_kirim) {
        this.ongkos_kirim = ongkos_kirim;
    }

    public String getEkspedisi() {
        return ekspedisi;
    }

    public void setEkspedisi(String ekspedisi) {
        this.ekspedisi = ekspedisi;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public long getEkspedisi_seq() {
        return ekspedisi_seq;
    }

    public void setEkspedisi_seq(long ekspedisi_seq) {
        this.ekspedisi_seq = ekspedisi_seq;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getDiskon() {
        return diskon;
    }

    public void setDiskon(Double diskon) {
        this.diskon = diskon;
    }

    public Double getTotalBerat() {
        return totalBerat;
    }

    public void setTotalBerat(Double totalBerat) {
        this.totalBerat = totalBerat;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getAda_setting() {
        return ada_setting;
    }

    public void setAda_setting(String ada_setting) {
        this.ada_setting = ada_setting;
    }

    public Double getMax_diskon_setting() {
        return max_diskon_setting;
    }

    public void setMax_diskon_setting(Double max_diskon_setting) {
        this.max_diskon_setting = max_diskon_setting;
    }

    public String getNoResi() {
        return NoResi;
    }

    public void setNoResi(String noResi) {
        NoResi = noResi;
    }
}
