package bandung.orion.com.ethica.form.product_list.filter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.pre_order.PreOrderList;
import bandung.orion.com.ethica.form.product_list.UrutkanModel;

public class UrutkanPreOrderAdapter extends RecyclerView.Adapter {
    Context context;
    List<UrutkanModel> items;
    PreOrderList Activity;

    private final String TEXT_URUTKAN_NAMA     = "Nama";
    private final String TEXT_URUTKAN_TERBARU  = "Terbaru";
    private final String TEXT_URUTKAN_TERMURAH = "Termurah";
    private final String TEXT_URUTKAN_TERMAHAL = "Termahal";
    private final String TEXT_URUTKAN_TERBANYAK = "Terbanyak";

    public UrutkanPreOrderAdapter(Context context, List<UrutkanModel> item, PreOrderList AppCompatActivity) {
        this.context = context;
        this.Activity = AppCompatActivity;
        this.items = item;
    }

    public void addModels(List<UrutkanModel> items) {
        int pos = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(pos, items.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.list_item_urutkan, parent, false);
        return new UrutkanPreOrderAdapter.ItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final UrutkanModel item = items.get(position);
        final ItemHolder itemHolder = (ItemHolder) holder;

        itemHolder.txtUrutkan.setText(item.getNama());
        if (item.isPilih() == true){
            itemHolder.imgCeklis.setVisibility(View.VISIBLE);
        }else{
            itemHolder.imgCeklis.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Activity.mAdapter.removeAllModel();

                for (int i= 0; i < items.size(); i++){
                    if (i != position){
                        items.get(i).setPilih(false);
                    }else{
                        items.get(i).setPilih(true);
                    }
                }
                item.setPilih(true);

                if (item.isPilih() == true){
                    itemHolder.imgCeklis.setVisibility(View.VISIBLE);
                }else{
                    itemHolder.imgCeklis.setVisibility(View.INVISIBLE);
                }
                notifyDataSetChanged();
                Activity.OrderBy = SetFilter(item.getNama());
                Activity.DialogFilter.dismiss();
                if (!itemHolder.txtUrutkan.getText().equals("SEMUA")){
                    Activity.SetTextUrutkan(item.getNama());
                    Activity.SetTextStyle(Typeface.BOLD);
                }else{
                    Activity.SetTextUrutkan("Urutkan");
                    Activity.SetTextStyle(Typeface.NORMAL);
                }
                Activity.RcvProductList.smoothScrollToPosition(1);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtUrutkan;
        ImageView imgCeklis;

        public ItemHolder(View itemView) {
            super(itemView);
            txtUrutkan = itemView.findViewById(R.id.txtUrutkan);
            imgCeklis = itemView.findViewById(R.id.imgCeklis);
        }
    }

    private String SetFilter(String text_urutkan){
        String hasil = "";
        if (text_urutkan.equals(TEXT_URUTKAN_NAMA)){
            hasil =  "nama";
        }else if(text_urutkan.equals(TEXT_URUTKAN_TERMURAH)){
            hasil =  "harga%20ASC";
        }else if(text_urutkan.equals(TEXT_URUTKAN_TERMAHAL)){
            hasil =  "harga%20DESC";
        }else if(text_urutkan.equals(TEXT_URUTKAN_TERBANYAK)){
            hasil =  "stok%20DESC";
        }
        return hasil;
    }
}
