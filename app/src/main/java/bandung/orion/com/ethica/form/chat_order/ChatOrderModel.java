package bandung.orion.com.ethica.form.chat_order;

public class ChatOrderModel {
    int seq, pesanan_seq;
    long tanggal;
    String isi_pesan, tipe_pesan, user_id, is_baca, is_execute;

    public ChatOrderModel(int seq, int pesanan_seq, long tanggal, String isi_pesan, String tipe_pesan, String user_id, String is_baca, String is_execute) {
        this.seq = seq;
        this.pesanan_seq = pesanan_seq;
        this.tanggal = tanggal;
        this.isi_pesan = isi_pesan;
        this.tipe_pesan = tipe_pesan;
        this.user_id = user_id;
        this.is_baca = is_baca;
        this.is_execute = is_execute;
    }

    public ChatOrderModel() {
        this.seq = 0;
        this.pesanan_seq = 0;
        this.tanggal = 0;
        this.isi_pesan = "";
        this.tipe_pesan = "";
        this.user_id = "";
        this.is_baca = "";
        this.is_execute = "";
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getPesanan_seq() {
        return pesanan_seq;
    }

    public void setPesanan_seq(int pesanan_seq) {
        this.pesanan_seq = pesanan_seq;
    }

    public long getTanggal() {
        return tanggal;
    }

    public void setTanggal(long tanggal) {
        this.tanggal = tanggal;
    }

    public String getIsi_pesan() {
        return isi_pesan;
    }

    public void setIsi_pesan(String isi_pesan) {
        this.isi_pesan = isi_pesan;
    }

    public String getTipe_pesan() {
        return tipe_pesan;
    }

    public void setTipe_pesan(String tipe_pesan) {
        this.tipe_pesan = tipe_pesan;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIs_baca() {
        return is_baca;
    }

    public void setIs_baca(String is_baca) {
        this.is_baca = is_baca;
    }

    public String getIs_execute() {
        return is_execute;
    }

    public void setIs_execute(String is_execute) {
        this.is_execute = is_execute;
    }
}
