package bandung.orion.com.ethica.form.Ekspedisi;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.alamat_kirim.InputAlamatKirimCartActivity;
import bandung.orion.com.ethica.form.cart.CartFragment;
import bandung.orion.com.ethica.form.cart.CartModel;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT_PENGIRIMAN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_EKSPEDISI;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ITEM;

public class PilihEkspedisiModel {
    int seq;
    String kode, nama, service, deskripsi;


    public PilihEkspedisiModel(int seq, String kode, String nama, String service, String deskripsi) {
        this.seq = seq;
        this.nama = nama;
        this.kode = kode;
        this.service = service;
        this.deskripsi = deskripsi;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
