package bandung.orion.com.ethica.form.order_detail;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.cart.Cart;
import bandung.orion.com.ethica.form.cart.CartAdapter;
import bandung.orion.com.ethica.form.cart.CartFragment;
import bandung.orion.com.ethica.form.cart.CartModel;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MODE_DETAIL_ORDER_APPROVE;
import static bandung.orion.com.ethica.utility.JConst.MODE_DETAIL_ORDER_DETAIL;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_APPROVE;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_KEEP;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT_PENGIRIMAN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_EKSPEDISI;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ITEM;

public class OrderDetail extends AppCompatActivity {

    private RecyclerView rcvData;
    private TextView txtTglOrder, txtNoOrder, txtTotal, txtDiskon, txtSubtotal, txtRPTotal, txtRPDiskon, txtRPSubtotal, txtLabelSubtotal, txtLabelDiskon;
    private CardView cardButton;
    private ArrayList<OrderDetailModel> datas;
    private OrderDetailAdapter adapter;
    private long seqMst;
    private Button btnOrder, btnHapus, btnApprove;
    private TextView txtKg, txtBerat, txtRPOngkir, txtOngkir;
    private String mode;
    private ProgressDialog Loading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        this.setTitle("Order Detail");
        CreateView();
        InitClass();
        EventClass();
        load();
    }

    private void CreateView(){
        txtTotal = (TextView) findViewById(R.id.txtTotalMst);
        txtNoOrder = (TextView) findViewById(R.id.txtNoOrder);
        txtTglOrder = (TextView) findViewById(R.id.txtTglOrder);
        txtDiskon = (TextView) findViewById(R.id.txtDiskon);
        txtSubtotal = (TextView) findViewById(R.id.txtSubtotal);
        txtLabelDiskon = (TextView) findViewById(R.id.txtLabelDiskon);
        txtLabelSubtotal = (TextView) findViewById(R.id.txtLabelSubtotal);
        txtRPTotal = (TextView) findViewById(R.id.txtRPTotalMst);
        txtRPDiskon = (TextView) findViewById(R.id.txtRPDiskon);
        txtRPSubtotal = (TextView) findViewById(R.id.txtRPSubtotal);
        txtKg = (TextView) findViewById(R.id.txtKg);
        txtRPSubtotal = (TextView) findViewById(R.id.txtRPSubtotal);

        txtKg = (TextView) findViewById(R.id.txtKg);
        txtBerat = (TextView) findViewById(R.id.txtBerat);
        txtRPOngkir = (TextView) findViewById(R.id.txtRPOngkir);
        txtOngkir = (TextView) findViewById(R.id.txtOngkir);

        btnOrder = (Button) findViewById(R.id.btnOrder);
        btnHapus = (Button) findViewById(R.id.btnHapus);
        btnApprove = (Button) findViewById(R.id.btnApprove);

        cardButton = (CardView) findViewById(R.id.cardButton);

        rcvData = (RecyclerView) findViewById(R.id.rcvLoad);
        rcvData.setLayoutManager(new LinearLayoutManager(this));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        datas = new ArrayList<OrderDetailModel>();
        this.adapter = new OrderDetailAdapter(this);
        rcvData.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void InitClass(){
        txtTotal.setText("Total : 0 ");
        Bundle extra = this.getIntent().getExtras();
        seqMst = extra.getInt("seq");
        mode   = extra.getString("mode");
        if (seqMst == 0){
            seqMst = extra.getLong("seq");
        }
        this.Loading = new ProgressDialog(OrderDetail.this);
    }


    // untuk menampilkan semua data pada listview
    private void load(){
        Loading.setMessage("Loading...");
        Loading.setCancelable(false);
        Loading.show();

        datas.clear();
        adapter.notifyDataSetChanged();

        // membuat request JSON
        String url = Routes.url_get_pesanan_master+"/"+String.valueOf(seqMst);
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                datas.clear();

                for (int i = 0; i < response.length(); i++) {
                    try {
                        //SELECT m.tanggal, m.nomor, m.customer_seq, m.total, m.status, b.seq as brgseq, b.barcode, b.nama as nama,
                        // b.barcode as kode, b.gambar, d.harga as harga, d.seq as seq, d.barang_seq as barang_seq,  d.qty as qty, d.customer_seq,
                        // d.total FROM pesanan_master m, pesanan_detail d, master_barang b where m.seq = d.master_seq and b.seq = d.barang_seq and m.seq = :id');
                        JSONObject obj = response.getJSONObject(i);
                        OrderDetailModel item = new OrderDetailModel();
                        if (i == 0){
                            txtTglOrder.setText(": "+obj.getString("tanggal"));
                            txtNoOrder.setText(": "+obj.getString("nomor"));
                            String status = obj.getString("status");
                            cardButton.setVisibility(View.GONE);
                            if (mode.equals(MODE_DETAIL_ORDER_DETAIL) && (obj.getString("status").equals(STATUS_ORDER_KEEP)) ){
                                cardButton.setVisibility(View.VISIBLE);
                                btnApprove.setVisibility(View.GONE);
                                btnHapus.setVisibility(View.VISIBLE);
                                btnOrder.setVisibility(View.VISIBLE);
                            }

                            if (mode.equals(MODE_DETAIL_ORDER_APPROVE) && (obj.getString("status").equals(STATUS_ORDER_BELUM_APPROVE))){
                                cardButton.setVisibility(View.VISIBLE);
                                btnHapus.setVisibility(View.GONE);
                                btnOrder.setVisibility(View.GONE);
                            }

                            txtTotal.setText(FungsiGeneral.FloatToStrFmt(obj.getDouble("totalmst"), false));
                            //120dp
                            double disk, subtotal;
                            disk = obj.getDouble("diskonmst");
                            subtotal = obj.getDouble("subtotalmst");
                            double berat = obj.getDouble("total_berat");
                            double ongkir = obj.getDouble("ongkos_kirim");

                            txtBerat.setText(": "+FungsiGeneral.FloatToStrFmt(berat, false));
                            txtOngkir.setText(FungsiGeneral.FloatToStrFmt(ongkir, false));
                            txtKg.setText("Kg");
                            txtSubtotal.setText(FungsiGeneral.FloatToStrFmt(subtotal, false));

                            if (disk != 0){
                                txtDiskon.setVisibility(View.VISIBLE);
                                //txtSubtotal.setVisibility(View.VISIBLE);
                                txtLabelDiskon.setVisibility(View.VISIBLE);
                                //txtLabelSubtotal.setVisibility(View.VISIBLE);
                                txtDiskon.setText(FungsiGeneral.FloatToStrFmt(disk, false));
                                txtRPDiskon.setVisibility(View.VISIBLE);
                                //txtRPSubtotal.setVisibility(View.VISIBLE);
                            }else{
                                txtDiskon.setVisibility(View.GONE);
                                //txtSubtotal.setVisibility(View.GONE);

                                txtLabelDiskon.setVisibility(View.GONE);
                                //txtLabelSubtotal.setVisibility(View.GONE);
                                txtRPDiskon.setVisibility(View.GONE);
                                //txtRPSubtotal.setVisibility(View.GONE);
                            }
                            OrderDetailModel data = new OrderDetailModel();
                            data.setAlamat_kirim(obj.getString("alamat_kirim"));
                            data.setTipe_view(TIPE_VIEW_ALAMAT);
                            datas.add(data);
                            OrderDetailModel data2 = new OrderDetailModel();
                            data2.setAlamat_pengirim(obj.getString("alamat_pengirim"));
                            data2.setTipe_view(TIPE_VIEW_ALAMAT_PENGIRIMAN);
                            datas.add(data2);


                            OrderDetailModel data3 = new OrderDetailModel();
                            data3.setEkspedisi(obj.getString("ekspedisi"));
                            data3.setService(obj.getString("service"));
                            data3.setNo_resi(obj.getString("no_resi"));
                            data3.setTipe_view(TIPE_VIEW_EKSPEDISI);
                            datas.add(data3);
                        }
                        item.setBarang_seq(obj.getLong("brg_seq"));
                        item.setCustomer_seq(obj.getLong("customer_seq"));
                        item.setHarga(obj.getDouble("harga"));
                        item.setKode(obj.getString("kode"));
                        item.setNama(obj.getString("nama"));
                        item.setQty(obj.getDouble("qty"));
                        item.setGambar(obj.getString("gambar"));
                        item.setSeq(obj.getLong("seq"));
                        item.setDiskon(obj.getDouble("diskon"));
                        item.setGambar_besar(obj.getString("gambar_besar"));
                        item.setHarga_diskon(item.getHarga() - item.getDiskon());
                        item.setTipe_view(TIPE_VIEW_ITEM);
                        item.setKeterangan_gambar(obj.getString("keterangan_barang"));
                        item.setBerat_total(obj.getDouble("berat_total"));
                        datas.add(item);
                        Loading.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Loading.dismiss();
                    }
                }

                OrderDetail.this.adapter.addModels(datas);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
                Loading.dismiss();
            }
        });

        // menambah request ke request queue
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    private void EventClass(){
        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog = new AlertDialog.Builder(OrderDetail.this).create();
                dialog.setMessage("Pesanan akan diapprove?");
                dialog.setCancelable(true);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {
                        OrderDetail.this.Approve();
                    }
                });

                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {

                    }
                });
                dialog.show();
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = new AlertDialog.Builder(OrderDetail.this).create();
                dialog.setMessage("Apakah data sudah benar?");
                dialog.setCancelable(true);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {
                        OrderDetail.this.CommitDataMst();
                    }
                });

                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {

                    }
                });
                dialog.setIcon(android.R.drawable.ic_dialog_alert);
                dialog.show();
            }
        });

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = new AlertDialog.Builder(OrderDetail.this).create();
                dialog.setMessage("Apakah data akan hapus");
                dialog.setCancelable(true);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {
                        OrderDetail.this.DeleteDataMst();
                    }
                });

                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {

                    }
                });
                dialog.setIcon(android.R.drawable.ic_dialog_alert);
                dialog.show();
            }
        });
    }


    private void CommitDataMst(){
        String url;
        url = Routes.url_commit_keep_pesanan_master+"?key="+EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                        inform(OrderDetail.this, MSG_HARUS_UPDATE, "");
                        return;
                    }

                    if (jObj.getString("status").equals("status off")){
                        inform(OrderDetail.this, "Untuk sementara tidak dapat melakukan order", "");
                        return;
                    }

                    if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)){
                        Toast.makeText(OrderDetail.this,"Data berhasil diorder", Toast.LENGTH_SHORT).show();
                        Intent intent = getIntent();
                        setResult(RESULT_OK, intent);
                        finish();
                        //onBackPressed();
                    }else{
                        Toast.makeText(OrderDetail.this, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(OrderDetail.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrderDetail.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("seq", String.valueOf(seqMst));
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };
        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
    }

    private void Approve(){
        String url;
        url = Routes.url_approve_pesanan_master+"?key="+EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                        inform(OrderDetail.this, MSG_HARUS_UPDATE, "");
                        return;
                    }

                    if (jObj.getString("status").equals("status off")){
                        inform(OrderDetail.this, "Untuk sementara tidak dapat melakukan approve", "");
                        return;
                    }

                    if (jObj.getString("status").equals("sudah approve")){
                        inform(OrderDetail.this, "Pesanan sudah diapprove", "");
                        return;
                    }

                    if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)){
                        Toast.makeText(OrderDetail.this,"Berhasil diapprove", Toast.LENGTH_SHORT).show();
                        Intent intent = getIntent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }else{
                        Toast.makeText(OrderDetail.this, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(OrderDetail.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrderDetail.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("seq", String.valueOf(seqMst));
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };
        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
    }

    private void DeleteDataMst(){
        String url;
        url = Routes.url_delete_keep_pesanan_master+"?key="+EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals("status off")){
                        inform(OrderDetail.this, "Untuk sementara tidak dapat menghapus", "");
                        return;
                    }

                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                        inform(OrderDetail.this, MSG_HARUS_UPDATE, "");
                        return;
                    }

                    if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)){
                        Toast.makeText(OrderDetail.this,"Data berhasil dihapus", Toast.LENGTH_SHORT).show();
                        Intent intent = getIntent();
                        setResult(RESULT_OK, intent);
                        finish();
                        //onBackPressed();
                    }else{
                        Toast.makeText(OrderDetail.this, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(OrderDetail.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrderDetail.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("seq", String.valueOf(seqMst));
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };

        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }
}