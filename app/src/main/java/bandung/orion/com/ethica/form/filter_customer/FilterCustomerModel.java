package bandung.orion.com.ethica.form.filter_customer;

public class FilterCustomerModel {
    int seq;
    String nama, no_telpon, alamat;


    public FilterCustomerModel(int seq, String nama, String no_telpon, String alamat) {
        this.seq = seq;
        this.nama = nama;
        this.no_telpon = no_telpon;
        this.alamat = alamat;
    }

    public FilterCustomerModel() {
        this.seq = 0;
        this.nama = "";
        this.no_telpon = "";
        this.alamat = "";
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_telpon() {
        return no_telpon;
    }

    public void setNo_telpon(String no_telpon) {
        this.no_telpon = no_telpon;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
