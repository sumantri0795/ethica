package bandung.orion.com.ethica.form.profile.change_password;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.cart.CartFragment;
import bandung.orion.com.ethica.form.order_detail.OrderDetail;
import bandung.orion.com.ethica.form.order_detail.OrderDetailAdapter;
import bandung.orion.com.ethica.form.order_detail.OrderDetailModel;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;

public class ChangePassword extends AppCompatActivity {
    private TextInputEditText txtPasswordLama, txtPasswordBaru, txtKonfirmasiPassword;
    private Button btnOk;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        this.setTitle("Change Password");
        CreateView();
        InitClass();
        EventClass();
    }

    private void CreateView(){
        txtPasswordLama = (TextInputEditText) findViewById(R.id.txtPasswordLama);
        txtPasswordBaru = (TextInputEditText) findViewById(R.id.txtPasswordBaru);
        txtKonfirmasiPassword = (TextInputEditText) findViewById(R.id.txtKonfirmasiPassword);
        btnOk = (Button) findViewById(R.id.BtnOK);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void InitClass(){
        txtKonfirmasiPassword.setText("");
        txtPasswordBaru.setText("");
        txtPasswordLama.setText("");
    }

    private void EventClass(){
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidDataLocal()){

                    AlertDialog dialog = new AlertDialog.Builder(ChangePassword.this).create();
                    dialog.setMessage("Apakah data sudah benar");
                    dialog.setCancelable(true);
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int buttonId) {
                            UpdatePassword();
                        }
                    });
                    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int buttonId) {

                        }
                    });
                    dialog.setIcon(android.R.drawable.ic_dialog_alert);
                    dialog.show();;
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    private void UpdatePassword(){
        String url;
        url = Routes.url_change_password_master_customer+"?key="+(EthicaApplication.getInstance().ApiKey)+"&versi="+EthicaApplication.getInstance().getVersionCode();
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    if (jObj.getString("status").equals("Wrong Password")){
                        FungsiGeneral.inform(ChangePassword.this, "Password lama tidak valid","");
                    }else if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)){
                        Toast.makeText(ChangePassword.this, "Password berhasil diubah", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }else{
                        Toast.makeText(ChangePassword.this, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(ChangePassword.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChangePassword.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();

                params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                params.put("pass_lama", txtPasswordLama.getText().toString());
                params.put("pass_baru", txtPasswordBaru.getText().toString());
                params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };

        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);

    }

    private boolean isValidDataLocal(){
        if (!txtPasswordBaru.getText().toString().equals(txtKonfirmasiPassword.getText().toString())){
            Toast.makeText(this, "Password tidak sama, coba masukan ulang.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}