package bandung.orion.com.ethica.utility;

public class JConst {

    public static final String API_KEY_UNAUTORIZED = "Unauthorized";
    public static final String API_KEY_AUTHORIZED  = "Authorized";

    //TIPE CUSTOMER
    public static final String TIPE_CUSTOMER_ETHICA = "E";
    public static final String TIPE_CUSTOMER_SEPLY  = "S";

    public static final String STATUS_ORDER_KEEP          = "K";
    public static final String STATUS_ORDER_BELUM_APPROVE = "E";
    public static final String STATUS_ORDER_BELUM_PROSES  = "B";
    public static final String STATUS_ORDER_DIPROSES      = "P";
    public static final String STATUS_ORDER_TOLAK         = "T";
    public static final String STATUS_ORDER_DIHAPUS       = "H";
    public static final String STATUS_ORDER_SELESAI       = "S";

    public static final String STATUS_ORDER_KEEP_TEXT          = "Keep";
    public static final String STATUS_ORDER_BELUM_APPROVE_TEXT = "Belum Approve";
    public static final String STATUS_ORDER_BELUM_PROSES_TEXT  = "Belum Diproses";
    public static final String STATUS_ORDER_DIPROSES_TEXT      = "Diproses";
    public static final String STATUS_ORDER_TOLAK_TEXT         = "Ditolak";
    public static final String STATUS_ORDER_DIHAPUS_TEXT       = "Dihapus";
    public static final String STATUS_ORDER_SELESAI_TEXT       = "Selesai";

    public static final String KLA_DIAMOND      = "D";
    public static final String KLA_SHAPPHIRE    = "S";
    public static final String KLA_EMERALD      = "E";
    public static final String KLA_RUBI         = "R";
    public static final String KLA_NOT_DIAMOND  = "ND";

    //MISSAGE
    public static final String MSG_API_KEY_UNAUTORIZED = "Sesi login telah berakhir, silahkan login ulang";

    public static final String MSG_NOT_CONNECTION = "Tidak tersambung ke server, pastikan terkoneksi ke internet";

    public static final String KONDISI_HARUS_UPDATE = "harus update";
    public static final String KONDISI_TERUPDATE = "terupdate";
    public static final String MSG_HARUS_UPDATE     = "Versi baru telah tersedia, silahkan update aplikasi";


    //TIPE VIEW CART & ORDER LIST
    public static final int TIPE_VIEW_ITEM = 1;
    public static final int TIPE_VIEW_ALAMAT = 2;
    public static final int TIPE_VIEW_ALAMAT_PENGIRIMAN = 3;
    public static final int TIPE_VIEW_EKSPEDISI = 4;
    public static final int TIPE_VIEW_TOTAL = 5;

    //JENIS MENU
    public static final int JENIS_MENU_NEW_PRODUCT  = 1;
    public static final int JENIS_MENU_ETHICA_HIJAB = 2;
    public static final int JENIS_MENU_SARIMBIT     = 3;
    public static final int JENIS_MENU_PROMO        = 5;

    //TIPE_PESAN
    public static final String TIPE_CHAT_ANDROID  = "A";
    public static final String TIPE_CHAT_DESKTOP  = "D";
    public static final String TIPE_CHAT_TANGGAL  = "T";

    public static final int TYPE_RIGHT = 1;
    public static final int TYPE_LEFT = 2;
    public static final int TYPE_TANGGAL = 3;

    //JENIS CUSTOMER
    public static final String JENIS_CUST_AGEN          = "AGN";
    public static final String JENIS_CUST_ONLINE_RETAIL = "ONL";
    public static final String JENIS_CUST_TOKO_STORE    = "TOK";
    public static final String JENIS_CUST_KOPERASI      = "KOP";
    public static final String JENIS_CUST_MITRA         = "MIT";
    public static final String JENIS_CUST_MARKETING     = "MRT";
    public static final String JENIS_CUST_LAINNYA       = "LAI";
    public static final String JENIS_CUST_SUB_AGEN      = "SAG";

    public static final String JENIS_PESANAN_SO         = "S";
    public static final String JENIS_PESANAN_PREORDER   = "P";

    //MODE DETAIL ORDER
    public static final String MODE_DETAIL_ORDER_DETAIL    = "D";
    public static final String MODE_DETAIL_ORDER_APPROVE   = "A";

    //MODE DETAIL ORDER
    public static final String MODE_FILTER_CUSTOMER_ORDER   = "O";
    public static final String MODE_FILTER_CUSTOMER_CART    = "C";

    //EKSPEDISI LAIN LAIN
    public static final String NAMA_EKSPEDISI_LAIN  = "LAIN - LAIN";

    //TIPE APPS
    public static final String TIPE_APPS_ANDROID  = "A";




}
