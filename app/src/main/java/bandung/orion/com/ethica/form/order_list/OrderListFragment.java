package bandung.orion.com.ethica.form.order_list;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.product_list.ProductListAdapter;
import bandung.orion.com.ethica.form.product_list.ProductListModel;
import bandung.orion.com.ethica.form.product_list.UrutkanAdapter;
import bandung.orion.com.ethica.form.product_list.UrutkanModel;
import bandung.orion.com.ethica.form.product_list.filter.FilterProductList;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static android.app.Activity.RESULT_OK;
import static bandung.orion.com.ethica.utility.FungsiGeneral.EndOfTheMonthLong;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getSimpleDate;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getTglFormat;
import static bandung.orion.com.ethica.utility.FungsiGeneral.getTglFormatMySql;
import static bandung.orion.com.ethica.utility.FungsiGeneral.hideSoftKeyboard;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowLong;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowStartOfTheMonthLong;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_APPROVE;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_APPROVE_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_PROSES;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_BELUM_PROSES_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIHAPUS;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIHAPUS_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIPROSES;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_DIPROSES_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_KEEP;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_KEEP_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_SELESAI;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_SELESAI_TEXT;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_TOLAK;
import static bandung.orion.com.ethica.utility.JConst.STATUS_ORDER_TOLAK_TEXT;

public class OrderListFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener{
    public RecyclerView rcvData;
    private View v;
    private SwipeRefreshLayout swipe;
    private OrderListAdapter adapter;
    private Spinner spStatus;
    private ArrayList<String> ListStatus;
    public List<OrderListModel> ListItems = new ArrayList<>();
    private ILoadMore loadMore;
    private String status;
    int lastVisibleItem, totalItemCount;
    boolean IsLoading;
    private ArrayAdapter<String> adapterStatus;
    private boolean isStart;
    private int mMaxScrollSize;
    private AppBarLayout appbarLayout;
    private TextInputEditText txtTglDari, txtTglSampai;

    private LinearLayoutManager linearLayoutManager;
    int visibleThreshold = 4;
    private Long tgl_dari, tgl_Sampai;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_list_fragment, container, false);
        isStart = false;
        v = view;
        CreateView();
        InitClass();
        EventClass();
        //load();
        return view;
    }

    private void CreateView(){
        this.appbarLayout = (AppBarLayout) v.findViewById(R.id.materialup_appbar);
        this.rcvData = (RecyclerView) v.findViewById(R.id.rcvLoad);
        this.swipe = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        this.adapter = new OrderListAdapter(getActivity(), ListItems, this);
        rcvData.setAdapter(adapter);
        spStatus = (Spinner) v.findViewById(R.id.spStatus);
        txtTglDari   = v.findViewById(R.id.txtTglDari);
        txtTglSampai = v.findViewById(R.id.txtTglSampai);
    }

    private void InitClass(){
        rcvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        linearLayoutManager = (LinearLayoutManager)rcvData.getLayoutManager();
        this.adapter.removeAllModel();
        getActivity().setTitle("Order List");
        adapter.notifyDataSetChanged();
        status = "";
        ListStatus = new ArrayList<>();
        ListStatus.add("Semua");
        ListStatus.add(STATUS_ORDER_KEEP_TEXT);
        ListStatus.add(STATUS_ORDER_BELUM_APPROVE_TEXT);
        ListStatus.add(STATUS_ORDER_BELUM_PROSES_TEXT);
        ListStatus.add(STATUS_ORDER_DIPROSES_TEXT);
        ListStatus.add(STATUS_ORDER_TOLAK_TEXT);
        ListStatus.add(STATUS_ORDER_DIHAPUS_TEXT);
        ListStatus.add(STATUS_ORDER_SELESAI_TEXT);

        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        adapterStatus = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, ListStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(adapterStatus);
        spStatus.setSelection(0);
        swipe.setRefreshing(false);

        tgl_dari   = serverNowStartOfTheMonthLong();
        tgl_Sampai = EndOfTheMonthLong(serverNowLong());

        txtTglDari.setText(getTglFormat(serverNowStartOfTheMonthLong()));
        txtTglSampai.setText(getTglFormat(serverNowLong()));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0){
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        }
    }

    protected void EventClass(){
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                rcvData.scrollToPosition(0);
                adapter.removeAllModel();
                RefreshRecyclerView();
                swipe.setRefreshing(false);
            }
        });

        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0){
                    OrderListFragment.this.status = "";
                }else if (i == 1){
                    OrderListFragment.this.status = STATUS_ORDER_KEEP;
                }else if (i == 2){
                    OrderListFragment.this.status = STATUS_ORDER_BELUM_APPROVE;
                }else if (i == 3){
                    OrderListFragment.this.status = STATUS_ORDER_BELUM_PROSES;
                }else if (i == 4){
                    OrderListFragment.this.status = STATUS_ORDER_DIPROSES;
                }else if (i == 5){
                    OrderListFragment.this.status = STATUS_ORDER_TOLAK;
                }else if (i == 6){
                    OrderListFragment.this.status = STATUS_ORDER_DIHAPUS;
                }else if (i == 7){
                    OrderListFragment.this.status = STATUS_ORDER_SELESAI;
                }

                if (isStart) {
                    rcvData.scrollToPosition(0);
                    adapter.removeAllModel();
                    RefreshRecyclerView();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
//                if (ListItems.size() <=100 ){//100 adalah jumlah maksimal, bisa diisi kondisi juga nantinya
                adapter.addModel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.removeMoel(ListItems.size()-1);
                        load();
//                            setLoaded(false);
                    }
                },1000);
//                }else {
//                }
            }
        });

        rcvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

        txtTglDari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(getActivity(), v);
                if (txtTglDari.getText().toString().equals("")){
                    txtTglDari.setText(FungsiGeneral.serverNowFormated());
                }
                Long tgl = FungsiGeneral.getMillisDate(txtTglDari.getText().toString());
                int mYear = (Integer.parseInt(FungsiGeneral.getTahun(tgl)));
                int mMonth = (Integer.parseInt(FungsiGeneral.getBulan(tgl)))-1;
                int mDay = (Integer.parseInt(FungsiGeneral.getHari(tgl)));

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth);
                                SimpleDateFormat format = new SimpleDateFormat("MMyy");
                                format = new SimpleDateFormat("dd-MM-yyyy");
                                txtTglDari.setText(format.format(calendar.getTime()));

                                rcvData.scrollToPosition(0);
                                adapter.removeAllModel();
                                RefreshRecyclerView();
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        txtTglSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(getActivity(), v);
                if (txtTglSampai.getText().toString().equals("")){
                    txtTglSampai.setText(FungsiGeneral.serverNowFormated());
                }
                Long tgl = FungsiGeneral.getMillisDate(txtTglSampai.getText().toString());
                int mYear = (Integer.parseInt(FungsiGeneral.getTahun(tgl)));
                int mMonth = (Integer.parseInt(FungsiGeneral.getBulan(tgl)))-1;
                int mDay = (Integer.parseInt(FungsiGeneral.getHari(tgl)));

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth);
                                SimpleDateFormat format = new SimpleDateFormat("MMyy");
                                format = new SimpleDateFormat("dd-MM-yyyy");
                                txtTglSampai.setText(format.format(calendar.getTime()));

                                rcvData.scrollToPosition(0);
                                adapter.removeAllModel();
                                RefreshRecyclerView();
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }


    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }

    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }


    public void load(){
        String Filter = "";
        tgl_dari   = getSimpleDate(txtTglDari.getText().toString());
        tgl_Sampai = getSimpleDate(txtTglSampai.getText().toString());

        Filter += "?customer_seq=" + EthicaApplication.getInstance().getCustomerSeqGlobal()+"&user_id=" + EthicaApplication.getInstance().getUserIdGlobal()+ "&offset=" + Integer.toString(adapter.getItemCount())+
                  "&status="+status+"&tgl_dari="+getTglFormatMySql(tgl_dari)+"&tgl_sampai="+getTglFormatMySql(tgl_Sampai);

        String url = Routes.url_load_pesanan_master + Filter;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<OrderListModel> itemDataModels = new ArrayList<>();
                OrderListModel Data;
                itemDataModels.clear();
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            Data = new OrderListModel(
                                    obj.getInt("seq"),
                                    obj.getString("tanggal"),
                                    obj.getString("nomor"),
                                    obj.getDouble("total"),
                                    obj.getDouble("diskon"),
                                    obj.getDouble("subtotal"),
                                    obj.getString("status"),
                                    obj.getString("jenis_so"),
                                    obj.getDouble("total_berat"),
                                    obj.getDouble("ongkos_kirim")
                            );
                            Data.setNotif(obj.getInt("notif"));
                            Data.setIs_selesai_chat(obj.getString("is_selesai_chat"));
                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(false);
                    }
                }
                adapter.addModels(itemDataModels);
                setLoaded(false);
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                swipe.setRefreshing(false);
                setLoaded(false);
                Toast.makeText(getActivity(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    @Override
    public void onStart() {
        super.onStart();
        isStart = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                rcvData.scrollToPosition(0);
                adapter.removeAllModel();
                RefreshRecyclerView();
            }
        }
    }
}
