package bandung.orion.com.ethica.form.sarimbit;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;

import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.calculateWidth.calculateNoOfColumns;
import static java.lang.Math.round;

public class product_sarimbit extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener{
    public RecyclerView RcvProductList;
    public SarimbitListAdapter mAdapter;
    private int mMaxScrollSize;
    private AppBarLayout appbarLayout;
    private SwipeRefreshLayout swipe;
    private SearchView txtSearch;
    private CollapsingToolbarLayout collapsing;

    int visibleThreshold = 6;
    int lastVisibleItem, totalItemCount;
    boolean IsLoading;
    ILoadMore loadMore;

    private LinearLayoutManager linearLayoutManager;
    public List<SarimbitListModel> ListItems = new ArrayList<>();

    private final String MODE_TAMPILAN_GRID = "G";
    private final String MODE_TAMPILAN_LIST = "L";

    private String ModeTampilan;
    private int NoOfColumns;
    private int CountAllVew;
    final List<View> pageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_sarimbit);
        CreateView();
        InitClass();
        EventClass();
        LoadData();
    }

    private void CreateView(){
        this.RcvProductList = (RecyclerView) findViewById(R.id.rcvLoad);
        this.appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        this.swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        this.txtSearch = (SearchView) findViewById(R.id.txtSearch);
        this.collapsing = findViewById(R.id.collapsing);
    }

    private void InitClass(){
        ModeTampilan = MODE_TAMPILAN_GRID;        
        NoOfColumns = calculateNoOfColumns(getApplicationContext());
        SetJenisTampilan();

        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();
        Toolbar toolbar = (Toolbar) findViewById(R.id.ToolbarAct);
        this.setSupportActionBar(toolbar);
        CountAllVew = 0;
        SetHeightCarousel();
        collapsing.setVisibility(View.GONE);
    }

    protected void EventClass(){
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!IsLoading){
                    mAdapter.removeAllModel();
                    RefreshRecyclerView();
                }
                swipe.setRefreshing(false);
            }
        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {

                mAdapter.addMoel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.removeMoel(ListItems.size()-1);
                        LoadData();
                    }
                },1000);
            }
        });

        RcvProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

        txtSearch.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

        });

        txtSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0){
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        }
    }

    public void LoadData(){
        String Filter = "";
        Filter += "?search=" + txtSearch.getQuery() + "&offset=" + Integer.toString(mAdapter.getItemCount())+
                  "&is_ethica=" + EthicaApplication.getInstance().getIsEthicaGlobal()+
                  "&is_seply=" + EthicaApplication.getInstance().getIsSeplyGlobal()+
                  "&is_ethica_hijab=" + EthicaApplication.getInstance().getIsEthicaHijabGlobal();
        String url = Routes.url_load_sarimbit + Filter;
        url = url.replace(" ", "%20");
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<SarimbitListModel> itemDataModels = new ArrayList<>();
                SarimbitListModel Data;
                itemDataModels.clear();
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            Data = new SarimbitListModel(
                                    obj.getInt("seq"),
                                    obj.getString("nama"),
                                    getMillisDate(obj.getString("tgl_berlaku_dari")),
                                    getMillisDate(obj.getString("tgl_berlaku_sampai")),
                                    obj.getString("kelompok_brand"),
                                    obj.getString("keterangan"),
                                    obj.getString("user_id"),
                                    getMillisDate(obj.getString("tgl_hapus")),
                                    obj.getString("gambar"),
                                    obj.getString("gambar_sedang"),
                                    obj.getString("gambar_besar")
                            );
                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(false);
                    }
                }
                mAdapter.addModels(itemDataModels);
                setLoaded(false);
                swipe.setRefreshing(false);
                if (ListItems.size() <= 0){Toast.makeText(getApplicationContext(), "Tidak ditemukan", Toast.LENGTH_SHORT).show();}
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                swipe.setRefreshing(false);
                setLoaded(false);
                Toast.makeText(getApplicationContext(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }


    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }

    private void SetJenisTampilan(){
        if (ModeTampilan.equals(MODE_TAMPILAN_GRID)){
            mAdapter = new SarimbitListAdapter(product_sarimbit.this, ListItems,  product_sarimbit.this, R.layout.list_item_sarimbit_list_grid);
            RcvProductList.setLayoutManager(new GridLayoutManager(product_sarimbit.this, NoOfColumns, GridLayoutManager.VERTICAL, false));
            linearLayoutManager = (LinearLayoutManager)RcvProductList.getLayoutManager();
            RcvProductList.setAdapter(mAdapter);
//        }else if (ModeTampilan.equals(MODE_TAMPILAN_LIST)){
//            mAdapter = new SarimbitListAdapter(getApplicationContext(), ListItems, R.layout.list_item_product_list);
//            RcvProductList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//            linearLayoutManager = (LinearLayoutManager)RcvProductList.getLayoutManager();
//            RcvProductList.setAdapter(mAdapter);
        }
    }

    private void SetHeightCarousel(){
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        AppBarLayout.LayoutParams layoutParams = (AppBarLayout.LayoutParams) collapsing.getLayoutParams();
        Integer height = (int) (long) round(displayMetrics.heightPixels * 0.46);
        layoutParams.height = height;
        collapsing.setLayoutParams(layoutParams);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (EthicaApplication.getInstance().KeCart == true){
                finish();
            }
        }
    }
}
