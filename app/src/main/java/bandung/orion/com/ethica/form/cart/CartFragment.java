package bandung.orion.com.ethica.form.cart;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.alamat_kirim.InputAlamatKirimCartActivity;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.order_detail.OrderDetail;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.JConst;
import bandung.orion.com.ethica.utility.Routes;

import static android.app.Activity.RESULT_OK;
import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.FungsiGeneral.serverNowStartOfTheMonthLong;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.JENIS_PESANAN_PREORDER;
import static bandung.orion.com.ethica.utility.JConst.JENIS_PESANAN_SO;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MODE_DETAIL_ORDER_DETAIL;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.NAMA_EKSPEDISI_LAIN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT_PENGIRIMAN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_EKSPEDISI;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ITEM;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_TOTAL;
import static bandung.orion.com.ethica.utility.Routes.ANDROID_KEY_RAJA_ONGKIR;
import static bandung.orion.com.ethica.utility.Routes.API_KEY_RAJA_ONGKIR;

public class CartFragment extends Fragment{
    private Button btnOrder;
    private RecyclerView rcvData;
    private TextView txtTotal, txtSubtotal, txtDiskon;
    private ArrayList<CartModel> datas;
    private CartAdapter adapter;
    private View v;
    private double total, subtotal, diskon, berat;
    private ProgressDialog Loading;
    private String jenisSO; //diisi sebelum save
    private FragmentActivity ThisActivity;
    private Boolean MunculkanLoadingAwal = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cart_fragment, container, false);
        v = view;
        MunculkanLoadingAwal = true;
        CreateView();
        InitClass();
        EventClass();
        LoadAlamat();
        return view;
    }

    private void CreateView(){
        ThisActivity = getActivity();
        btnOrder = (Button) v.findViewById(R.id.btnOrder);
        txtSubtotal = (TextView) v.findViewById(R.id.txtSubtotal);
        txtDiskon = (TextView) v.findViewById(R.id.txtDiskon);
        txtTotal = (TextView) v.findViewById(R.id.txtTotalMst);
        rcvData = (RecyclerView) v.findViewById(R.id.rcvData);
        rcvData.setLayoutManager(new LinearLayoutManager(ThisActivity));

        datas = new ArrayList<CartModel>();
        this.adapter = new CartAdapter(ThisActivity, this);
        rcvData.setAdapter(adapter);
    }

    private void InitClass(){
        total = 0;
        subtotal = 0;
        diskon = 0;
        ThisActivity.setTitle("Cart");
        this.Loading = new ProgressDialog(getContext());
    }

    private void EventClass(){
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String ADA_SETTING_DISKON       = "A";
                final String TIDAK_ADA_SETTING_DISKON = "K";


                if (datas.get(0).getId_provinsi() <= 0 ){
                    inform(ThisActivity, "Provinsi belum dipilih.", "");
                    rcvData.smoothScrollToPosition(0);
                    return;
                }

                if (datas.get(0).getId_kota() <= 0 ){
                    inform(ThisActivity, "Kota belum dipilih.", "");
                    rcvData.smoothScrollToPosition(0);
                    return;
                }

                if (datas.get(0).getId_kecamatan() <= 0 ){
                    inform(ThisActivity, "Kecamatan belum dipilih.", "");
                    rcvData.smoothScrollToPosition(0);
                    return;
                }


                int jml = 0;
                jenisSO = "";
                String adaSetting = "";
                for (int i=0; i < datas.size(); i ++){
                    if (datas.get(i).getTipe_view() == TIPE_VIEW_ITEM){
                        jml += datas.get(i).getQty_order();

                        if (datas.get(i).getQty_order() > 0) {
                            if (datas.get(i).getBerat() <= 0){
                                inform(ThisActivity, datas.get(i).getNama()+" belum ada berat.", "");

                                datas.clear();
                                adapter.deleteAllModels();
                                adapter.notifyDataSetChanged();
                                txtTotal.setText(FungsiGeneral.FloatToStrFmt(0, false));
                                LoadAlamat();

                                return;
                            }

                            String jenisSOTemp = "";
                            if (datas.get(i).getIs_preorder().equals("T")) {
                                jenisSOTemp = JENIS_PESANAN_PREORDER;
                            } else {
                                jenisSOTemp = JENIS_PESANAN_SO;
                            }
                            if (!jenisSO.equals("") && !jenisSO.equals(jenisSOTemp)) {
                                inform(ThisActivity, "Barang preorder dan barang biasa tidak bisa dikeep dalam 1 order.", "");
                                return;
                            }
                            jenisSO = jenisSOTemp;

                            String adaSettingTmp = "";
                            if (datas.get(i).getAda_setting().equals("T")) {
                                adaSettingTmp = ADA_SETTING_DISKON;
                            } else {
                                adaSettingTmp = TIDAK_ADA_SETTING_DISKON;
                            }
                            if (!adaSetting.equals("") && !adaSetting.equals(adaSettingTmp)) {
                                inform(ThisActivity, "Barang promo dan non promo tidak bisa dikeep dalam 1 order.", "");
                                return;
                            }
                            adaSetting = adaSettingTmp;
                        }
                    }
                }

                if (jml == 0) {
                    inform(ThisActivity, "Belum ada jumlah keep yang diisi.","");
                    return;
                }

                if (datas.get(2).getEkspedisi_seq() == 0){
                    inform(ThisActivity, "Kurir belum dipilih.","");
                    rcvData.smoothScrollToPosition(2);
                    return;
                }

                AlertDialog dialog = new AlertDialog.Builder(ThisActivity).create();
                dialog.setMessage("Apakah data sudah benar");
                dialog.setCancelable(true);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {
                        CartFragment.this.KirimDataMst();
                    }
                });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {

                    }
                });
                dialog.setIcon(android.R.drawable.ic_dialog_alert);
                dialog.show();;
            }
        });
    }

    private void KirimDataMst(){
        String url;
        url = Routes.url_keep_pesanan_master+"?key="+EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                        inform(ThisActivity, MSG_HARUS_UPDATE, "");
                        return;
                    }

                    if (jObj.getString("status").equals("status off")){
                        inform(ThisActivity, "Untuk sementara tidak dapat melakukan keep barang", "");
                        return;
                    }

                    if (jObj.getString("status").equals("Plafon tidak mencukupi")){
                        Toast.makeText(ThisActivity, "Plafon tidak mencukupi", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (jObj.getString("status").equals("error customer")){
                        Toast.makeText(ThisActivity,"Terdapat kesalahan pada customer", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)){
                        int seq = jObj.getInt("seq");
                        Toast.makeText(ThisActivity,"Data berhasil dikeep", Toast.LENGTH_SHORT).show();

                        Intent in = new Intent(ThisActivity, OrderDetail.class);
                        in.putExtra("seq", seq);
                        in.putExtra("mode", MODE_DETAIL_ORDER_DETAIL);
                        startActivityForResult(in, 4);
                    }else{
                        Toast.makeText(ThisActivity, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(ThisActivity, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ThisActivity, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                params.put("diskon", String.valueOf(diskon));
                params.put("subtotal", String.valueOf(subtotal));
                params.put("total", String.valueOf(total));
                //idx datas yang ke 0 harusnya selalu alamat kirim
                params.put("alamat_kirim", datas.get(0).getAlamat_kirim());
                //idx datas yang ke 1 harusnya selalu alamat pengirim
                params.put("alamat_pengirim", datas.get(1).getAlamat_pengirim());

                params.put("nama_pengirim", datas.get(1).getNama_pengirim());
                params.put("alamat_pengirim_lengkap", datas.get(1).getAlamat_pengirim_lengkap());
                params.put("no_telepon_pengirim", datas.get(1).getNo_telepon_pengirim());
                params.put("nama_kirim", datas.get(0).getNama_kirim());
                params.put("kelurahan", datas.get(0).getKelurahan());
                params.put("provinsi", datas.get(0).getProvinsi());
                params.put("kota", datas.get(0).getKota());
                params.put("kecamatan", datas.get(0).getKecamatan());
                params.put("alamat_kirim_lengkap", datas.get(0).getAlamat_kirim_lengkap());
                params.put("no_telepon_kirim", datas.get(0).getNo_telepon_kirim());
                params.put("ongkos_kirim", String.valueOf(datas.get(datas.size()-1).getOngkos_kirim()));

                params.put("ekspedisi_seq", String.valueOf(datas.get(2).getEkspedisi_seq()));
                params.put("ekspedisi", datas.get(2).getEkspedisi());
                params.put("service", datas.get(2).getService());
                params.put("no_resi", datas.get(2).getNoResi());
                params.put("is_selesai_chat", "F");
                params.put("jenis_so", jenisSO);

                params.put("id_provinsi", String.valueOf(datas.get(0).getId_provinsi()));
                params.put("id_kota", String.valueOf(datas.get(0).getId_kota()));
                params.put("id_kecamatan", String.valueOf(datas.get(0).getId_kecamatan()));
                params.put("total_berat", String.valueOf(datas.get(datas.size()-1).getTotalBerat()));
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };

        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);

    }

    public void hitung(){
        total = 0;
        subtotal = 0;
        diskon = 0;
        double qty = 0;
        txtTotal.setText(FungsiGeneral.FloatToStrFmt(0, false));
        if (datas.size() != 0) {
            double Ongkir = datas.get(datas.size() - 1).getOngkos_kirim();
            berat = 0;
            for (int i = 0; i < datas.size(); i++) {
                qty = datas.get(i).getQty_order();
                subtotal += (datas.get(i).getHarga() * qty);
                diskon   += (datas.get(i).getHarga_diskon() * qty);
                berat    += (datas.get(i).getBerat() * qty);
            }
            total += subtotal - diskon + Ongkir;
            txtSubtotal.setText(FungsiGeneral.FloatToStrFmt(subtotal, false));
            txtDiskon.setText(FungsiGeneral.FloatToStrFmt(diskon, false));
            txtTotal.setText(FungsiGeneral.FloatToStrFmt(total, false));


            datas.get(datas.size() - 1).setTotal(total);
            datas.get(datas.size() - 1).setSubTotal(subtotal);
            datas.get(datas.size() - 1).setDiskon(diskon);
            datas.get(datas.size() - 1).setTotalBerat(berat);
            datas.get(2).setTotalBerat(berat);
            adapter.notifyDataSetChanged();
        }else{
            txtTotal.setText(FungsiGeneral.FloatToStrFmt(0, false));
        }
    }

    private void getDiskon(){
        if (MunculkanLoadingAwal == true){
            Loading.setMessage("Loading...");
            Loading.setCancelable(false);
            Loading.show();
        }

        for (int i = 3; i<= datas.size()-2; i++){String url = Routes.url_get_nilai_diskon;
            final int nilaiI = i;
            StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (MunculkanLoadingAwal == true){
                        Loading.setMessage("Loading...");
                        Loading.setCancelable(false);
                        Loading.show();
                    }

                    try {
                        JSONObject jObj = new JSONObject(response);
                        if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)){
                            double diskon = jObj.getDouble("diskon");
                            String dariSetting = jObj.getString("dari_setting");
                            Double maxSettinDiskon = jObj.getDouble("max_diskon_setting");

                            datas.get(nilaiI).setDiskon(diskon * datas.get(nilaiI).getHarga() / 100);
                            datas.get(nilaiI).setHarga_diskon(diskon * datas.get(nilaiI).getHarga() / 100);
                            datas.get(nilaiI).setAda_setting(dariSetting);
                            datas.get(nilaiI).setMax_diskon_setting(maxSettinDiskon);
                            adapter.notifyDataSetChanged();
                            hitung();
                            if (nilaiI==datas.size()-2) {
                                Loading.dismiss();
                                MunculkanLoadingAwal = false;
                            }

                        }else{
                            Toast.makeText(ThisActivity, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        // JSON error
                        e.printStackTrace();
                        Toast.makeText(ThisActivity, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ThisActivity, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters ke post url
                    Map<String, String> params = new HashMap<String, String>();


                    params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                    params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                    params.put("barang_seq", String.valueOf(datas.get(nilaiI).getBarang_seq()));
                    return params;
                }
            };
            // menambah request ke request queue
            EthicaApplication.getInstance().addToRequestQueue(strReq);
        }
    }

    private void load(){
        // membuat request JSON

        String url = Routes.url_load_cart+"/"+String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal())+
                     "?tipe_cust=" + EthicaApplication.getInstance().getTipeCust()+"&user_id=" + EthicaApplication.getInstance().getUserIdGlobal();
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        CartModel item = new CartModel();
                        item.setBarang_seq(obj.getLong("barang_seq"));
                        item.setCustomer_seq(obj.getLong("customer_seq"));
                        item.setHarga(obj.getDouble("harga"));
                        item.setKode(obj.getString("kode"));
                        item.setNama(obj.getString("nama"));
                        item.setQty(obj.getDouble("qty"));
                        item.setGambar(obj.getString("gambar"));
                        item.setSeq(obj.getLong("seq"));
                        item.setHarga_diskon(obj.getDouble("diskon"));
                        item.setBerat(obj.getDouble("berat"));
                        item.setBerat_total(obj.getDouble("berat_total"));
                        item.setQty_order(obj.getDouble("qty_order"));
                        item.setIs_preorder(obj.getString("is_preorder"));
                        item.setTipe_view(TIPE_VIEW_ITEM);
                        datas.add(item);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //buat array untuk total

                CartModel item2 = new CartModel();
                item2.setOngkos_kirim(0.0);
                item2.setTotal(0.0);
                item2.setSubTotal(0.0);
                item2.setDiskon(0.0);
                item2.setTotalBerat(0.0);
                item2.setTipe_view(TIPE_VIEW_TOTAL);
                datas.add(item2);
                hitung();
                getOngkir();
                CartFragment.this.adapter.addModels(datas);
                MunculkanLoadingAwal = true;
                getDiskon();
                hitung();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
                Loading.dismiss();
            }
        });

        // menambah request ke request queue
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    private void LoadAlamat(){
        Loading.setMessage("Loading...");
        Loading.setCancelable(false);
        Loading.show();

        datas.clear();
        adapter.notifyDataSetChanged();

        // membuat request JSON
        String url = Routes.url_get_cart_master+"/"+String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal())+"/"+
                     EthicaApplication.getInstance().getUserIdGlobal();

        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        CartModel item = new CartModel();
                        item.setTipe_view(TIPE_VIEW_ALAMAT);
                        item.setAlamat_kirim(obj.getString("alamat_kirim"));
                        item.setNama_kirim(obj.getString("nama_kirim"));
                        item.setId_provinsi(obj.getInt("id_provinsi"));
                        item.setId_kota(obj.getInt("id_kota"));
                        item.setId_kecamatan(obj.getInt("id_kecamatan"));
                        item.setProvinsi(obj.getString("provinsi"));
                        item.setKota(obj.getString("kota"));
                        item.setKecamatan(obj.getString("kecamatan"));
                        item.setKelurahan(obj.getString("kelurahan"));
                        item.setAlamat_kirim_lengkap(obj.getString("alamat_kirim_lengkap"));
                        item.setNo_telepon_kirim(obj.getString("no_telepon_kirim"));
                        item.setEkspedisi(obj.getString("ekspedisi"));
                        datas.add(item);

                        CartModel item2 = new CartModel();
                        item2.setTipe_view(TIPE_VIEW_ALAMAT_PENGIRIMAN);
                        item2.setAlamat_pengirim(obj.getString("alamat_pengirim"));

                        item2.setNama_pengirim(obj.getString("nama_pengirim"));
                        item2.setAlamat_pengirim_lengkap(obj.getString("alamat_pengirim_lengkap"));
                        item2.setNo_telepon_pengirim(obj.getString("no_telepon_pengirim"));

                        datas.add(item2);


                        CartModel item3 = new CartModel();
                        item3.setTipe_view(TIPE_VIEW_EKSPEDISI);
                        item3.setEkspedisi(obj.getString("ekspedisi"));
                        item3.setService(obj.getString("service"));
                        item3.setEkspedisi_seq(obj.getInt("ekspedisi_seq"));
                        item3.setNoResi(obj.getString("no_resi"));
                        item3.setOngkos_kirim(0.0);
                        item3.setTotalBerat(0.0);
                        datas.add(item3);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                load();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
                Loading.dismiss();
            }
        });
        // menambah request ke request queue
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }


    public void delete_list(int pos){
        datas.remove(pos);
    }

    public void delete_list_by_seq(long seq){
        for (int x = 0; x < this.datas.size(); x++) {
            if (datas.get(x).getSeq() == seq) {
                datas.remove(x);
            }
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extra = data.getExtras();
                datas.get(0).setKecamatan(extra.getString("kecamatan"));
                datas.get(0).setId_kecamatan(extra.getInt("id_kecamatan"));
                datas.get(0).setId_kota(extra.getInt("id_kota"));
                datas.get(0).setId_provinsi(extra.getInt("id_provinsi"));

                datas.get(0).setKelurahan(extra.getString("kelurahan"));
                datas.get(0).setKota(extra.getString("kota"));
                datas.get(0).setProvinsi(extra.getString("provinsi"));
                datas.get(0).setAlamat_kirim_lengkap(extra.getString("alamat_kirim_lengkap"));
                datas.get(0).setNo_telepon_kirim(extra.getString("no_telepon_kirim"));
                datas.get(0).setNama_kirim(extra.getString("nama_kirim"));
                String alamatKirim =
                        "Kepada \n"+
                        "Yth : "+ datas.get(0).getNama_kirim()+"\n"+
                        "Alamat : "+datas.get(0).getAlamat_kirim_lengkap() +", "+
                        datas.get(0).getKelurahan() +", "+
                        datas.get(0).getKecamatan () +", "+
                        datas.get(0).getKota () +", "+
                        datas.get(0).getProvinsi() +", "+
                        "No. HP : "+datas.get(0).getNo_telepon_kirim();
                datas.get(0).setAlamat_kirim(alamatKirim);
                getOngkir();
                adapter.notifyDataSetChanged();
            } else if (requestCode == 2) {
                Bundle extra = data.getExtras();
                datas.get(2).setEkspedisi(extra.getString("ekspedisi"));
                datas.get(2).setEkspedisi_seq(extra.getInt("ekspedisi_seq"));
                datas.get(2).setService(extra.getString("service"));
                getOngkir();
                adapter.notifyDataSetChanged();
            } else if (requestCode == 3) {
                Bundle extra = data.getExtras();
                datas.get(1).setAlamat_pengirim(extra.getString("ALAMAT_PENGIRIM"));
                datas.get(1).setNama_pengirim(extra.getString("NAMA_PENGIRIM"));
                datas.get(1).setAlamat_pengirim_lengkap(extra.getString("ALAMAT_PENGIRIM_LENGKAP"));
                datas.get(1).setNo_telepon_pengirim(extra.getString("NO_TELEPON_PENGIRIM"));
                adapter.notifyDataSetChanged();
            }
        }
        if (requestCode == 4) {
            datas.clear();
            adapter.deleteAllModels();
            adapter.notifyDataSetChanged();
            txtTotal.setText(FungsiGeneral.FloatToStrFmt(0, false));
            LoadAlamat();//agar ngeload urang setelah save
            ((Home)ThisActivity).updateJumlahKeranjang();
        }
    }

    private void update_ekspedisi_ongkir(){
        String url;
        url = Routes.url_update_cart_master_ekspedisi_ongkir + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                        inform(ThisActivity, MSG_HARUS_UPDATE, "");
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ThisActivity, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ThisActivity, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                params.put("ekspedisi", datas.get(2).getEkspedisi());
                params.put("ekspedisi_seq", String.valueOf(datas.get(2).getEkspedisi_seq()));
                params.put("service", datas.get(2).getService());
                params.put("ongkos_kirim", String.valueOf(datas.get(2).getOngkos_kirim()));
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };

        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);

    }


    public void getOngkir(){
        Loading.setMessage("Loading...");
        Loading.setCancelable(false);
        Loading.show();

        if (datas.size() <= 3) {
            return;
        }
        final int id_kecamatan = datas.get(0).getId_kecamatan();
        final String ekspedisi = datas.get(2).getEkspedisi().toLowerCase();
        final String service = datas.get(2).getService().toLowerCase();

        //Jika ekspedisinya lain lain harga dinolin
        if (ekspedisi.equals(NAMA_EKSPEDISI_LAIN.toLowerCase())){
            datas.get(2).setOngkos_kirim(0.0);
            datas.get(datas.size()-1).setOngkos_kirim(0.0);
            hitung();
            update_ekspedisi_ongkir();
            adapter.notifyDataSetChanged();
            Loading.dismiss();
            return;
        }

        datas.get(2).setOngkos_kirim(0.0);
        if (id_kecamatan > 0 && !ekspedisi.equals("") && !service.equals("") && (berat != 0)) {
            String url = Routes.url_get_harga;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray ArrResults = new JSONObject(response).getJSONObject("rajaongkir").getJSONArray("results");

                                    try {
                                        JSONObject obj = ArrResults.getJSONObject(0);
                                        JSONArray arrCost = obj.getJSONArray("costs");

                                        for (int j = 0; j < arrCost.length(); j++) {
                                            JSONObject obj2 = arrCost.getJSONObject(j);
                                            String temp = obj2.getString("service");
                                            if ((temp.toLowerCase().equals(service))|| (temp.toLowerCase().equals("ctc") && service.toLowerCase().equals("reg")) ||
                                                 (temp.toLowerCase().equals("ctcyes") && service.toLowerCase().equals("yes"))){

                                                JSONArray arrHasil = obj2.getJSONArray("cost");
                                                for (int k = 0; k < arrHasil.length(); j++) {
                                                    JSONObject obj3 = arrHasil.getJSONObject(k);
                                                    datas.get(2).setOngkos_kirim(obj3.getDouble("value"));
                                                    datas.get(datas.size()-1).setOngkos_kirim(obj3.getDouble("value"));
                                                    hitung();
                                                    update_ekspedisi_ongkir();
                                                    adapter.notifyDataSetChanged();
                                                    Loading.dismiss();
                                                    return;
                                                }
                                            }
                                        }

                                        if (datas.get(2).getOngkos_kirim() == 0){
                                            getOngkirAntarKota();
                                            Loading.dismiss();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Loading.dismiss();
                                    }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Loading.dismiss();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(ThisActivity,"Kurir tidak ditemukan untuk alamat terpilih.", Toast.LENGTH_SHORT).show();
                            Loading.dismiss();
                            datas.get(2).setOngkos_kirim(0.0);
                            datas.get(2).setEkspedisi("");
                            datas.get(2).setService("");
                            datas.get(2).setEkspedisi_seq(0);
                            datas.get(datas.size()-1).setOngkos_kirim(0.0);
                            hitung();
                            adapter.notifyDataSetChanged();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters ke post url
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("originType", "subdistrict");
                    params.put("origin", "343");
                    params.put("destination", String.valueOf(id_kecamatan));
                    params.put("destinationType", "subdistrict");
                    int berattemp;
                    double beratgram = berat * 1000;
                    berattemp = (int) beratgram;
                    params.put("weight", String.valueOf(berattemp));
                    params.put("courier", ekspedisi);
                    params.put("key", API_KEY_RAJA_ONGKIR);
                    params.put("android-key", ANDROID_KEY_RAJA_ONGKIR);
//                    String filter = "originType&=subdistrict"+
//                            "&origin="+String.valueOf(343)+
//                            "&destination="+String.valueOf(id_kecamatan)+
//                            "destinationType&=subdistrict"+
//                            "&weight="+String.valueOf(berat)+
//                            "&courier="+String.valueOf(ekspedisi);
                    return params;
                }
            };
            EthicaApplication.getInstance().addToRequestQueue(stringRequest, FungsiGeneral.tag_json_obj);
        }else{
            Loading.dismiss();
        }
    }



    public void getOngkirAntarKota(){
        final int id_kota = datas.get(0).getId_kota();
        final String ekspedisi = datas.get(2).getEkspedisi().toLowerCase();
        final String service = datas.get(2).getService().toLowerCase();

        datas.get(2).setOngkos_kirim(0.0);
        if (id_kota > 0 && !ekspedisi.equals("") && !service.equals("")) {
            String url = Routes.url_get_harga;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray ArrResults = new JSONObject(response).getJSONObject("rajaongkir").getJSONArray("results");

                                try {
                                    JSONObject obj = ArrResults.getJSONObject(0);
                                    JSONArray arrCost = obj.getJSONArray("costs");

                                    for (int j = 0; j < arrCost.length(); j++) {
                                        JSONObject obj2 = arrCost.getJSONObject(j);
                                        String temp = obj2.getString("service");
                                        if ((temp.toLowerCase().equals(service))||
                                                (temp.toLowerCase().equals("ctc") && service.toLowerCase().equals("reg"))||
                                                (temp.toLowerCase().equals("ctcyes") && service.toLowerCase().equals("yes"))
                                                ){
                                            JSONArray arrHasil = obj2.getJSONArray("cost");
                                            for (int k = 0; k < arrHasil.length(); j++) {
                                                JSONObject obj3 = arrHasil.getJSONObject(k);
                                                datas.get(2).setOngkos_kirim(obj3.getDouble("value"));
                                                datas.get(datas.size()-1).setOngkos_kirim(obj3.getDouble("value"));
                                                hitung();
                                                adapter.notifyDataSetChanged();
                                                return;
                                            }
                                        }
                                    }
                                    if (datas.get(2).getOngkos_kirim() == 0){
                                        Toast.makeText(ThisActivity,"Kurir tidak ditemukan untuk alamat terpilih.", Toast.LENGTH_SHORT).show();
                                        datas.get(2).setOngkos_kirim(0.0);
                                        datas.get(2).setEkspedisi("");
                                        datas.get(2).setService("");
                                        datas.get(2).setEkspedisi_seq(0);
                                        datas.get(datas.size()-1).setOngkos_kirim(0.0);
                                        hitung();
                                        adapter.notifyDataSetChanged();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(ThisActivity,"Kurir tidak ditemukan untuk alamat terpilih.", Toast.LENGTH_SHORT).show();
                            datas.get(2).setOngkos_kirim(0.0);
                            datas.get(2).setEkspedisi("");
                            datas.get(2).setService("");
                            datas.get(2).setEkspedisi_seq(0);
                            datas.get(datas.size()-1).setOngkos_kirim(0.0);
                            hitung();
                            adapter.notifyDataSetChanged();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters ke post url
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("originType", "city");
                    params.put("origin", "23");
                    params.put("destination", String.valueOf(id_kota));
                    params.put("destinationType", "city");
                    int berattemp;
                    berattemp = (int)berat * 1000;
                    params.put("weight", String.valueOf(berattemp));
                    params.put("courier", ekspedisi);
                    params.put("key", API_KEY_RAJA_ONGKIR);
                    params.put("android-key", ANDROID_KEY_RAJA_ONGKIR);
//                    String filter = "originType&=subdistrict"+
//                            "&origin="+String.valueOf(343)+
//                            "&destination="+String.valueOf(id_kecamatan)+
//                            "destinationType&=subdistrict"+
//                            "&weight="+String.valueOf(berat)+
//                            "&courier="+String.valueOf(ekspedisi);
                    return params;
                }
            };
            EthicaApplication.getInstance().addToRequestQueue(stringRequest, FungsiGeneral.tag_json_obj);
        }
    }
}

