package bandung.orion.com.ethica.form.order_detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.ZoomImage.ZoomImageActivity;
import bandung.orion.com.ethica.form.cart.CartAdapter;
import bandung.orion.com.ethica.form.cart.CartModel;
import bandung.orion.com.ethica.utility.FungsiGeneral;

import static bandung.orion.com.ethica.utility.JConst.NAMA_EKSPEDISI_LAIN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT_PENGIRIMAN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_EKSPEDISI;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ITEM;

public class OrderDetailAdapter extends RecyclerView.Adapter {
    Context context;
    List<OrderDetailModel> itemDataModels;

    public OrderDetailAdapter(Context context) {
        this.context = context;
        itemDataModels = new ArrayList<>();
    }

    public void addModels(List<OrderDetailModel> itemDataModels) {
        int pos = this.itemDataModels.size();
        //this.itemDataModels.clear();
        this.itemDataModels.addAll(itemDataModels);
        notifyItemRangeInserted(pos, itemDataModels.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TIPE_VIEW_ITEM) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.order_detail_list_item, parent, false);
            return new ItemHolder(row);
        }else if(viewType == TIPE_VIEW_ALAMAT){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.cart_item_alamat, parent, false);
            return new AlamatHolder(row);
        }else if(viewType == TIPE_VIEW_ALAMAT_PENGIRIMAN){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.cart_item_alamat_pengirim, parent, false);
            return new AlamatKirimHolder(row);
        }else if(viewType == TIPE_VIEW_EKSPEDISI){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.cart_item_ekspedisi, parent, false);
            return new EkspedisiHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OrderDetailAdapter.ItemHolder) {
            final OrderDetailModel mCurrentItem = itemDataModels.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;
            if (!mCurrentItem.getGambar().equals("")){
//            Picasso.with(context).load(mCurrentItem.getGambar()).into(itemHolder.imgGambar);
                Picasso.get().load(mCurrentItem.getGambar()).into(itemHolder.imgGambar);

                if (itemHolder.imgGambar.getDrawable() == null){
                    itemHolder.imgGambar.setImageResource(R.drawable.gambar_tidak_tersedia);
                }
            }else{
                itemHolder.imgGambar.setImageResource(R.drawable.gambar_tidak_tersedia);
            }

            //itemHolder.txtNama.setText(mCurrentItem.getKode()+" - "+mCurrentItem.getNama());
            itemHolder.txtNama.setText(mCurrentItem.getNama());
            itemHolder.txtHarga.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getHarga(), true));
            if (mCurrentItem.getDiskon() != 0){
                itemHolder.txtHarga.setPaintFlags(itemHolder.txtHarga.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                itemHolder.txtHargaDiskon.setVisibility(View.VISIBLE);
                itemHolder.txtHargaDiskon.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getHarga_diskon(), true));
            }else{
                itemHolder.txtHarga.setPaintFlags(itemHolder.txtHarga.getPaintFlags());
                itemHolder.txtHargaDiskon.setVisibility(View.INVISIBLE);
            }
            itemHolder.txtTotal.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getHarga_diskon() * mCurrentItem.getQty(), true));
            itemHolder.txtJumlah.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getQty()));
            itemHolder.txtBerat.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getBerat_total())+" Kg");

            itemHolder.imgGambar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent s = new Intent(context, ZoomImageActivity.class);
                    s.putExtra("GAMBAR", mCurrentItem.getGambar_besar());
                    s.putExtra("NAMA", mCurrentItem.getNama());
                    s.putExtra("KETERANGAN", mCurrentItem.getKeterangan_gambar());
                    s.putExtra("SEQ", mCurrentItem.getBarang_seq());context.startActivity(s);
                }
            });
        }else if (holder instanceof OrderDetailAdapter.AlamatHolder){
            final OrderDetailModel mCurrentItem = itemDataModels.get(position);
            final OrderDetailAdapter.AlamatHolder alamatHolder = (OrderDetailAdapter.AlamatHolder) holder;
            alamatHolder.txtAlamatKirim.setText(mCurrentItem.getAlamat_kirim());
            alamatHolder.txtUbah.setVisibility(View.GONE);
        }else if (holder instanceof OrderDetailAdapter.AlamatKirimHolder){
            final OrderDetailModel mCurrentItem = itemDataModels.get(position);
            final OrderDetailAdapter.AlamatKirimHolder alamatHolder = (OrderDetailAdapter.AlamatKirimHolder) holder;
            alamatHolder.txtAlamatPengirim.setText(mCurrentItem.getAlamat_pengirim());
            alamatHolder.txtUbah.setVisibility(View.GONE);
        }else if (holder instanceof OrderDetailAdapter.EkspedisiHolder){
            final OrderDetailModel mCurrentItem = itemDataModels.get(position);
            final OrderDetailAdapter.EkspedisiHolder ekspedisiHolder = (OrderDetailAdapter.EkspedisiHolder) holder;

            if (!mCurrentItem.getEkspedisi().equals(NAMA_EKSPEDISI_LAIN)){
                ekspedisiHolder.txtEkspedisi.setText((mCurrentItem.getEkspedisi() + " - " + mCurrentItem.getService()).toUpperCase() +
                " \nNo. Resi : " +mCurrentItem.getNo_resi());
            }else{
                ekspedisiHolder.txtEkspedisi.setText((mCurrentItem.getEkspedisi()).toUpperCase() +
                " \nNo. Resi : " +mCurrentItem.getNo_resi());
            }

            ekspedisiHolder.txtEkspedisi.setTextIsSelectable(true);
            ekspedisiHolder.btnEkspedisi.setVisibility(View.GONE);
            ekspedisiHolder.lblNoResi.setVisibility(View.GONE);
            ekspedisiHolder.txtNoResi.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemDataModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemDataModels.get(position).getTipe_view();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtNama, txtHarga, txtHargaDiskon, txtJumlah, txtTotal, txtBerat;
        ImageView imgGambar;

        public ItemHolder(View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtHarga = itemView.findViewById(R.id.txtHarga);
            txtJumlah = itemView.findViewById(R.id.txtJumlah);
            txtTotal = itemView.findViewById(R.id.txtTotalMst);
            txtBerat = itemView.findViewById(R.id.txtBerat);
            imgGambar = itemView.findViewById(R.id.imgGambar);
            txtHargaDiskon = itemView.findViewById(R.id.txtHargaDiskon);

        }
    }

    private class AlamatHolder extends RecyclerView.ViewHolder{
        TextView txtAlamatKirim, txtUbah;
        public AlamatHolder(View itemView) {
            super(itemView);
            txtAlamatKirim = itemView.findViewById(R.id.txtAlamatKirim);
            txtUbah = itemView.findViewById(R.id.txtUbah);
        }
    }

    private class AlamatKirimHolder extends RecyclerView.ViewHolder{
        TextView txtAlamatPengirim, txtUbah;
        public AlamatKirimHolder(View itemView) {
            super(itemView);
            txtAlamatPengirim = itemView.findViewById(R.id.txtAlamatPengirim);
            txtUbah = itemView.findViewById(R.id.txtUbah);
        }
    }


    private class EkspedisiHolder extends RecyclerView.ViewHolder{
        TextView txtEkspedisi, lblNoResi;
        ImageView btnEkspedisi;
        CardView crdView;
        EditText txtNoResi;
        public EkspedisiHolder(View itemView) {
            super(itemView);
            txtEkspedisi = itemView.findViewById(R.id.txtEkspedisi);
            btnEkspedisi = itemView.findViewById(R.id.btnEkspedisi);
            lblNoResi    = itemView.findViewById(R.id.lblNoResi);
            txtNoResi    = itemView.findViewById(R.id.txtNoResi);
            crdView      = itemView.findViewById(R.id.crdView);
        }
    }

}

