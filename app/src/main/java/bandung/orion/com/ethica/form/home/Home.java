package bandung.orion.com.ethica.form.home;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.alamat_kirim.InputAlamatKirim;
import bandung.orion.com.ethica.form.cart.CartFragment;
import bandung.orion.com.ethica.form.cart.InputAlamatCart;
import bandung.orion.com.ethica.form.product_detail_list.ProductDetailList;
import bandung.orion.com.ethica.form.product_list.BadgeDrawerArrowDrawable;
import bandung.orion.com.ethica.form.product_list.ProductListModel;
import bandung.orion.com.ethica.form.profile.login.Login;
import bandung.orion.com.ethica.form.profile.login.LoginModel;
import bandung.orion.com.ethica.form.order_list.OrderListFragment;
import bandung.orion.com.ethica.form.product_list.ProductList;
import bandung.orion.com.ethica.form.profile.ProfileFragment;
import bandung.orion.com.ethica.form.profile.profile_login.profile_login;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_AUTHORIZED;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_ETHICA_HIJAB;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_NEW_PRODUCT;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_PROMO;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_SARIMBIT;
import static bandung.orion.com.ethica.utility.JConst.KLA_DIAMOND;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_TERUPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CUSTOMER_ETHICA;
import static bandung.orion.com.ethica.utility.JConst.TIPE_CUSTOMER_SEPLY;

public class Home extends AppCompatActivity {
    Fragment frgProductList;
    Fragment frgCart;
    Fragment frgOrder;
    Fragment frgProfile;
    BottomNavigationView navigation;
    //juli tambah untuk notif jumlah keranjang
    private View badgeCart, budgeOrderList;
    private TextView txtBadgeCart, txtBudgeOrderList;
    private BottomNavigationItemView itemViewCart, itemViewOrderList;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return loadFragment(frgProductList);
                case R.id.navigation_chart: {
                    if (((EthicaApplication)getApplicationContext()).getCustomerSeqGlobal() == 0)
                        return false;
                    else
                        return loadFragment(frgCart);
                }
                case R.id.navigation_order_list:{
                    if (((EthicaApplication)getApplicationContext()).getCustomerSeqGlobal() == 0)
                        return false;
                    else
                        return loadFragment(frgOrder);
                }
                case R.id.navigation_account:{
                    if (((EthicaApplication)getApplicationContext()).getCustomerSeqGlobal() == 0){
                        Intent s = new Intent(Home.this, Login.class);
                        startActivity(s);
                        return false;
                    }else{
                        return loadFragment(frgProfile);
                    }
                }
            }
            return loadFragment(null);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        InitClass();
        EthicaApplication.getInstance().setHomeGlobal(this);
    }

    private void InitClass(){
        frgProductList = new ProductList();
        frgCart        = new CartFragment();
        frgOrder       = new OrderListFragment();
        frgProfile     = new ProfileFragment();
        loadFragment(frgProductList);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        LoginModel lastLogin = ((EthicaApplication)getApplicationContext()).loginTable.getLastLogin();
        ((EthicaApplication)getApplicationContext()).ApiKey = lastLogin.getApi_key();
        ((EthicaApplication)getApplicationContext()).setTipeCust(lastLogin.getTipe());
        ((EthicaApplication)getApplicationContext()).setCustomerSeqGlobal(lastLogin.getCustomer_seq());
        ((EthicaApplication)getApplicationContext()).setUserIdGlobal(lastLogin.getUser_id());
        ((EthicaApplication)getApplicationContext()).setIsEthicaGlobal(lastLogin.getIs_ethica());
        ((EthicaApplication)getApplicationContext()).setIsSeplyGlobal(lastLogin.getIs_seply());
        ((EthicaApplication)getApplicationContext()).setIsEthicaHijabGlobal(lastLogin.getIs_ethica_hijab());
        ((EthicaApplication)getApplicationContext()).setJenisCustGlobal(lastLogin.getJenis());
        CekExpiredAPI();

        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) navigation.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(1);
        itemViewCart = (BottomNavigationItemView) v;

        BottomNavigationMenuView bottomNavigationMenuView2 = (BottomNavigationMenuView) navigation.getChildAt(0);
        View v2 = bottomNavigationMenuView.getChildAt(2);
        itemViewOrderList = (BottomNavigationItemView) v2;

        badgeCart = LayoutInflater.from(this).inflate(R.layout.notif_count, bottomNavigationMenuView, false);
        txtBadgeCart = badgeCart.findViewById(R.id.notification_badge);
        txtBadgeCart.setText("");

        budgeOrderList = LayoutInflater.from(this).inflate(R.layout.notif_count, bottomNavigationMenuView, false);
        txtBudgeOrderList = budgeOrderList.findViewById(R.id.notification_badge);
        txtBudgeOrderList.setText("");
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    private Fragment getCurrentFragment(){
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
        return currentFragment;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(((EthicaApplication)getApplicationContext()).isLogOn) {
            frgProductList = new ProductList();
            loadFragment(frgProductList);
            ((EthicaApplication)getApplicationContext()).isLogOn = false;
            navigation.setSelectedItemId(R.id.navigation_home);
        }
        LoginModel lastLogin = ((EthicaApplication)getApplicationContext()).loginTable.getLastLogin();
        ((EthicaApplication)getApplicationContext()).ApiKey = lastLogin.getApi_key();
        ((EthicaApplication)getApplicationContext()).setTipeCust(lastLogin.getTipe());
        ((EthicaApplication)getApplicationContext()).setCustomerSeqGlobal(lastLogin.getCustomer_seq());
        ((EthicaApplication)getApplicationContext()).setUserIdGlobal(lastLogin.getUser_id());
        ((EthicaApplication)getApplicationContext()).setIsEthicaGlobal(lastLogin.getIs_ethica());
        ((EthicaApplication)getApplicationContext()).setIsSeplyGlobal(lastLogin.getIs_seply());
        ((EthicaApplication)getApplicationContext()).setIsEthicaHijabGlobal(lastLogin.getIs_ethica_hijab());
        ((EthicaApplication)getApplicationContext()).setJenisCustGlobal(lastLogin.getJenis());
        CekExpiredAPI();
        updateJumlahKeranjang();
        updateJumlahPesanOrderList();
    }


    private void CekExpiredAPI(){
        String url;
        url = Routes.url_cek_API+"?key="+EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode()+"&tipe_apps="+TIPE_APPS_ANDROID;
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);

                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                        return;
                    }

                    if (!jObj.getString("status").equals(API_KEY_AUTHORIZED)){
                        ((EthicaApplication)getApplicationContext()).loginTable.deleteAll();
                        ((EthicaApplication)getApplicationContext()).ApiKey = "";
                        ((EthicaApplication)getApplicationContext()).setTipeCust("");
                        ((EthicaApplication)getApplicationContext()).setCustomerSeqGlobal(0);
                        ((EthicaApplication)getApplicationContext()).setUserIdGlobal("");
                        ((EthicaApplication)getApplicationContext()).setIsEthicaGlobal("");
                        ((EthicaApplication)getApplicationContext()).setIsSeplyGlobal("");
                        ((EthicaApplication)getApplicationContext()).setIsEthicaHijabGlobal("");
                        ((EthicaApplication)getApplicationContext()).setJenisCustGlobal("");
                        ((EthicaApplication)getApplicationContext()).isLogOn = false;
                        if (EthicaApplication.getInstance().getCustomerSeqGlobal() > 0){
                            Toast.makeText(getApplicationContext(), "Sesi login telah berakhir", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
    }

    public void backHome(){
        ((EthicaApplication)getApplicationContext()).viewTempProduct = null;
        frgProductList = new ProductList();
        loadFragment(frgProductList);
        navigation.setSelectedItemId(R.id.navigation_home);
        updateJumlahKeranjang();
        updateJumlahPesanOrderList();
    }

    public void updateJumlahKeranjang(){
        if (EthicaApplication.getInstance().getCustomerSeqGlobal() > 0){
            String url = Routes.url_get_count_cart+"/"+EthicaApplication.getInstance().getCustomerSeqGlobal()+
                        "?tipe_customer="+EthicaApplication.getInstance().getTipeCust()+
                        "&user_id="+EthicaApplication.getInstance().getUserIdGlobal()+
                        "&versi="+EthicaApplication.getInstance().getVersionCode();
            JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    for (int i = 0; i < response.length(); i++) {
                        try { JSONObject obj = response.getJSONObject(i);
                            if (obj.getInt("total") > 0){
                                txtBadgeCart.setText(" "+obj.getString("total")+" ");
                                itemViewCart.removeView(badgeCart);
                                itemViewCart.addView(badgeCart);
                            }else{
                                itemViewCart.removeView(badgeCart);
                            }
                        } catch (JSONException e) {
                            itemViewCart.removeView(badgeCart);
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError e) {
                    itemViewCart.removeView(badgeCart);
                }
            });
            EthicaApplication.getInstance().addToRequestQueue(jArr);
        }else{
            itemViewCart.removeView(badgeCart);
        }
    }

    public void updateJumlahPesanOrderList(){
        if (EthicaApplication.getInstance().getCustomerSeqGlobal() > 0){
            String url = Routes.url_get_chat_box+"?customer_seq="+EthicaApplication.getInstance().getCustomerSeqGlobal()+
                        "&user_id="+EthicaApplication.getInstance().getUserIdGlobal()+
                        "&versi="+EthicaApplication.getInstance().getVersionCode();

            JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    for (int i = 0; i < response.length(); i++) {
                        try { JSONObject obj = response.getJSONObject(i);
                            if (obj.getInt("total") > 0){
                                txtBudgeOrderList.setText(" "+obj.getString("total")+" ");
                                itemViewOrderList.removeView(budgeOrderList);
                                itemViewOrderList.addView(budgeOrderList);
                            }else{
                                itemViewOrderList.removeView(budgeOrderList);
                            }
                        } catch (JSONException e) {
                            itemViewOrderList.removeView(budgeOrderList);
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError e) {
                    itemViewOrderList.removeView(budgeOrderList);
                }
            });
            EthicaApplication.getInstance().addToRequestQueue(jArr);
        }else{
            itemViewOrderList.removeView(budgeOrderList);
        }
    }


    public void AktifkanCart(){
        if (((EthicaApplication)getApplicationContext()).getCustomerSeqGlobal() > 0){
            loadFragment(frgCart);
            navigation.setSelectedItemId(R.id.navigation_chart);
        }
    }

    private String getCertificate() {
        String certificate = null;
        PackageManager pm = this.getPackageManager();
        String packageName = this.getPackageName();
        int flags = PackageManager.GET_SIGNATURES;
        PackageInfo packageInfo = null;
        try {
            packageInfo = pm.getPackageInfo(packageName, flags);
        } catch (PackageManager.NameNotFoundException e) {
        }
        Signature[] signatures = packageInfo.signatures;
        byte[] cert = signatures[0].toByteArray();
        InputStream input = new ByteArrayInputStream(cert);
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X509");
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        X509Certificate c = null;
        try {
            c = (X509Certificate) cf.generateCertificate(input);
        } catch (CertificateException e) {
        }
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(c.getPublicKey().getEncoded());
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < publicKey.length; i++) {
                String appendString = Integer.toHexString(0xFF & publicKey[i]);
                if (appendString.length() == 1)
                    hexString.append("0");
                hexString.append(appendString);
            }
            certificate = hexString.toString();
        } catch (NoSuchAlgorithmException e1) {
        }
        return certificate + ";" + packageName;
    }
}