package bandung.orion.com.ethica.form.order_list_sub_agen;

public class OrderListSubAgenModel {
    private long seq;
    private long customer_seq;
    private String nama_customer;
    private String tanggal;
    private String nomor;
    private double total;
    private double subtotal;
    private double diskon;
    private double total_berat;
    private double ongkos_kirim;
    private String status;
    private int notif;

    public OrderListSubAgenModel(long seq, String tanggal, String nomor, double total, double diskon, double subtotal, String status, Double total_berat, Double ongkos_kirim, long customer_seq, String nama_customer) {
        this.seq = seq;
        this.tanggal = tanggal;
        this.nomor = nomor;
        this.total = total;
        this.diskon = diskon;
        this.subtotal = subtotal;
        this.status = status;
        this.notif = 0;
        this.total_berat = total_berat;
        this.ongkos_kirim = ongkos_kirim;
        this.customer_seq = customer_seq;
        this.nama_customer = nama_customer;
    }

    public OrderListSubAgenModel() {
        this.seq = 0;
        this.tanggal = "";
        this.nomor = "";
        this.total = 0;
        this.diskon = 0;
        this.subtotal = 0;
        this.status = "";
        this.notif = 0;
        this.total_berat = 0;
        this.ongkos_kirim = 0;
        this.customer_seq = 0;
        this.nama_customer = "";
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getDiskon() {
        return diskon;
    }

    public void setDiskon(double diskon) {
        this.diskon = diskon;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNotif() {
        return notif;
    }

    public void setNotif(int notif) {
        this.notif = notif;
    }

    public double getTotal_berat() {
        return total_berat;
    }

    public void setTotal_berat(double total_berat) {
        this.total_berat = total_berat;
    }

    public double getOngkos_kirim() {
        return ongkos_kirim;
    }

    public void setOngkos_kirim(double ongkos_kirim) {
        this.ongkos_kirim = ongkos_kirim;
    }

    public long getCustomer_seq() {
        return customer_seq;
    }

    public void setCustomer_seq(long customer_seq) {
        this.customer_seq = customer_seq;
    }

    public String getNama_customer() {
        return nama_customer;
    }

    public void setNama_customer(String nama_customer) {
        this.nama_customer = nama_customer;
    }
}
