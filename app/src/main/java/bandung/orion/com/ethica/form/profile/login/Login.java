package bandung.orion.com.ethica.form.profile.login;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;

public class Login extends AppCompatActivity {
    AnimationDrawable animationDrawable;
    ConstraintLayout Layouts;
    Button BtnLogin;
    EditText txtUserId, txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        Layouts = (ConstraintLayout)findViewById(R.id.Layouts);
        txtUserId = (EditText) findViewById(R.id.txtUserId);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        BtnLogin = (Button) findViewById(R.id.BtnLogin);

        BtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtUserId.getText().toString().trim().equals("")){
                    Toast.makeText(Login.this, "User id belum diisi.", Toast.LENGTH_SHORT).show();
                    txtUserId.requestFocus();
                    return;
                }

                if (txtPassword.getText().toString().trim().equals("")){
                    Toast.makeText(Login.this, "Password belum diisi.", Toast.LENGTH_SHORT).show();
                    txtPassword.requestFocus();
                    return;
                }
                Login();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (animationDrawable != null && animationDrawable.isRunning()){
            animationDrawable.stop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (animationDrawable != null && !animationDrawable.isRunning()){
            animationDrawable.start();
        }
    }


    private void Login(){
        String url;
        url = Routes.url_login_master_customer;
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    if (!jObj.getString("status").equals("Failed")){
                        int SeqCustomer = jObj.getInt("id_customer");
                        String key = jObj.getString("api_key");
                        String is_ethica = jObj.getString("is_ethica");
                        String is_seply = jObj.getString("is_seply");
                        String is_ethica_hijab = jObj.getString("is_ethica_hijab");
                        String jenis = jObj.getString("jenis");

                        save_login(SeqCustomer, key, "", is_ethica, is_seply, is_ethica_hijab, jenis);
                        Intent intent = getIntent();
                        setResult(RESULT_OK, intent);
                        onBackPressed();
                    }else{
                        Toast.makeText(Login.this, "User id atau password salah.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(Login.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Login.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user", txtUserId.getText().toString());
                params.put("pass", txtPassword.getText().toString());
                params.put("tipe_apps", TIPE_APPS_ANDROID);
                return params;
            }
        };

        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
    }

    private void save_login(int customer_seq, String key, String tipe, String is_ethica, String is_seply, String is_ethica_hijab, String jenis){
        LoginTable loginTable = ((EthicaApplication)getApplicationContext()).loginTable;
        LoginModel item = new LoginModel();

        item.setCustomer_seq(customer_seq);
        item.setApi_key(key);
        item.setTipe(tipe);
        item.setUser_id(txtUserId.getText().toString());
        item.setIs_ethica(is_ethica);
        item.setIs_seply(is_seply);
        item.setIs_ethica_hijab(is_ethica_hijab);
        item.setJenis(jenis);
        loginTable.insert(item);
        ((EthicaApplication)getApplicationContext()).setCustomerSeqGlobal(customer_seq);
        ((EthicaApplication)getApplicationContext()).ApiKey = key;
        ((EthicaApplication)getApplicationContext()).setTipeCust(tipe);
        ((EthicaApplication)getApplicationContext()).setUserIdGlobal(txtUserId.getText().toString());
        ((EthicaApplication)getApplicationContext()).setIsEthicaGlobal(is_ethica);
        ((EthicaApplication)getApplicationContext()).setIsSeplyGlobal(is_seply);
        ((EthicaApplication)getApplicationContext()).setIsEthicaHijabGlobal(is_ethica_hijab);
        ((EthicaApplication)getApplicationContext()).setJenisCustGlobal(jenis);
        ((EthicaApplication)getApplicationContext()).isLogOn = true;
        ((EthicaApplication)getApplicationContext()).viewTempProduct = null;
    };
}
