package bandung.orion.com.ethica.form.profile.login;

public class LoginModel {
    private long customer_seq;
    private String api_key;
    private String tipe;
    private String user_id, is_ethica, is_seply, is_ethica_hijab, jenis;


    public long getCustomer_seq() {
        return customer_seq;
    }

    public void setCustomer_seq(long customer_seq) {
        this.customer_seq = customer_seq;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public LoginModel(long customer_seq, String api_key, String tipe, String user_id, String is_ethica, String is_seply, String is_ethica_hijab, String jenis) {
        this.customer_seq = customer_seq;
        this.api_key = api_key;
        this.tipe = tipe;
        this.user_id = user_id;
        this.is_ethica = is_ethica;
        this.is_seply = is_seply;
        this.is_ethica_hijab = is_ethica_hijab;
        this.jenis = jenis;
    }

    public LoginModel() {
        this.customer_seq = 0;
        this.api_key = "";
        this.tipe = "";
        this.is_ethica = "";
        this.is_seply = "";
        this.is_ethica_hijab = "";
        this.jenis = "";
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIs_ethica() {
        return is_ethica;
    }

    public void setIs_ethica(String is_ethica) {
        this.is_ethica = is_ethica;
    }

    public String getIs_seply() {
        return is_seply;
    }

    public void setIs_seply(String is_seply) {
        this.is_seply = is_seply;
    }

    public String getIs_ethica_hijab() {
        return is_ethica_hijab;
    }

    public void setIs_ethica_hijab(String is_ethica_hijab) {
        this.is_ethica_hijab = is_ethica_hijab;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
}