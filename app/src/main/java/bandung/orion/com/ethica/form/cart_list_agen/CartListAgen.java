package bandung.orion.com.ethica.form.cart_list_agen;

import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.EthicaApplication.fmt;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_SARIMBIT;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static java.lang.Math.round;

public class CartListAgen extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener{
    private TextView txtNama, txtNoTelpon, txtAlamat, txtJumlah;
    public RecyclerView RcvData;
    public CartListAdapter mAdapter;
    private SwipeRefreshLayout swipe;
    private SearchView txtSearch;

    int visibleThreshold = 6;
    int lastVisibleItem, totalItemCount;
    boolean IsLoading;
    ILoadMore loadMore;

    private LinearLayoutManager linearLayoutManager;
    public List<CartListModel> ListItems = new ArrayList<>();

    private CollapsingToolbarLayout collapsing;

    private int NoOfColumns;
    private int CountAllVew;
    final List<View> pageList = new ArrayList<>();

    private AppBarLayout appbarLayout;
    private int mMaxScrollSize;

    private int customer_seq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list_agen);
        CreateView();
        InitClass();
        EventClass();
        LoadMaster();
    }

    private void CreateView(){
        this.txtNama = (TextView) findViewById(R.id.txtNama);
        this.txtNoTelpon = (TextView) findViewById(R.id.txtNoTelpon);
        this.txtAlamat = (TextView) findViewById(R.id.txtAlamat);
        this.txtJumlah = (TextView) findViewById(R.id.txtJumlah);
        this.appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        this.RcvData = (RecyclerView) findViewById(R.id.RcvData);
        this.swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        this.txtSearch = (SearchView) findViewById(R.id.txtSearch);
        this.collapsing = findViewById(R.id.collapsing);
    }

    private void InitClass(){
        NoOfColumns = 1;
        SetJenisTampilan();

        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();
        Toolbar toolbar = (Toolbar) findViewById(R.id.ToolbarAct);
        this.setSupportActionBar(toolbar);
        CountAllVew = 0;
        SetHeightCarousel();

        Bundle extra   = this.getIntent().getExtras();
        customer_seq     = extra.getInt("CUSTOMER_SEQ");
    }

    protected void EventClass(){
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!IsLoading){
                    mAdapter.removeAllModel();
                    RefreshRecyclerView();
                }
                swipe.setRefreshing(false);
            }
        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                mAdapter.addMoel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.removeMoel(ListItems.size()-1);
                        LoadData();
                    }
                },1000);
            }
        });

        RcvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

        txtSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        txtSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0){
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        }
    }

    private void LoadMaster() {
        String Filter = "";
        Filter += "?customer_seq="+String.valueOf(customer_seq);
        String url = Routes.url_get_sub_agen_customer + Filter;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            txtNama.setText(obj.getString("nama"));
                            txtNoTelpon.setText(obj.getString("no_telp"));
                            txtJumlah.setText(fmt.format(  obj.getDouble("jumlah")));

                            String Alamat = "";
                            if (!obj.getString("alamat").equals("")) {Alamat = Alamat +obj.getString("alamat");}
                            if (!obj.getString("kecamatan").equals("")) {Alamat = Alamat +", "+obj.getString("kecamatan");}
                            if (!obj.getString("kota").equals("")) {Alamat = Alamat +", "+obj.getString("kota");}
                            if (!obj.getString("provinsi").equals("")) {Alamat = Alamat +", "+obj.getString("provinsi");}
                            txtAlamat.setText(Alamat);
                            LoadData();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                setLoaded(false);
                Toast.makeText(getApplicationContext(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void LoadData(){
        String Filter = "";
        Filter += "?customer_seq="+String.valueOf(customer_seq)+"&search=" + txtSearch.getQuery()+"&offset=" + Integer.toString(mAdapter.getItemCount());
        String url = Routes.url_load_cart_sub_agen + Filter;
        url = url.replace(" ", "%20");
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<CartListModel> itemDataModels = new ArrayList<>();
                CartListModel Data;
                itemDataModels.clear();

                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("barang_seq") > 0){
                            Data = new CartListModel(
                                    obj.getInt("barang_seq"),
                                    obj.getString("nama"),
                                    obj.getString("gambar"),
                                    obj.getInt("jumlah"),
                                    obj.getDouble("stok"),
                                    obj.getDouble("harga")
                            );
                            Data.setKeterangan(obj.getString("keterangan"));
                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(false);
                    }
                }
                mAdapter.addModels(itemDataModels);
                setLoaded(false);
                swipe.setRefreshing(false);
                if (ListItems.size() <= 0){Toast.makeText(getApplicationContext(), "Tidak ditemukan", Toast.LENGTH_SHORT).show();}
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                swipe.setRefreshing(false);
                setLoaded(false);
                Toast.makeText(getApplicationContext(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }


    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }

    private void SetJenisTampilan(){
        mAdapter = new CartListAdapter(CartListAgen.this, ListItems,  R.layout.list_item_cart_list);
        RcvData.setLayoutManager(new GridLayoutManager(CartListAgen.this, NoOfColumns, GridLayoutManager.VERTICAL, false));
        linearLayoutManager = (LinearLayoutManager)RcvData.getLayoutManager();
        RcvData.setAdapter(mAdapter);
    }

    private void SetHeightCarousel(){
        AppBarLayout.LayoutParams layoutParams = (AppBarLayout.LayoutParams) collapsing.getLayoutParams();
        collapsing.setLayoutParams(layoutParams);
    }
}