package bandung.orion.com.ethica.form.filter_customer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.cart_list_agen.CartListAgen;

import static bandung.orion.com.ethica.utility.JConst.MODE_FILTER_CUSTOMER_CART;
import static bandung.orion.com.ethica.utility.JConst.MODE_FILTER_CUSTOMER_ORDER;

public class FilterCustomerAdapter extends RecyclerView.Adapter {
    Context context;
    List<FilterCustomerModel> Datas;
    private final int VIEW_TYVE_ITEM = 0, VIEW_TYVE_LOADING = 1;
    private ProgressDialog Loading;
    private int ViewAlamat;
    private String mode;
    private FilterCustomer Activity;

    public FilterCustomerAdapter(Context context, List<FilterCustomerModel> Datas, int view, String mode, FilterCustomer Activity) {
        this.mode = mode;
        this.context = context;
        this.Datas = Datas;
        this.Loading = new ProgressDialog(context);
        this.ViewAlamat = view;
        this.Activity = Activity;
    }

    public void addModels(List<FilterCustomerModel> Datas) {
        int pos = this.Datas.size();
        this.Datas.addAll(Datas);
        notifyItemRangeInserted(pos, Datas.size());
    }

    public void addMoel(FilterCustomerModel FilterCustomerModel) {
        this.Datas.add(FilterCustomerModel);
        notifyItemRangeInserted(Datas.size()-1,Datas.size()-1);
    }

    public void removeMoel(int idx) {
        if (Datas.size() > 0){
            this.Datas.remove(Datas.size()-1);
            notifyItemRemoved(Datas.size());
        }
    }

    public void removeAllModel(){
        int LastPosition = Datas.size();
        this.Datas.removeAll(Datas);
        notifyItemRangeRemoved(0, LastPosition);
    }

    public void SetJenisView(int view){
        this.ViewAlamat = view;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYVE_ITEM){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(ViewAlamat, parent, false);
            return new ItemHolder(row);
        }else if(viewType == VIEW_TYVE_LOADING){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemHolder){
            final FilterCustomerModel mCurrentItem = Datas.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;

            itemHolder.txtNama.setText(mCurrentItem.getNama());
            itemHolder.txtNoTelpon.setText(mCurrentItem.getNo_telpon());
            itemHolder.txtAlamat.setText(mCurrentItem.getAlamat());

            itemHolder.crdView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mode.equals(MODE_FILTER_CUSTOMER_CART)){
                        Intent s = new Intent(context, CartListAgen.class);
                        s.putExtra("CUSTOMER_SEQ", mCurrentItem.getSeq());
                        context.startActivity(s);
                    }else if (mode.equals(MODE_FILTER_CUSTOMER_ORDER)){
                        Activity.SetResult(mCurrentItem.getSeq(), mCurrentItem.getNama());
                    }
                }
            });

        }else if (holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return Datas.get(position) == null ? VIEW_TYVE_LOADING : VIEW_TYVE_ITEM;
    }

    @Override
    public int getItemCount() {
        return Datas.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtNama, txtNoTelpon, txtAlamat;
        CardView crdView;

        public ItemHolder(View itemView) {
            super(itemView);
            txtNama    = itemView.findViewById(R.id.txtNama);
            txtNoTelpon = itemView.findViewById(R.id.txtNoTelpon);
            txtAlamat  = itemView.findViewById(R.id.txtAlamat);
            crdView = itemView.findViewById(R.id.crdView);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pg_loading);
        }
    }
}
