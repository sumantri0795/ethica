package bandung.orion.com.ethica.form.Ekspedisi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.alamat_kirim.InputAlamatKirimCartActivity;
import bandung.orion.com.ethica.form.cart.CartFragment;
import bandung.orion.com.ethica.form.cart.CartModel;
import bandung.orion.com.ethica.form.cart_list_agen.CartListAgen;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.NAMA_EKSPEDISI_LAIN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT_PENGIRIMAN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_EKSPEDISI;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ITEM;

public class PilihEkspedisiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<PilihEkspedisiModel> Datas;
    List<List<PilihEkspedisiModel>> Childs;
    private final int VIEW_TYVE_ITEM = 0, VIEW_TYVE_LOADING = 1;
    private ProgressDialog Loading;
    private Intent intent;
    private Activity mActivity;
    List<Integer> counter = new ArrayList<Integer>();

    public PilihEkspedisiAdapter(Context context, List<PilihEkspedisiModel> Datas, List<List<PilihEkspedisiModel>> Childs, Intent intent, Activity mActivity) {
        this.context = context;
        this.Datas = Datas;
        this.Childs = Childs;
        this.Loading = new ProgressDialog(context);
        this.intent = intent;
        this.mActivity = mActivity;

        for (int i = 0; i < Datas.size(); i++) {
            counter.add(0);
        }
    }

    public void addModels(List<PilihEkspedisiModel> Datas) {
        int pos = this.Datas.size();
        this.Datas.addAll(Datas);
        notifyItemRangeInserted(pos, Datas.size());

        counter.clear();
        for (int i = 0; i < Datas.size(); i++) {
            counter.add(0);
        }
    }

    public void addMoel(PilihEkspedisiModel PilihEkspedisiModel) {
        this.Datas.add(PilihEkspedisiModel);
        notifyItemRangeInserted(Datas.size()-1,Datas.size()-1);
    }

    public void removeMoel(int idx) {
        if (Datas.size() > 0){
            this.Datas.remove(Datas.size()-1);
            notifyItemRemoved(Datas.size());
        }
    }

    public void removeAllModel(){
        int LastPosition = Datas.size();
        this.Datas.removeAll(Datas);
        notifyItemRangeRemoved(0, LastPosition);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYVE_ITEM){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.pilih_ekspedisi_list_item, parent, false);
            return new PilihEkspedisiAdapter.ItemHolder(row);
        }else if(viewType == VIEW_TYVE_LOADING){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_loading, parent, false);
            return new PilihEkspedisiAdapter.LoadingViewHolder(row);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PilihEkspedisiAdapter.ItemHolder){
            final PilihEkspedisiModel mCurrentItem = Datas.get(position);
            final PilihEkspedisiAdapter.ItemHolder itemHolder = (PilihEkspedisiAdapter.ItemHolder) holder;

//            if (!mCurrentItem.getKode().equals(NAMA_EKSPEDISI_LAIN)){
//                itemHolder.txtEkspedisi.setText((mCurrentItem.getKode() + " - " + mCurrentItem.getService()).toUpperCase());
//            }else{
//                itemHolder.txtEkspedisi.setText((mCurrentItem.getKode()));
//            }

            itemHolder.txtEkspedisi.setText((mCurrentItem.getKode()));

            PilihEkspedisiChildExpAdapter itemInnerRecyclerView = new PilihEkspedisiChildExpAdapter(Childs.get(position), intent, mActivity);
            itemHolder.cardRecyclerView.setLayoutManager(new GridLayoutManager(context, 1));

            itemHolder.crdView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (counter.get(position) % 2 == 0) {
                        itemHolder.cardRecyclerView.setVisibility(View.VISIBLE);
                    } else {
                        itemHolder.cardRecyclerView.setVisibility(View.GONE);
                    }

                    counter.set(position, counter.get(position) + 1);
                }
            });
            itemHolder.cardRecyclerView.setAdapter(itemInnerRecyclerView);

//            itemHolder.crdView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    intent.putExtra("ekspedisi", Datas.get(position).getKode());
//                    intent.putExtra("service", Datas.get(position).getService());
//                    intent.putExtra("ekspedisi_seq", Datas.get(position).getSeq());
//                    mActivity.setResult(Activity.RESULT_OK, intent);
//                    mActivity.finish();
//                }
//            });

        }else if (holder instanceof PilihEkspedisiAdapter.LoadingViewHolder){
            PilihEkspedisiAdapter.LoadingViewHolder loadingViewHolder = (PilihEkspedisiAdapter.LoadingViewHolder)holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return Datas.get(position) == null ? VIEW_TYVE_LOADING : VIEW_TYVE_ITEM;
    }

    @Override
    public int getItemCount() {
        return Datas.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtEkspedisi;
        CardView crdView;
        RecyclerView cardRecyclerView;

        public ItemHolder(View itemView) {
            super(itemView);
            txtEkspedisi    = itemView.findViewById(R.id.txtEkspedisi);
            crdView = itemView.findViewById(R.id.crdView);
            cardRecyclerView = itemView.findViewById(R.id.childRecyclerView);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pg_loading);
        }
    }
}