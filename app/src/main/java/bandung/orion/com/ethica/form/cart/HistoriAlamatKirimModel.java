package bandung.orion.com.ethica.form.cart;

public class HistoriAlamatKirimModel {
    String nama_kirim, kelurahan, provinsi, kota, kecamatan, alamat_kirim_lengkap, no_telepon_kirim, alamat_kirim;
    int id_provinsi, id_kota, id_kecamatan;

    public HistoriAlamatKirimModel(String nama_kirim, String kelurahan, String provinsi, String kota, String kecamatan, String alamat_kirim_lengkap, String no_telepon_kirim, int id_provinsi, int id_kota, int id_kecamatan, String alamat_kirim) {
        this.nama_kirim = nama_kirim;
        this.kelurahan = kelurahan;
        this.provinsi = provinsi;
        this.kota = kota;
        this.kecamatan = kecamatan;
        this.alamat_kirim_lengkap = alamat_kirim_lengkap;
        this.no_telepon_kirim = no_telepon_kirim;
        this.id_provinsi = id_provinsi;
        this.id_kota = id_kota;
        this.id_kecamatan = id_kecamatan;
        this.alamat_kirim = alamat_kirim;
    }


    public HistoriAlamatKirimModel() {
        this.nama_kirim = "";
        this.kelurahan = "";
        this.provinsi = "";
        this.kota = "";
        this.kecamatan = "";
        this.alamat_kirim_lengkap = "";
        this.no_telepon_kirim = "";
        this.id_provinsi = 0;
        this.id_kota = 0;
        this.id_kecamatan = 0;
        this.alamat_kirim = "";
    }

    public String getNama_kirim() {
        return nama_kirim;
    }

    public void setNama_kirim(String nama_kirim) {
        this.nama_kirim = nama_kirim;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getAlamat_kirim_lengkap() {
        return alamat_kirim_lengkap;
    }

    public void setAlamat_kirim_lengkap(String alamat_kirim_lengkap) {
        this.alamat_kirim_lengkap = alamat_kirim_lengkap;
    }

    public String getNo_telepon_kirim() {
        return no_telepon_kirim;
    }

    public void setNo_telepon_kirim(String no_telepon_kirim) {
        this.no_telepon_kirim = no_telepon_kirim;
    }

    public int getId_provinsi() {
        return id_provinsi;
    }

    public void setId_provinsi(int id_provinsi) {
        this.id_provinsi = id_provinsi;
    }

    public int getId_kota() {
        return id_kota;
    }

    public void setId_kota(int id_kota) {
        this.id_kota = id_kota;
    }

    public int getId_kecamatan() {
        return id_kecamatan;
    }

    public void setId_kecamatan(int id_kecamatan) {
        this.id_kecamatan = id_kecamatan;
    }

    public String getAlamat_kirim() {
        return alamat_kirim;
    }

    public void setAlamat_kirim(String alamat_kirim) {
        this.alamat_kirim = alamat_kirim;
    }
}
