package bandung.orion.com.ethica.form.sarimbit;

public class SarimbitListModel {
    private long seq;
    private String nama;
    private long tgl_berlaku_dari;
    private long tgl_berlaku_sampai;
    private String kelompok_brand;
    private String keterangan;
    private String user_id;
    private long disable_date;
    private String gambar;
    private String gambar_sedang;
    private String gambar_besar;

    public SarimbitListModel(long seq, String nama, long tgl_berlaku_dari, long tgl_berlaku_sampai, String kelompok_brand, String keterangan, String user_id, long disable_date, String gambar, String gambar_sedang, String gambar_besar) {
        this.seq = seq;
        this.nama = nama;
        this.tgl_berlaku_dari = tgl_berlaku_dari;
        this.tgl_berlaku_sampai = tgl_berlaku_sampai;
        this.kelompok_brand = kelompok_brand;
        this.keterangan = keterangan;
        this.user_id = user_id;
        this.disable_date = disable_date;
        this.gambar = gambar;
        this.gambar_sedang = gambar_sedang;
        this.gambar_besar = gambar_besar;
    }


    public SarimbitListModel() {
        this.seq = 0;
        this.nama = "";
        this.tgl_berlaku_dari = 0;
        this.tgl_berlaku_sampai = 0;
        this.kelompok_brand = "";
        this.keterangan = "";
        this.user_id = "";
        this.disable_date = 0;
        this.gambar = "";
        this.gambar_sedang = "";
        this.gambar_besar = "";
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getTgl_berlaku_dari() {
        return tgl_berlaku_dari;
    }

    public void setTgl_berlaku_dari(long tgl_berlaku_dari) {
        this.tgl_berlaku_dari = tgl_berlaku_dari;
    }

    public long getTgl_berlaku_sampai() {
        return tgl_berlaku_sampai;
    }

    public void setTgl_berlaku_sampai(long tgl_berlaku_sampai) {
        this.tgl_berlaku_sampai = tgl_berlaku_sampai;
    }

    public String getKelompok_brand() {
        return kelompok_brand;
    }

    public void setKelompok_brand(String kelompok_brand) {
        this.kelompok_brand = kelompok_brand;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public long getDisable_date() {
        return disable_date;
    }

    public void setDisable_date(long disable_date) {
        this.disable_date = disable_date;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getGambar_sedang() {
        return gambar_sedang;
    }

    public void setGambar_sedang(String gambar_sedang) {
        this.gambar_sedang = gambar_sedang;
    }

    public String getGambar_besar() {
        return gambar_besar;
    }

    public void setGambar_besar(String gambar_besar) {
        this.gambar_besar = gambar_besar;
    }
}
