package bandung.orion.com.ethica.form.ZoomImage;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.product_list.ProductListModel;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.getMillisDate;
import static bandung.orion.com.ethica.utility.JConst.KLA_NOT_DIAMOND;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static java.lang.Math.round;

public class ZoomImageActivity extends AppCompatActivity {
    private PhotoView photo;
    private TextView txtDeskripsi, txtKeteranganDiskon, txtDiskon, txtBerat;
    private ProgressDialog Loading;
    private ImageButton btnDownload, btnClose;
    private int ResultGambar;
    private boolean IsAdaGambar;
    private String gambar;
    private String nama;
    private String keterangan;
    private CardView CardDiskon;
    private long seq;
    private int berat;    private ProductListModel Data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        CreateView();
        InitView();
        EventClass();
        LoadData();
    }

    private void InitView() {
        Bundle extra = this.getIntent().getExtras();
        gambar = extra.getString("GAMBAR");
        nama = extra.getString("NAMA");
        keterangan = extra.getString("KETERANGAN");
        seq = extra.getLong("SEQ");
        berat = extra.getInt("BERAT");
        this.Loading = new ProgressDialog(ZoomImageActivity.this);
        Data = new ProductListModel();
    }

    private void CreateView(){
        photo = findViewById(R.id.imgSizePack);
        btnClose = findViewById(R.id.btnClose);
        btnDownload = findViewById(R.id.btnDownload);
        txtDeskripsi = findViewById(R.id.txtDeskripsi);
        txtKeteranganDiskon = findViewById(R.id.txtKeteranganDiskon);
        CardDiskon = findViewById(R.id.CardDiskon);
        txtDiskon = findViewById(R.id.txtDiskon);
        txtBerat = findViewById(R.id.txtBerat);
    }

    private void EventClass() {
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsAdaGambar){
                    ResultGambar = 0;
                    String Path = CreateGetDir();
                    if (!gambar.equals("")){
                        Picasso.get().load(gambar).into(picassoImageTarget(Path, nama+".jpg"));

                        int i = 0;
                        do {
                            i = 0;
                        }while (ResultGambar == 0);
                        if (ResultGambar == 1) {
                            Toast.makeText(getApplicationContext(), "Gambar berhasil disimpan di : "+Path+"/"+nama, Toast.LENGTH_SHORT).show();

                            //Koding agar setelah download langsung masuk ke galeri
                            MediaScannerConnection.scanFile(getApplicationContext(), new String[]{Path+"/"+nama+".jpg"},null, new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {
                                    // now visible in gallery
                                }
                            });
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Gambar tidak tersedia", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Gambar tidak tersedia", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void LoadData() {
        Loading.setMessage("Loading...");
        Loading.setCancelable(false);
        Loading.show();
        IsAdaGambar = false;

        LoadDataDiskon();
        LoadDataBarang();

        if ((gambar!= null) && (!gambar.equals(""))){
            Picasso.get().load(gambar).into(photo, new Callback() {
                @Override
                public void onSuccess() {
                    IsAdaGambar = true;
                }

                @Override
                public void onError(Exception e) {
                    IsAdaGambar = false;
                }
            });
        }else{
            Picasso.get().load(R.drawable.gambar_tidak_tersedia).into(photo);
            IsAdaGambar = false;
        }
    }

    public void LoadDataBarang(){

        String url = Routes.url_get_barang + "/"+seq;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                String berat = "0";
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            Data = new ProductListModel(
                                    obj.getInt("seq"),
                                    obj.getString("barcode"),
                                    obj.getString("nama"),
                                    obj.getString("brand"),
                                    obj.getString("sub_brand"),
                                    obj.getString("warna"),
                                    obj.getString("ukuran"),
                                    obj.getString("katalog"),
                                    getMillisDate(obj.getString("tahun")),
                                    obj.getString("tipe_brg"),
                                    obj.getString("gambar"),
                                    obj.getDouble("harga"),
                                    getMillisDate(obj.getString("tgl_hapus")),
                                    0,
                                    0,
                                    obj.getString("gambar_besar"),
                                    obj.getString("keterangan")
                            );
                            berat = obj.getString("berat");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ZoomImageActivity.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    }
                }

                txtDeskripsi.setText(Data.getKeterangan());
                txtBerat.setText(berat+ " Kg");
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();

                Toast.makeText(ZoomImageActivity.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    private void LoadDataDiskon(){
        txtKeteranganDiskon.setText("");
        txtDiskon.setText("");
        String url = Routes.url_load_diskon_barang + "/"+seq;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                String keterangan = "";
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        keterangan += "Beli "+obj.getString("range_mulai")+" - "+obj.getString("range_sampai") +
                                " diskon "+obj.getString("diskon_pct")+"%";
                        if (i < response.length()-1){
                            keterangan += "\n";
                        }
                        txtDiskon.setText("Up to "+obj.getString("diskon_pct")+"%");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ZoomImageActivity.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    }
                }
                txtKeteranganDiskon.setText(keterangan);
                if (keterangan.equals("")){
                    CardDiskon.setVisibility(View.GONE);
                    txtDiskon.setVisibility(View.GONE);
                }
                Loading.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                Loading.dismiss();
                e.printStackTrace();
                Toast.makeText(ZoomImageActivity.this, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    //target to save
    private Target picassoImageTarget(final String imageDir, final String imageName) {
        final File directory = new File(imageDir);
        return new Target() {

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (bitmap!= null){
                            final File myImageFile = new File(directory, imageName); // Create image file
                            FileOutputStream fos = null;
                            try {
                                fos = new FileOutputStream(myImageFile);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                ResultGambar = 1;
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            ResultGambar = 2;
                        }
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                ResultGambar = 2;
                Toast.makeText(getApplicationContext(), "Gambar gagal disimpan", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {}
            }
        };
    }

    private String CreateGetDir() {
        File direct = new File("/storage/emulated/0/Download/ethica");
        if (!direct.exists()) {
            direct.mkdirs();
        }
        return direct.getPath();
    }


}

