package bandung.orion.com.ethica.form.cart;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.Ekspedisi.PilihEkspedisiActivity;
import bandung.orion.com.ethica.form.alamat_kirim.InputAlamatKirimCartActivity;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.order_detail.OrderDetail;
import bandung.orion.com.ethica.form.product_list.ProductListAdapter;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static android.support.v4.content.ContextCompat.getDrawable;
import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.NAMA_EKSPEDISI_LAIN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ALAMAT_PENGIRIMAN;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_EKSPEDISI;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_ITEM;
import static bandung.orion.com.ethica.utility.JConst.TIPE_VIEW_TOTAL;

public class CartAdapter extends RecyclerView.Adapter {
    Context context;
    List<CartModel> itemDataModels;
    CartFragment fragment;
    private ProgressDialog Loading;

    public CartAdapter(Context context, CartFragment fragment) {
        this.context = context;
        itemDataModels = new ArrayList<>();
        this.fragment = fragment;
        this.Loading = new ProgressDialog(context);
    }

    public void addModels(List<CartModel> itemDataModels) {
        int pos = this.itemDataModels.size();
        //this.itemDataModels.clear();
        this.itemDataModels.addAll(itemDataModels);
        notifyItemRangeInserted(pos, itemDataModels.size());
    }

    public void deleteAllModels(){
        itemDataModels = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TIPE_VIEW_ITEM) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.cart_list_item, parent, false);
            return new ItemHolder(row);
        }else if(viewType == TIPE_VIEW_ALAMAT){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.cart_item_alamat, parent, false);
            return new AlamatHolder(row);
        }else if(viewType == TIPE_VIEW_ALAMAT_PENGIRIMAN){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.cart_item_alamat_pengirim, parent, false);
            return new AlamatKirimHolder(row);
        }else if(viewType == TIPE_VIEW_EKSPEDISI){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.cart_item_ekspedisi, parent, false);
            return new EkspedisiHolder(row);
        }else if(viewType == TIPE_VIEW_TOTAL){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.cart_item_detail_pembayaran, parent, false);
            return new RincianHargaHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CartAdapter.ItemHolder) {
            final CartModel mCurrentItem = itemDataModels.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;
            if (!mCurrentItem.getGambar().equals("")) {
                //            Picasso.with(context).load(mCurrentItem.getGambar()).resize(900, 300).into(itemHolder.imgGambar);
                Picasso.get().load(mCurrentItem.getGambar()).into(itemHolder.imgGambar);
                //Picasso.with(context).load(mCurrentItem.getGambar()).into(itemHolder.imgGambar);

                if (itemHolder.imgGambar.getDrawable() == null){
                    itemHolder.imgGambar.setImageResource(R.drawable.gambar_tidak_tersedia);
                }
            } else {
                itemHolder.imgGambar.setImageResource(R.drawable.gambar_tidak_tersedia);
            }
            //itemHolder.txtNama.setText(mCurrentItem.getKode()+" - "+mCurrentItem.getNama());
            itemHolder.txtNama.setText(mCurrentItem.getNama());
            itemHolder.txtHarga.setText("Rp." + FungsiGeneral.FloatToStrFmt(mCurrentItem.getHarga()));
            itemHolder.txtTotal.setText("Rp." + FungsiGeneral.FloatToStrFmt((mCurrentItem.getHarga() * mCurrentItem.getQty_order()) - (mCurrentItem.getHarga_diskon() * mCurrentItem.getQty_order())));
            itemHolder.txtJumlah.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getQty()));
            itemHolder.txtJumlahKeep.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getQty_order()));
            itemHolder.txtBerat.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getBerat_total())+" Kg");
            if (mCurrentItem.getIs_preorder().equals("T")){
                itemHolder.txtPO.setVisibility(View.VISIBLE);
            }else{
                itemHolder.txtPO.setVisibility(View.INVISIBLE);
            }

            if ((mCurrentItem.getAda_setting().equals("T")) &&  (mCurrentItem.getMax_diskon_setting() > 0)){
                itemHolder.txtDiskon.setVisibility(View.VISIBLE);
                itemHolder.txtDiskon.setText("Up to "+String.valueOf(Math.round(mCurrentItem.getMax_diskon_setting()))+ "%");
                itemHolder.txtDiskon.bringToFront();
            }else{
                itemHolder.txtDiskon.setVisibility(View.GONE);
            }

            itemHolder.txtHargaDiskon.setText("Rp." + FungsiGeneral.FloatToStrFmt(mCurrentItem.getHarga() - mCurrentItem.getHarga_diskon()));
            if (mCurrentItem.getHarga_diskon() > 0) {
                itemHolder.txtHargaDiskon.setVisibility(View.VISIBLE);
                itemHolder.txtHarga.setPaintFlags(itemHolder.txtHarga.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                itemHolder.txtHargaDiskon.setVisibility(View.INVISIBLE);
                if ((itemHolder.txtHarga.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0){
                    itemHolder.txtHarga.setPaintFlags( itemHolder.txtHarga.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
                }
            }

            //=========================================================================btnHapus=================================================================================================================================================
            itemHolder.btnHapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new android.support.v7.app.AlertDialog.Builder(context)
                            .setMessage("Yakin akan dihapus ?")
                            .setCancelable(true)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    String url = Routes.url_delete_cart + "/" + String.valueOf(mCurrentItem.getSeq()) +
                                                "?key=" + EthicaApplication.getInstance().ApiKey+
                                                "&versi="+EthicaApplication.getInstance().getVersionCode()+
                                                "&tipe_apps="+TIPE_APPS_ANDROID;
                                    StringRequest jArr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            // Parsing json
                                            try {
                                                JSONObject jObj = new JSONObject(response);
                                                if (jObj.getString("status").equals("status off")){
                                                    inform(context, "Untuk sementara tidak dapat menghapus", "");
                                                    return;
                                                }

                                                if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                                    inform(context, MSG_HARUS_UPDATE, "");
                                                    return;
                                                }
                                                if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)) {
                                                    int idx = 0;
                                                    for (int x = 0; x < CartAdapter.this.itemDataModels.size(); x++) {
                                                        if (CartAdapter.this.itemDataModels.get(x).getSeq() == mCurrentItem.getSeq()) {
                                                            CartAdapter.this.itemDataModels.remove(x);
                                                            idx = x;
                                                        }
                                                    }
                                                    fragment.delete_list_by_seq(mCurrentItem.getSeq());
                                                    fragment.getOngkir();
                                                    fragment.hitung();
                                                    CartAdapter.this.notifyItemRemoved(idx);
                                                    if (itemDataModels.size() == 2){
                                                        itemDataModels.remove(0);
                                                        itemDataModels.remove(0);
                                                        CartAdapter.this.notifyItemRangeRemoved(0,2);
                                                    }
                                                    ((Home)context).updateJumlahKeranjang();
                                                } else {
                                                    Toast.makeText(context, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (JSONException e) {
                                                // JSON error
                                                e.printStackTrace();
                                                Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            VolleyLog.d("error", "Error: " + error.getMessage());
                                        }
                                    });
                                    EthicaApplication.getInstance().addToRequestQueue(jArr);
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });
            //===============================================================================================================================================================================================================================

            //===================================================================btnMin======================================================================================================================================================
            itemHolder.btnMin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ((mCurrentItem.getQty() == 1) || (mCurrentItem.getQty() == 0)) {

                    } else {
                        itemHolder.btnMin.setEnabled(false);

                        final double qty = mCurrentItem.getQty() - 1;
                        double qtyorder;
                        if (mCurrentItem.getQty_order() > qty){
                            qtyorder = qty;
                        }else{
                            qtyorder = mCurrentItem.getQty_order();
                        }
                        final double qty_order = qtyorder;
                        String url;
                        url = Routes.url_update_qty_cart + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jObj = new JSONObject(response);

                                    if (jObj.getString("status").equals("status off")){
                                        inform(context, "Untuk sementara tidak dapat mengurangi", "");
                                        itemHolder.btnMin.setEnabled(true);
                                        return;
                                    }

                                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                        inform(context, MSG_HARUS_UPDATE, "");
                                        itemHolder.btnMin.setEnabled(true);
                                        return;
                                    }
                                    if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)) {
                                        CartAdapter.this.itemDataModels.get(position).setQty(qty);
//                                        fragment.hitung();//untuk
//                                        fragment.getOngkir();
//                                        fragment.hitung();

                                        mCurrentItem.setQty(qty);
                                        itemHolder.txtJumlah.setText(FungsiGeneral.FloatToStrFmt(qty));

                                        mCurrentItem.setQty_order(qty_order);
                                        itemHolder.txtJumlahKeep.setText(FungsiGeneral.FloatToStrFmt(qty_order));

                                        mCurrentItem.setBerat_total(qty_order * mCurrentItem.getBerat());
                                        itemHolder.txtBerat.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getBerat_total())+" Kg");

                                        itemHolder.txtTotal.setText(FungsiGeneral.FloatToStrFmt((mCurrentItem.getHarga() * mCurrentItem.getQty_order()) - (mCurrentItem.getHarga_diskon() * mCurrentItem.getQty_order()), true));


                                        mCurrentItem.setDiskon(jObj.getDouble("diskon") * mCurrentItem.getHarga() /100);
                                        mCurrentItem.setHarga_diskon(mCurrentItem.getDiskon());

                                        mCurrentItem.setAda_setting(jObj.getString("dari_setting"));
                                        mCurrentItem.setMax_diskon_setting(jObj.getDouble("max_diskon_setting"));

                                        if (mCurrentItem.getHarga_diskon() > 0) {
                                            itemHolder.txtHargaDiskon.setVisibility(View.VISIBLE);
                                            itemHolder.txtHarga.setPaintFlags(itemHolder.txtHarga.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                        } else {
                                            itemHolder.txtHargaDiskon.setVisibility(View.INVISIBLE);
                                            if ((itemHolder.txtHarga.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0){
                                                itemHolder.txtHarga.setPaintFlags( itemHolder.txtHarga.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
                                            }
                                        }

                                        if ((mCurrentItem.getAda_setting().equals("T")) &&  (mCurrentItem.getMax_diskon_setting() > 0)){
                                            itemHolder.txtDiskon.setVisibility(View.VISIBLE);
                                            itemHolder.txtDiskon.setText("Up to "+String.valueOf(Math.round(mCurrentItem.getMax_diskon_setting()))+ "%");
                                            itemHolder.txtDiskon.bringToFront();
                                        }else{
                                            itemHolder.txtDiskon.setVisibility(View.GONE);
                                        }

                                        notifyDataSetChanged();
                                        fragment.hitung();//untuk
                                        fragment.getOngkir();
                                        fragment.hitung();

                                    } else {
                                        Toast.makeText(context, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                                        itemHolder.btnMin.setEnabled(true);
                                    }
                                    itemHolder.btnMin.setEnabled(true);
                                } catch (JSONException e) {
                                    // JSON error
                                    e.printStackTrace();
                                    Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                                    itemHolder.btnMin.setEnabled(true);
                                }
                                itemHolder.btnMin.setEnabled(true);
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                                itemHolder.btnMin.setEnabled(true);
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {
                                // Posting parameters ke post url
                                Map<String, String> params = new HashMap<String, String>();

                                params.put("seq", String.valueOf(mCurrentItem.getSeq()));
                                params.put("qty", String.valueOf(qty));
                                params.put("qty_order", String.valueOf(qty_order));
                                params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                                params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                                params.put("barang_seq", String.valueOf(mCurrentItem.getBarang_seq()));
                                params.put("tipe", "K");
                                params.put("tipe_apps", TIPE_APPS_ANDROID);
                                return params;
                            }
                        };
                        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
                    }
                }
            });
            //=================================================================================================================================================================================================================================================

            //===================================================================btnPlus======================================================================================================================================================
            itemHolder.btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemHolder.btnPlus.setEnabled(false);
                    final double qty = mCurrentItem.getQty() + 1;
                    String url;
                    url = Routes.url_update_qty_cart + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                    StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jObj = new JSONObject(response);

                                if (jObj.getString("status").equals("status off")){
                                    inform(context, "Untuk sementara tidak dapat menambah", "");
                                    itemHolder.btnPlus.setEnabled(true);
                                    return;
                                }

                                if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                    inform(context, MSG_HARUS_UPDATE, "");
                                    itemHolder.btnPlus.setEnabled(true);
                                    return;
                                }else if (jObj.getString("status").equals("Plafon tidak mencukupi")){
                                    Loading.dismiss();
                                    Toast.makeText(context, "Plafon tidak mencukupi", Toast.LENGTH_SHORT).show();
                                    itemHolder.btnPlus.setEnabled(true);
                                    return;
                                }

                                if (jObj.getString("status").equals("stok kurang")) {
                                    Toast.makeText(context, "Stok tidak mencukupi", Toast.LENGTH_SHORT).show();
                                    itemHolder.btnPlus.setEnabled(true);
                                } else if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)) {
                                    CartAdapter.this.itemDataModels.get(position).setQty(qty);
                                    fragment.hitung();//untuk
                                    fragment.getOngkir();
                                    fragment.hitung();
                                    itemHolder.txtJumlah.setText(FungsiGeneral.FloatToStrFmt(qty));
                                    itemHolder.btnPlus.setEnabled(true);
                                } else {
                                    Toast.makeText(context, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                                    itemHolder.btnPlus.setEnabled(true);
                                }
                                itemHolder.btnPlus.setEnabled(true);
                            } catch (JSONException e) {
                                // JSON error
                                e.printStackTrace();
                                Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                                itemHolder.btnPlus.setEnabled(true);
                            }
                            itemHolder.btnPlus.setEnabled(true);
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                            itemHolder.btnPlus.setEnabled(true);
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            // Posting parameters ke post url
                            Map<String, String> params = new HashMap<String, String>();

                            params.put("seq", String.valueOf(mCurrentItem.getSeq()));
                            params.put("barang_seq", String.valueOf(mCurrentItem.getBarang_seq()));
                            params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                            params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                            params.put("qty", String.valueOf(qty));
                            params.put("tipe", "T");
                            params.put("is_preorder", mCurrentItem.getIs_preorder());
                            params.put("tipe_apps", TIPE_APPS_ANDROID);
                            return params;
                        }
                    };
                    EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
                }
            });



            //===================================================================btnMin======================================================================================================================================================
            itemHolder.btnMinKeep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ((mCurrentItem.getQty_order() == 0)) {

                    } else {
                        itemHolder.btnMinKeep.setEnabled(false);
                        final double qty = mCurrentItem.getQty_order() - 1;
                        String url;
                        url = Routes.url_update_qty_order_cart + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jObj = new JSONObject(response);
                                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                        inform(context, MSG_HARUS_UPDATE, "");
                                        itemHolder.btnMinKeep.setEnabled(true);
                                        return;
                                    }
                                    if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)) {
                                        CartAdapter.this.itemDataModels.get(position).setQty_order(qty);
                                        itemHolder.txtTotal.setText(FungsiGeneral.FloatToStrFmt((mCurrentItem.getHarga() * qty) - (mCurrentItem.getHarga_diskon() * qty), true));
                                        mCurrentItem.setBerat_total(qty * mCurrentItem.getBerat());
                                        itemHolder.txtBerat.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getBerat_total())+" Kg");
                                        itemHolder.txtJumlahKeep.setText(FungsiGeneral.FloatToStrFmt(qty));

                                        mCurrentItem.setDiskon(jObj.getDouble("diskon") * mCurrentItem.getHarga() /100);
                                        mCurrentItem.setHarga_diskon(mCurrentItem.getDiskon());

                                        mCurrentItem.setAda_setting(jObj.getString("dari_setting"));
                                        mCurrentItem.setMax_diskon_setting(jObj.getDouble("max_diskon_setting"));

                                        if (mCurrentItem.getHarga_diskon() > 0) {
                                            itemHolder.txtHargaDiskon.setVisibility(View.VISIBLE);
                                            itemHolder.txtHarga.setPaintFlags(itemHolder.txtHarga.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                        } else {
                                            itemHolder.txtHargaDiskon.setVisibility(View.INVISIBLE);
                                            if ((itemHolder.txtHarga.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0){
                                                itemHolder.txtHarga.setPaintFlags( itemHolder.txtHarga.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
                                            }
                                        }

                                        if ((mCurrentItem.getAda_setting().equals("T")) &&  (mCurrentItem.getMax_diskon_setting() > 0)){
                                            itemHolder.txtDiskon.setVisibility(View.VISIBLE);
                                            itemHolder.txtDiskon.setText("Up to "+String.valueOf(Math.round(mCurrentItem.getMax_diskon_setting()))+ "%");
                                            itemHolder.txtDiskon.bringToFront();
                                        }else{
                                            itemHolder.txtDiskon.setVisibility(View.GONE);
                                        }

                                        notifyDataSetChanged();
                                        fragment.hitung();//untuk
                                        fragment.getOngkir();
                                        fragment.hitung();
                                    } else {
                                        Toast.makeText(context, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                                        itemHolder.btnMinKeep.setEnabled(true);
                                    }
                                    itemHolder.btnMinKeep.setEnabled(true);
                                } catch (JSONException e) {
                                    // JSON error
                                    e.printStackTrace();
                                    Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                                    itemHolder.btnMinKeep.setEnabled(true);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                                itemHolder.btnMinKeep.setEnabled(true);
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {
                                // Posting parameters ke post url
                                Map<String, String> params = new HashMap<String, String>();

                                params.put("seq", String.valueOf(mCurrentItem.getSeq()));
                                params.put("barang_seq", String.valueOf(mCurrentItem.getBarang_seq()));
                                params.put("qty_order", String.valueOf(qty));
                                params.put("tipe", "K");
                                params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                                params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                                params.put("tipe_apps", TIPE_APPS_ANDROID);
                                return params;
                            }
                        };
                        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
                    }
                }
            });



            //===================================================================btnMin======================================================================================================================================================
            itemHolder.btnPlusKeep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ((mCurrentItem.getQty_order() == mCurrentItem.getQty())) {

                    } else {
                        itemHolder.btnPlusKeep.setEnabled(false);
                        final double qty = mCurrentItem.getQty_order() + 1;
                        String url;
                        url = Routes.url_update_qty_order_cart + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jObj = new JSONObject(response);
                                    if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                        inform(context, MSG_HARUS_UPDATE, "");
                                        itemHolder.btnPlusKeep.setEnabled(true);
                                        return;
                                    }

                                    if (!jObj.getString("status").equals(API_KEY_UNAUTORIZED)) {
                                        CartAdapter.this.itemDataModels.get(position).setQty_order(qty);
                                        itemHolder.txtTotal.setText(FungsiGeneral.FloatToStrFmt((mCurrentItem.getHarga() * qty) - (mCurrentItem.getHarga_diskon() * qty), true));
                                        itemHolder.txtJumlahKeep.setText(FungsiGeneral.FloatToStrFmt(qty));

                                        mCurrentItem.setBerat_total(qty * mCurrentItem.getBerat());
                                        itemHolder.txtBerat.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getBerat_total())+" Kg");

                                        mCurrentItem.setDiskon(jObj.getDouble("diskon") * mCurrentItem.getHarga() /100);
                                        mCurrentItem.setHarga_diskon(mCurrentItem.getDiskon());

                                        mCurrentItem.setAda_setting(jObj.getString("dari_setting"));
                                        mCurrentItem.setMax_diskon_setting(jObj.getDouble("max_diskon_setting"));

                                        if (mCurrentItem.getHarga_diskon() > 0) {
                                            itemHolder.txtHargaDiskon.setVisibility(View.VISIBLE);
                                            itemHolder.txtHarga.setPaintFlags(itemHolder.txtHarga.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                        } else {
                                            itemHolder.txtHargaDiskon.setVisibility(View.INVISIBLE);
                                            if ((itemHolder.txtHarga.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0){
                                                itemHolder.txtHarga.setPaintFlags( itemHolder.txtHarga.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
                                            }
                                        }

                                        if ((mCurrentItem.getAda_setting().equals("T")) &&  (mCurrentItem.getMax_diskon_setting() > 0)){
                                            itemHolder.txtDiskon.setVisibility(View.VISIBLE);
                                            itemHolder.txtDiskon.setText("Up to "+String.valueOf(Math.round(mCurrentItem.getMax_diskon_setting()))+ "%");
                                            itemHolder.txtDiskon.bringToFront();
                                        }else{
                                            itemHolder.txtDiskon.setVisibility(View.GONE);
                                        }

                                        notifyDataSetChanged();
                                        fragment.hitung();//untuk
                                        fragment.getOngkir();
                                        fragment.hitung();
                                    } else {
                                        Toast.makeText(context, MSG_API_KEY_UNAUTORIZED, Toast.LENGTH_SHORT).show();
                                        itemHolder.btnPlusKeep.setEnabled(true);
                                    }
                                    itemHolder.btnPlusKeep.setEnabled(true);
                                } catch (JSONException e) {
                                    // JSON error
                                    e.printStackTrace();
                                    Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                                    itemHolder.btnPlusKeep.setEnabled(true);
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                                itemHolder.btnPlusKeep.setEnabled(true);
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {
                                // Posting parameters ke post url
                                Map<String, String> params = new HashMap<String, String>();

                                params.put("seq", String.valueOf(mCurrentItem.getSeq()));
                                params.put("qty_order", String.valueOf(qty));
                                params.put("barang_seq", String.valueOf(mCurrentItem.getBarang_seq()));
                                params.put("tipe", "T");
                                params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                                params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                                params.put("tipe_apps", TIPE_APPS_ANDROID);
                                return params;
                            }
                        };
                        EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
                    }
                }
            });
            //=================================================================================================================================================================================================================================================

        }else if (holder instanceof AlamatHolder){
            final CartModel mCurrentItem = itemDataModels.get(position);
            final AlamatHolder alamatHolder = (AlamatHolder) holder;
            alamatHolder.txtAlamatKirim.setText(mCurrentItem.getAlamat_kirim());

            alamatHolder.txtUbah.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(fragment.getActivity(), InputAlamatKirimCartActivity.class);

                    in.putExtra("kecamatan", itemDataModels.get(position).getKecamatan());
                    in.putExtra("id_kecamatan", itemDataModels.get(position).getId_kecamatan());
                    in.putExtra("id_kota", itemDataModels.get(position).getId_kota());
                    in.putExtra("id_provinsi", itemDataModels.get(position).getId_provinsi());
                    in.putExtra("kelurahan", itemDataModels.get(position).getKelurahan());
                    in.putExtra("kota", itemDataModels.get(position).getKota());
                    in.putExtra("provinsi", itemDataModels.get(position).getProvinsi());
                    in.putExtra("alamat_kirim_lengkap", itemDataModels.get(position).getAlamat_kirim_lengkap());
                    in.putExtra("no_telepon_kirim", itemDataModels.get(position).getNo_telepon_kirim());
                    in.putExtra("nama_kirim", itemDataModels.get(position).getNama_kirim());

                    fragment.startActivityForResult(in, 1);
                }
                /*
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Masukan alamat kirim");
                    View view = View.inflate(context, R.layout.input_alamat_cart, null);
                    final EditText input = view.findViewById(R.id.txtInputAlamat);
                    input.setText(mCurrentItem.getAlamat_kirim());
                    builder.setView(view);

                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Loading.setMessage("Loading...");
                            Loading.setCancelable(false);
                            Loading.show();

                            if (input.getText().toString().trim().equals("")){
                                Toast.makeText(context, "Alamat kirim belum diisi", Toast.LENGTH_SHORT).show();
                                Loading.dismiss();
                            }else{
                                String url;
                                url = Routes.url_update_cart_master_alamat_kirim+ "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                                StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jObj = new JSONObject(response);
                                            if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                                Loading.dismiss();
                                                inform(context, MSG_HARUS_UPDATE, "");
                                                return;
                                            }
                                            alamatHolder.txtAlamatKirim.setText(input.getText().toString());
                                            mCurrentItem.setAlamat_kirim(input.getText().toString());
                                            itemDataModels.get(position).setAlamat_kirim(input.getText().toString());
                                            Loading.dismiss();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            Loading.dismiss();
                                            Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Loading.dismiss();
                                        Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_LONG).show();
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("tipe_customer", String.valueOf(EthicaApplication.getInstance().getTipeCust()));
                                        params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                                        params.put("alamat_kirim", input.getText().toString());
                                        return params;
                                    }
                                };
                                EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);

                            }
                        }
                    });
                    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }*/
            });
        }else if (holder instanceof AlamatKirimHolder){
//            final CartModel mCurrentItem = itemDataModels.get(position);
//            final AlamatKirimHolder alamatHolder = (AlamatKirimHolder) holder;
//            alamatHolder.txtAlamatPengirim.setText(mCurrentItem.getAlamat_pengirim());
//            alamatHolder.txtUbah.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                    builder.setTitle("Masukan alamat pengirim");
//                    View view = View.inflate(context, R.layout.input_alamat_cart, null);
//                    final EditText inputNama = view.findViewById(R.id.txtInputNamaPengirim);
//                    final EditText inputAlamat = view.findViewById(R.id.txtInputAlamat);
//                    final EditText inputNoHp = view.findViewById(R.id.txtInputNoTelponPengirim);
//                    inputNama.setText(mCurrentItem.getNama_pengirim());
//                    inputAlamat.setText(mCurrentItem.getAlamat_pengirim_lengkap());
//                    inputNoHp.setText(mCurrentItem.getNo_telepon_pengirim());
//                    builder.setView(view);
//
//                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Loading.setMessage("Loading...");
//                            Loading.setCancelable(false);
//                            Loading.show();
//
//                            if (inputNama.getText().toString().trim().equals("")){
//                                Toast.makeText(context, "Nama pengirim belum diisi", Toast.LENGTH_SHORT).show();
//                                Loading.dismiss();
//                                return;
//                            }
//
//                            if (inputAlamat.getText().toString().trim().equals("")){
//                                Toast.makeText(context, "Alamat lengkap belum diisi", Toast.LENGTH_SHORT).show();
//                                Loading.dismiss();
//                                return;
//                            }
//
//                            if (inputNoHp.getText().toString().trim().equals("")){
//                                Toast.makeText(context, "No. Hp belum diisi", Toast.LENGTH_SHORT).show();
//                                Loading.dismiss();
//                                return;
//                            }
//
//                            final String alamatPengirim = inputNama.getText().toString() +"\n"+
//                                    inputAlamat.getText().toString() +"\n"+
//                                    "No. HP : "+inputNoHp.getText().toString();
//
//                            String url;
//                            url = Routes.url_update_cart_master_alamat_pengirim + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
//                            StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//                                @Override
//                                public void onResponse(String response) {
//                                    try {
//                                        JSONObject jObj = new JSONObject(response);
//
//                                        if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
//                                            Loading.dismiss();
//                                            inform(context, MSG_HARUS_UPDATE, "");
//                                            return;
//                                        }
//
//                                        alamatHolder.txtAlamatPengirim.setText(alamatPengirim);
//                                        mCurrentItem.setAlamat_pengirim(alamatPengirim);
//                                        mCurrentItem.setNama_pengirim(inputNama.getText().toString());
//                                        mCurrentItem.setAlamat_pengirim_lengkap(inputAlamat.getText().toString());
//                                        mCurrentItem.setNo_telepon_pengirim(inputNoHp.getText().toString());
//
//                                        itemDataModels.get(position).setAlamat_pengirim(alamatPengirim);
//                                        itemDataModels.get(position).setNama_pengirim(inputNama.getText().toString());
//                                        itemDataModels.get(position).setAlamat_pengirim_lengkap(inputAlamat.getText().toString());
//                                        itemDataModels.get(position).setNo_telepon_pengirim(inputNoHp.getText().toString());
//
//                                        Loading.dismiss();
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                        Loading.dismiss();
//                                        Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
//                                    }
//                                }
//                            }, new Response.ErrorListener() {
//
//                                @Override
//                                public void onErrorResponse(VolleyError error) {
//                                    Loading.dismiss();
//                                    Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_LONG).show();
//                                }
//                            }) {
//                                @Override
//                                protected Map<String, String> getParams() {
//                                    Map<String, String> params = new HashMap<String, String>();
//                                    params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
//                                    params.put("alamat_pengirim", alamatPengirim);
//                                    params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
//                                    params.put("nama_pengirim", inputNama.getText().toString());
//                                    params.put("alamat_pengirim_lengkap", inputAlamat.getText().toString());
//                                    params.put("no_telepon_pengirim", inputNoHp.getText().toString());
//                                    return params;
//                                }
//                            };
//                            EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
//                        }
//                    });
//
//                    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
//                    builder.show();
//                }
//            });

            final CartModel mCurrentItem = itemDataModels.get(position);
            final AlamatKirimHolder alamatHolder = (AlamatKirimHolder) holder;
            alamatHolder.txtAlamatPengirim.setText(mCurrentItem.getAlamat_pengirim());
            alamatHolder.txtUbah.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent s = new Intent(fragment.getActivity(), InputAlamatCart.class);
                    s.putExtra("NAMA_PENGIRIM", mCurrentItem.getNama_pengirim());
                    s.putExtra("ALAMAT_PENGIRIM", mCurrentItem.getAlamat_pengirim_lengkap());
                    s.putExtra("NO_TELPON_PENGIRIM", mCurrentItem.getNo_telepon_pengirim());
                    fragment.startActivityForResult(s, 3);
                }
            });
        }else if (holder instanceof EkspedisiHolder){
            final CartModel mCurrentItem = itemDataModels.get(position);
            final EkspedisiHolder ekspedisiHolder = (EkspedisiHolder) holder;

            if (!mCurrentItem.getEkspedisi().equals("")) {
                ekspedisiHolder.txtNoResi.setText(mCurrentItem.getNoResi());

                if (!mCurrentItem.getEkspedisi().equals(NAMA_EKSPEDISI_LAIN)){
                    ekspedisiHolder.txtEkspedisi.setText((mCurrentItem.getEkspedisi() + " - " + mCurrentItem.getService()).toUpperCase() + "  " +
                    FungsiGeneral.FloatToStrFmt(mCurrentItem.getOngkos_kirim(), true));
                    ekspedisiHolder.lblNoResi.setVisibility(View.GONE);
                    ekspedisiHolder.txtNoResi.setVisibility(View.GONE);
                    ekspedisiHolder.txtNoResi.setText("");
                    mCurrentItem.setNoResi("");
                }else{
                    ekspedisiHolder.txtEkspedisi.setText(mCurrentItem.getEkspedisi().toUpperCase() + "  " +
                     FungsiGeneral.FloatToStrFmt(mCurrentItem.getOngkos_kirim(), true));
                    ekspedisiHolder.lblNoResi.setVisibility(View.VISIBLE);
                    ekspedisiHolder.txtNoResi.setVisibility(View.VISIBLE);
                }
            }else{
                ekspedisiHolder.txtEkspedisi.setText("Pilih Kurir");
                ekspedisiHolder.lblNoResi.setVisibility(View.GONE);
                ekspedisiHolder.txtNoResi.setVisibility(View.GONE);
                ekspedisiHolder.txtNoResi.setText("");
                mCurrentItem.setNoResi("");
            }

            ekspedisiHolder.crdView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mCurrentItem.getTotalBerat() == 0) {
                        Toast.makeText(context, "Total berat masih 0", Toast.LENGTH_SHORT).show();
                    }else{
                        Intent in = new Intent(fragment.getActivity(), PilihEkspedisiActivity.class);
                        fragment.startActivityForResult(in, 2);
                    }
                }
            });

            ekspedisiHolder.txtNoResi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Input");
                    View view = View.inflate(context, R.layout.activity_input_no_resi, null);
                    final EditText input = view.findViewById(R.id.txtNoResi);
                    input.setText(mCurrentItem.getNoResi());
                    builder.setView(view);

                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Loading.setMessage("Loading...");
                            Loading.setCancelable(false);
                            Loading.show();

                            String url;
                            url = Routes.url_update_cart_no_resi+ "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                            StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jObj = new JSONObject(response);
                                        if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                            Loading.dismiss();
                                            inform(context, MSG_HARUS_UPDATE, "");
                                            return;
                                        }

                                        ekspedisiHolder.txtNoResi.setText(input.getText().toString());
                                        mCurrentItem.setNoResi(input.getText().toString());
                                        itemDataModels.get(position).setNoResi(input.getText().toString());

                                        Loading.dismiss();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Loading.dismiss();
                                        Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Loading.dismiss();
                                    Toast.makeText(context, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_LONG).show();
                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<String, String>();params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                                    params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                                    params.put("tipe_apps", TIPE_APPS_ANDROID);
                                    params.put("no_resi", input.getText().toString());
                                    return params;
                                }
                            };
                            EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
                        }
                    });
                    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }
            });

//            ekspedisiHolder.txtNoResi.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    mCurrentItem.setNoResi(ekspedisiHolder.txtNoResi.getText().toString());
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//
//                }
//            });


        }else if (holder instanceof RincianHargaHolder){
            final CartModel mCurrentItem = itemDataModels.get(position);
            final RincianHargaHolder rincianHargaHolder = (RincianHargaHolder) holder;
            rincianHargaHolder.txtDiskon.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getDiskon()));
            rincianHargaHolder.txtOngkir.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getOngkos_kirim()));
            rincianHargaHolder.txtSubtotal.setText(FungsiGeneral.FloatToStrFmt(mCurrentItem.getSubTotal()));
            rincianHargaHolder.txtTotalBerat.setText(": "+FungsiGeneral.FloatToStrFmt(mCurrentItem.getTotalBerat()));
        }
//========================================================================================================================================================================================================================================
    }

    @Override
    public int getItemCount() {
        return itemDataModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemDataModels.get(position).getTipe_view();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtNama, txtHarga, txtJumlah, txtTotal, txtHargaDiskon, txtJumlahKeep, txtBerat, txtPO, txtDiskon;
        ImageView imgGambar;
        ImageButton btnPlus, btnMin, btnHapus, btnPlusKeep, btnMinKeep;

        public ItemHolder(View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtHarga = itemView.findViewById(R.id.txtHarga);
            txtJumlah = itemView.findViewById(R.id.txtJumlah);
            txtTotal = itemView.findViewById(R.id.txtTotalMst);
            txtJumlahKeep = itemView.findViewById(R.id.txtJumlahKeep);
            imgGambar = itemView.findViewById(R.id.imgGambar);
            btnPlus = itemView.findViewById(R.id.btnPlus);
            btnMin = itemView.findViewById(R.id.btnMin);
            btnHapus = itemView.findViewById(R.id.btnHapus);
            txtHargaDiskon = itemView.findViewById(R.id.txtHargaDiskon);

            btnPlusKeep = itemView.findViewById(R.id.btnPlusKeep);
            btnMinKeep = itemView.findViewById(R.id.btnMinKeep);
            txtBerat = itemView.findViewById(R.id.txtBerat);
            txtPO = itemView.findViewById(R.id.txtPO);
            txtDiskon = itemView.findViewById(R.id.txtDiskon);
        }
    }

    private class AlamatHolder extends RecyclerView.ViewHolder{
        TextView txtAlamatKirim, txtUbah;
        public AlamatHolder(View itemView) {
            super(itemView);
            txtAlamatKirim = itemView.findViewById(R.id.txtAlamatKirim);
            txtUbah = itemView.findViewById(R.id.txtUbah);
        }
    }

    private class AlamatKirimHolder extends RecyclerView.ViewHolder{
        TextView txtAlamatPengirim, txtUbah;
        public AlamatKirimHolder(View itemView) {
            super(itemView);
            txtAlamatPengirim = itemView.findViewById(R.id.txtAlamatPengirim);
            txtUbah = itemView.findViewById(R.id.txtUbah);
        }
    }


    private class EkspedisiHolder extends RecyclerView.ViewHolder{
        TextView txtEkspedisi, lblNoResi;
        EditText txtNoResi;
        CardView crdView;
        public EkspedisiHolder(View itemView) {
            super(itemView);
            txtEkspedisi = itemView.findViewById(R.id.txtEkspedisi);
            crdView = itemView.findViewById(R.id.crdView);
            lblNoResi = itemView.findViewById(R.id.lblNoResi);
            txtNoResi = itemView.findViewById(R.id.txtNoResi);
        }
    }

    private class RincianHargaHolder extends RecyclerView.ViewHolder{
        TextView txtSubtotal, txtDiskon, txtTotalBerat, txtOngkir;
        public RincianHargaHolder(View itemView) {
            super(itemView);
            txtSubtotal = itemView.findViewById(R.id.txtSubtotal);
            txtDiskon = itemView.findViewById(R.id.txtDiskon);
            txtTotalBerat = itemView.findViewById(R.id.txtTotalBerat);
            txtOngkir = itemView.findViewById(R.id.txtOngkir);
        }
    }

    
}
