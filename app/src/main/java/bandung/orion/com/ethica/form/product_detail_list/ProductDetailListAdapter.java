package bandung.orion.com.ethica.form.product_detail_list;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.ZoomImage.ZoomImageActivity;
import bandung.orion.com.ethica.form.home.Home;
import bandung.orion.com.ethica.form.product_list.ProductList;
import bandung.orion.com.ethica.form.product_list.ProductListModel;
import bandung.orion.com.ethica.form.profile.login.Login;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.EthicaApplication.fmt;
import static bandung.orion.com.ethica.utility.FungsiGeneral.Round;
import static bandung.orion.com.ethica.utility.FungsiGeneral.StrToIntDef;
import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.FungsiGeneral.tag_json_obj;
import static bandung.orion.com.ethica.utility.JConst.API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.JENIS_MENU_PROMO;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_API_KEY_UNAUTORIZED;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;
import static bandung.orion.com.ethica.utility.JEngine.GetInfoDiskon;

public class ProductDetailListAdapter extends RecyclerView.Adapter {
    Context context;
    List<ProductListModel> ProductListModels;
    private final int VIEW_TYVE_ITEM = 0, VIEW_TYVE_LOADING = 1;
    ProductDetailList Activity;
    private ProgressDialog Loading;
    private int ViewProductList;

    public ProductDetailListAdapter(Context context, List<ProductListModel> ProductListModels, ProductDetailList AppCompatActivity, int view) {
        this.context = context;
        this.ProductListModels = ProductListModels;
        this.Activity = AppCompatActivity;
        this.Loading = new ProgressDialog(context);
        this.ViewProductList = view;
    }

    public void addModels(List<ProductListModel> ProductListModels) {
        int pos = this.ProductListModels.size();
        this.ProductListModels.addAll(ProductListModels);
        notifyItemRangeInserted(pos, ProductListModels.size());
    }

    public void addMoel(ProductListModel productListModel) {
        this.ProductListModels.add(productListModel);
        notifyItemRangeInserted(ProductListModels.size()-1,ProductListModels.size()-1);
    }

    public void removeMoel(int idx) {
        if (ProductListModels.size() > 0){
            this.ProductListModels.remove(ProductListModels.size()-1);
            notifyItemRemoved(ProductListModels.size());
        }
    }

    public void removeAllModel(){
        int LastPosition = ProductListModels.size();
        this.ProductListModels.removeAll(ProductListModels);
        notifyItemRangeRemoved(0, LastPosition);
    }

    public void SetJenisView(int view){
        this.ViewProductList = view;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYVE_ITEM){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(ViewProductList, parent, false);
            return new ItemHolder(row);
        }else if(viewType == VIEW_TYVE_LOADING){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemHolder){

            final ProductListModel mCurrentItem = ProductListModels.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;

            if (ViewProductList == R.layout.list_item_product_list_grid){
                if (!mCurrentItem.getGambar_besar().equals("") ){
                    Picasso.get().load(mCurrentItem.getGambar_besar()).into(itemHolder.imgBarang);
                    if (itemHolder.imgBarang.getDrawable() == null){
                        itemHolder.imgBarang.setImageResource(R.drawable.gambar_tidak_tersedia);
                    }
                }else{
                    itemHolder.imgBarang.setImageResource(R.drawable.gambar_tidak_tersedia);
                }
            }else if(ViewProductList == R.layout.list_item_product_list){
                if (!mCurrentItem.getGambar().equals("") ){
                    Picasso.get().load(mCurrentItem.getGambar()).into(itemHolder.imgBarang);

                    if (itemHolder.imgBarang.getDrawable() == null ){
                        itemHolder.imgBarang.setImageResource(R.drawable.gambar_tidak_tersedia);
                    }
                }else{
                    itemHolder.imgBarang.setImageResource(R.drawable.gambar_tidak_tersedia);
                }
            }

            if (mCurrentItem.getDiskon_pct() == 0){
                itemHolder.imgInfo.setVisibility(View.GONE);
                itemHolder.txtDiskon.setVisibility(View.GONE);
            }else{
                itemHolder.imgInfo.setVisibility(View.VISIBLE);
                itemHolder.txtDiskon.setVisibility(View.VISIBLE);
                itemHolder.txtDiskon.setText("Up to "+String.valueOf(Math.round(mCurrentItem.getDiskon_pct()))+ "%");
                itemHolder.txtDiskon.bringToFront();
            }

            itemHolder.txtNama.setText(mCurrentItem.getNama());
            itemHolder.txtHarga.setText("Rp." + fmt.format(mCurrentItem.getHarga()));
            itemHolder.txtStok.setText("Stok "+  fmt.format(mCurrentItem.getStok()));
            if (mCurrentItem.getQty() == 0){
                itemHolder.txtQty.setText("");
            } else {
                itemHolder.txtQty.setText( String.valueOf(mCurrentItem.getQty()));
            }


            itemHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Loading.setMessage("Loading...");
                    Loading.setCancelable(false);
                    Loading.show();

                    boolean IsSudahLogin = EthicaApplication.getInstance().getCustomerSeqGlobal() > 0;

                    if (IsSudahLogin == true) {
                        final int QtyInput = StrToIntDef(itemHolder.txtQty.getText().toString(),0);
                        if (QtyInput == 0) {
                            Loading.dismiss();
                            Toast.makeText(context, "Qty belum diisi", Toast.LENGTH_SHORT).show();
                            itemHolder.txtQty.requestFocus();
                        }else {
                            String url;
                            url = Routes.url_save_cart+"?key="+EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                            StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject obj = new JSONObject(response);
                                        if (obj.getString("status").equals("status off")){
                                            Loading.dismiss();
                                            inform(context, "Untuk sementara tidak dapat memasukan barang ke cart", "");
                                            return;
                                        }

                                        if (obj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                            Loading.dismiss();
                                            inform(context, MSG_HARUS_UPDATE, "");
                                        }else if  (obj.getString("status").equals("stok kurang")) {

                                            double Stok = obj.getDouble("stok");
                                            itemHolder.txtStok.setText("Stok "+  fmt.format(Stok));
                                            ProductListModels.get(position).setStok(Stok);
                                            notifyItemChanged(position);
                                            Loading.dismiss();
                                            Toast.makeText(context, "Stok tidak mencukupi", Toast.LENGTH_SHORT).show();
                                            itemHolder.txtQty.requestFocus();

                                        }else if (obj.getString("status").equals("Plafon tidak mencukupi")){

                                            Loading.dismiss();
                                            Toast.makeText(context, "Plafon tidak mencukupi", Toast.LENGTH_SHORT).show();

                                        }else if (!obj.getString("status").equals(API_KEY_UNAUTORIZED)){

                                            double Stok = obj.getDouble("stok");
                                            itemHolder.txtQty.requestFocus();
                                            itemHolder.txtStok.setText("Stok "+  fmt.format(Stok));
                                            ProductListModels.get(position).setStok(Stok);
                                            notifyItemChanged(position);

                                            EthicaApplication.getInstance().getHomeGlobal().updateJumlahKeranjang();
                                            EthicaApplication.getInstance().IsReload = true;
                                            Activity.updateJumlahKeranjang();
                                            Loading.dismiss();

                                        }else{
                                            AlertDialog dialog = new AlertDialog.Builder(context).create();
                                            dialog.setMessage(MSG_API_KEY_UNAUTORIZED);
                                            dialog.setCancelable(true);
                                            dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int buttonId) {
                                                    Intent s = new Intent(context, Login.class);
                                                    context.startActivity(s);
                                                }
                                            });
                                            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int buttonId) {

                                                }
                                            });
                                            Loading.dismiss();
                                            dialog.show();
                                        }
                                        Loading.dismiss();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Loading.dismiss();
                                        Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Loading.dismiss();
                                    Toast.makeText(context, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                                    params.put("barang_seq", String.valueOf(mCurrentItem.getSeq()));
                                    params.put("qty", String.valueOf(itemHolder.txtQty.getText()));
                                    //params.put("tipe_customer", String.valueOf(EthicaApplication.getInstance().getTipeCust()));
                                    params.put("user_id", String.valueOf(EthicaApplication.getInstance().getUserIdGlobal()));
                                    params.put("is_preorder", "F");
                                    params.put("tipe_apps", TIPE_APPS_ANDROID);
                                    return params;
                                }
                            };
                            EthicaApplication.getInstance().addToRequestQueue(strReq, tag_json_obj);
                        }
                    }else {
                        AlertDialog dialog = new AlertDialog.Builder(context).create();
                        dialog.setMessage("Untuk melanjutkan harus login terlebih dahulu");
                        dialog.setCancelable(true);
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int buttonId) {
                                Intent s = new Intent(context, Login.class);
                                context.startActivity(s);

                            }
                        });
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int buttonId) {

                            }
                        });
                        Loading.dismiss();
                        dialog.show();
                    }
                }
            });

            itemHolder.imgBarang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent s = new Intent(context, ZoomImageActivity.class);
                    s.putExtra("GAMBAR", mCurrentItem.getGambar_besar());
                    s.putExtra("NAMA", mCurrentItem.getNama());
                    s.putExtra("KETERANGAN", mCurrentItem.getKeterangan());
                    s.putExtra("SEQ", mCurrentItem.getSeq());
                    context.startActivity(s);
                }
            });

            itemHolder.imgInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GetInfoDiskon(context, mCurrentItem.getSeq());
                }
            });

        }else if (holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return ProductListModels.get(position) == null ? VIEW_TYVE_LOADING : VIEW_TYVE_ITEM;
    }

    @Override
    public int getItemCount() {
        return ProductListModels.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtNama, txtHarga, txtStok, txtDiskon;
        EditText txtQty;
        ImageView imgBarang, imgInfo;
        ImageButton btnAdd;


        public ItemHolder(View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtHarga = itemView.findViewById(R.id.txtHarga);
            txtStok = itemView.findViewById(R.id.txtStok);
            txtQty = itemView.findViewById(R.id.txtQty);
            imgBarang = itemView.findViewById(R.id.ImgBarang);
            btnAdd = itemView.findViewById(R.id.btnAdd);
            imgInfo = itemView.findViewById(R.id.imgInfo);
            txtDiskon = itemView.findViewById(R.id.txtDiskon);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder{
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pg_loading);
        }
    }

}
