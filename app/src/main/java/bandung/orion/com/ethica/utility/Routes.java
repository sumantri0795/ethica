package bandung.orion.com.ethica.utility;

import bandung.orion.com.ethica.EthicaApplication;

public class Routes {

    //public static String URL = "http://api.ethica.id/public/";//CLOUD DOMOSQUARE
    //public static String URL = "http://api.dev.ethica.id/public/";//CLOUD DOMOSQUARE get_keagenanCOBA - COBA
    //public static String URL = "http://35.184.244.190/ethica_api/public/";//CLOUD ORION

    public static final String URL = "http://10.100.100.124/ethica_api/public/";//
    //public static final String URL = "http://192.168.0.20/ethica_api/public/";// jika kirim buka yang ini
    //public static final String URL = "http://10.100.100.37/ethica_api/public/";// JULI ORION_BANDUNG
    //public static final String URL = "http://192.168.0.106/ethica_api/public/";// jika kirim buka yang ini
    //public static String URL = "http://192.168.43.136/ethica_api/public/";// jika kirim buka yang ini
    //public static final String URL = "http://192.168.137.249/ethica_api/public/";// jika kirim buka yang ini
//    public static final String URL = "http://10.100.100.55/ethica_api/public/";// jika kirim buka yang inix

    //KERANJANG MASTER*
    public static String url_cart_master                        = "keranjang_master/";
    public static String url_save_cart_master                   = URL + url_cart_master + "save";
    public static String url_update_cart_master_alamat_kirim    = URL + url_cart_master + "update_alamat_kirim";
    public static String url_update_cart_master_alamat_pengirim = URL + url_cart_master + "update_alamat_pengirim";
    public static String url_get_cart_master                    = URL + url_cart_master + "get";
    public static String url_update_cart_master_ekspedisi_ongkir    = URL + url_cart_master + "update_ekspedisi_ongkir";
    public static String url_update_cart_no_resi                = URL + url_cart_master + "update_no_resi";

    //KERANJANG*
    public static String url_cart                  = "keranjang/";
    public static String url_save_cart             = URL + url_cart + "save";
    public static String url_delete_cart           = URL + url_cart + "delete";
    public static String url_update_qty_cart       = URL + url_cart + "update_qty";
    public static String url_load_cart             = URL + url_cart + "load";
    public static String url_get_count_cart        = URL + url_cart + "getcount";
    public static String url_update_qty_order_cart = URL + url_cart + "update_qty_order";
    public static String url_load_cart_sub_agen    = URL + url_cart + "load_cart_sub_agen";
    public static String url_get_nilai_diskon      = URL + url_cart + "get_nilai_diskon";

    //PESANAN MASTER*
    public static String url_pesanan_master              = "pesanan_master/";
    public static String url_save_pesanan_master         = URL + url_pesanan_master + "save";
    public static String url_load_pesanan_master         = URL + url_pesanan_master + "load";
    public static String url_get_pesanan_master          = URL + url_pesanan_master + "get";
    public static String url_get_pesanan_master2         = URL + url_pesanan_master + "get_master";
    public static String url_get_pesanan_histori_alamat  = URL + url_pesanan_master + "load_histori_alamat_kirim";
    public static String url_keep_pesanan_master         = URL + url_pesanan_master + "keep_pesanan";
    public static String url_load_pesanan_sub_agen       = URL + url_pesanan_master + "load_pesanan_sub_agen";
    public static String url_delete_keep_pesanan_master  = URL + url_pesanan_master + "delete_keep_pesanan";
    public static String url_commit_keep_pesanan_master  = URL + url_pesanan_master + "commit_keep_pesanan";
    public static String url_approve_pesanan_master      = URL + url_pesanan_master + "approve";
    public static String url_get_count_pesanan_sub_agen  = URL + url_pesanan_master + "get_count_pesanan_sub_agen";



    //MASTER BARANG*
    public static String url_barang                = "master_barang/";
    public static String url_load_barang           = URL + url_barang + "loaddata";
    public static String url_load_brand_barang     = URL + url_barang + "load_brand";
    public static String url_load_sub_brand_barang = URL + url_barang + "load_sub_brand";
    public static String url_load_warna_barang     = URL + url_barang + "load_warna";
    public static String url_load_diskon_barang    = URL + url_barang + "get_diskon";
    public static String url_get_barang            = URL + url_barang + "get";

    //MASTER CUSTOMER*
    public static String url_master_customer                 = "master_customer/";
    public static String url_login_master_customer           = URL + url_master_customer + "login";
    public static String url_change_password_master_customer = URL + url_master_customer + "change_password";
    public static String url_get_master_customer             = URL + url_master_customer + "get";
    public static String url_load_sub_agen_customer          = URL + url_master_customer + "load_sub_agen";
    public static String url_get_sub_agen_customer           = URL + url_master_customer + "get_sub_agen";
    public static String url_get_keagenan                    = URL + url_master_customer + "get_keagenan";


    public static String url_API      = "api/";
    public static String url_cek_API  = URL + url_API + "cek";

    //CAROUSEL*
    public static String url_images_carousel      = "images_carousel/";
    public static String url_load_images_carousel = URL + url_images_carousel + "load";


    //SARIMBIT*
    public static String url_sarimbit      = "setting_sarimbit_master/";
    public static String url_load_sarimbit = URL + url_sarimbit + "load";


    //CHAT*
    public static String url_chat_box             = "chat_box/";
    public static String url_save_chat_box        = URL + url_chat_box + "save";
    public static String url_update_baca_chat_box = URL + url_chat_box + "update_baca";
    public static String url_load_chat_box        = URL + url_chat_box + "load";
    public static String url_get_chat_box         = URL + url_chat_box + "getcount";

    //ekspedisi*
    public static String url_ekspedisi             = "ekspedisi/";
    public static String url_save_ekspedisi        = URL + url_ekspedisi + "save";
    public static String url_load_ekspedisi        = URL + url_ekspedisi + "load";


    //Size Pack *
    public static String url_size_pack            = "size_pack/";
    public static String url_get_size_pack        = URL + url_size_pack + "get";

    //Versi
    public static String url_versi_app            = "versi_app/";
    public static String url_get_versi_app        = URL + url_versi_app + "get";


//    https://api.rajaongkir.com/starter/city?key=2449517f0589e417296c728097c72a71
    //++++++++++++++++++++++++ API RAJA ONGKIR ++++++++++++++++++++++++\\
    public static String CERTIFICATE_ETHCA       = "6eb94591021564d99beb71b31aba88aa530f04b0";

    public static String API_KEY_RAJA_ONGKIR     = "3afce7052c726488d68120396c032b34";
    public static String ANDROID_KEY_RAJA_ONGKIR = "6eb94591021564d99beb71b31aba88aa530f04b0;bandung.orion.com.ethica";
    public static String url_raja_ongkir  = "https://pro.rajaongkir.com/api/";

    public static String url_get_provinsi  = url_raja_ongkir + "province?key="+ API_KEY_RAJA_ONGKIR + "&android-key="+ANDROID_KEY_RAJA_ONGKIR;
    public static String url_get_kota      = url_raja_ongkir + "city?key="+ API_KEY_RAJA_ONGKIR + "&android-key="+ANDROID_KEY_RAJA_ONGKIR;
    public static String url_get_kecamatan = url_raja_ongkir + "subdistrict?key="+ API_KEY_RAJA_ONGKIR + "&android-key="+ANDROID_KEY_RAJA_ONGKIR;
    public static String url_get_harga     = url_raja_ongkir + "cost?key="+ API_KEY_RAJA_ONGKIR + "&android-key="+ANDROID_KEY_RAJA_ONGKIR;
}



    //CREATE TABLE `ethica_coba`.`images_size_pack` ( `seq` INT NOT NULL AUTO_INCREMENT , `path` VARCHAR(255) NOT NULL , PRIMARY KEY (`seq`)) ENGINE = InnoDB;