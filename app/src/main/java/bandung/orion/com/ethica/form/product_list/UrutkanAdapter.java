package bandung.orion.com.ethica.form.product_list;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import bandung.orion.com.ethica.R;

import static bandung.orion.com.ethica.EthicaApplication.fmt;

public class UrutkanAdapter extends RecyclerView.Adapter {
    Context context;
    List<UrutkanModel> items;
    ProductList fragment;

    private final String TEXT_URUTKAN_NAMA     = "Nama";
    private final String TEXT_URUTKAN_TERBARU  = "Terbaru";
    private final String TEXT_URUTKAN_TERMURAH = "Termurah";
    private final String TEXT_URUTKAN_TERMAHAL = "Termahal";
    private final String TEXT_URUTKAN_TERBANYAK = "Terbanyak";


    public UrutkanAdapter(Context context, List<UrutkanModel> item, ProductList fragment) {
        this.context = context;
        this.fragment = fragment;
        this.items = item;
    }

    public void addModels(List<UrutkanModel> items) {
        int pos = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(pos, items.size());
    }

    
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.list_item_urutkan, parent, false);
        return new UrutkanAdapter.ItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final UrutkanModel item = items.get(position);
        final ItemHolder itemHolder = (ItemHolder) holder;

        itemHolder.txtUrutkan.setText(item.getNama());
        if (item.isPilih() == true){
            itemHolder.imgCeklis.setVisibility(View.VISIBLE);
        }else{
            itemHolder.imgCeklis.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragment.ListItems.clear();
//                fragment.mAdapter.notifyDataSetChanged();
                fragment.mAdapter.removeAllModel();

                for (int i= 0; i < items.size(); i++){
                    if (i != position){
                        items.get(i).setPilih(false);
                    }else{
                        items.get(i).setPilih(true);
                    }
                }
                item.setPilih(true);

                if (item.isPilih() == true){
                    itemHolder.imgCeklis.setVisibility(View.VISIBLE);
                }else{
                    itemHolder.imgCeklis.setVisibility(View.INVISIBLE);
                }
                notifyDataSetChanged();
                fragment.OrderBy = SetFilter(item.getNama());
                fragment.DialogFilter.dismiss();
                if (!itemHolder.txtUrutkan.getText().equals("SEMUA")){
                    fragment.SetTextUrutkan(item.getNama());
                    fragment.SetTextStyle(Typeface.BOLD);
                }else{
                    fragment.SetTextUrutkan("Urutkan");
                    fragment.SetTextStyle(Typeface.NORMAL);
                }
                fragment.RcvProductList.smoothScrollToPosition(1);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtUrutkan;
        ImageView imgCeklis;

        public ItemHolder(View itemView) {
            super(itemView);
            txtUrutkan = itemView.findViewById(R.id.txtUrutkan);
            imgCeklis = itemView.findViewById(R.id.imgCeklis);
        }
    }

    private String SetFilter(String text_urutkan){
        String hasil = "";
        if (text_urutkan.equals(TEXT_URUTKAN_NAMA)){
            hasil =  "nama";
//        }else if(text_urutkan.equals(TEXT_URUTKAN_TERBARU)){
//            hasil =  "seq%20DESC";
        }else if(text_urutkan.equals(TEXT_URUTKAN_TERMURAH)){
            hasil =  "harga%20ASC";
        }else if(text_urutkan.equals(TEXT_URUTKAN_TERMAHAL)){
            hasil =  "harga%20DESC";
        }else if(text_urutkan.equals(TEXT_URUTKAN_TERBANYAK)){
            hasil =  "stok%20DESC";
        }
        return hasil;
    }
}
