package bandung.orion.com.ethica.form.filter_customer;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.utility.ILoadMore;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;

public class FilterCustomer extends AppCompatActivity {
    public RecyclerView RcvData;
    public FilterCustomerAdapter mAdapter;
    private SwipeRefreshLayout swipe;
    private SearchView txtSearch;

    int visibleThreshold = 6;
    int lastVisibleItem, totalItemCount;
    boolean IsLoading;
    ILoadMore loadMore;

    private LinearLayoutManager linearLayoutManager;
    public List<FilterCustomerModel> ListItems = new ArrayList<>();

    private int NoOfColumns;
    private int CountAllVew;
    final List<View> pageList = new ArrayList<>();
    String mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_customer);
        CreateView();
        InitClass();
        EventClass();
        LoadData();
    }

    private void CreateView(){
        this.RcvData = (RecyclerView) findViewById(R.id.RcvData);
        this.swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        this.txtSearch = (SearchView) findViewById(R.id.txtSearch);
    }

    private void InitClass(){
        Bundle extra   = this.getIntent().getExtras();
        this.mode     = extra.getString("MODE");

        NoOfColumns = 1;
        SetJenisTampilan();
        Toolbar toolbar = (Toolbar) findViewById(R.id.ToolbarAct);
        this.setSupportActionBar(toolbar);
        CountAllVew = 0;
    }

    protected void EventClass(){
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!IsLoading){
                    mAdapter.removeAllModel();
                    RefreshRecyclerView();
                }
                swipe.setRefreshing(false);
            }
        });

        setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                mAdapter.addMoel(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.removeMoel(ListItems.size()-1);
                        LoadData();
                    }
                },1000);
            }
        });

        RcvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RefreshRecyclerView();
            }
        });

        txtSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        txtSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mAdapter.removeAllModel();
                RefreshRecyclerView();
                return false;
            }
        });
    }

    public void LoadData(){
        String Filter = "";
        Filter += "?customer_seq="+String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal())+
                  "&search=" + txtSearch.getQuery() + "&offset=" + Integer.toString(mAdapter.getItemCount());
        String url = Routes.url_load_sub_agen_customer + Filter;
        url = url.replace(" ", "%20");
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<FilterCustomerModel> itemDataModels = new ArrayList<>();
                FilterCustomerModel Data;
                itemDataModels.clear();

                //int seq, String nama, String no_telpon, String alamat
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        if (obj.getInt("seq") > 0){
                            Data = new FilterCustomerModel(
                                    obj.getInt("seq"),
                                    obj.getString("nama"),
                                    obj.getString("no_telp"),
                                    ""
                            );

                            String Alamat = "";
                            if (!obj.getString("alamat").equals("")) {Alamat = Alamat +obj.getString("alamat");}
                            if (!obj.getString("kecamatan").equals("")) {Alamat = Alamat +", "+obj.getString("kecamatan");}
                            if (!obj.getString("kota").equals("")) {Alamat = Alamat +", "+obj.getString("kota");}
                            if (!obj.getString("provinsi").equals("")) {Alamat = Alamat +", "+obj.getString("provinsi");}

                            Data.setAlamat(Alamat);
                            itemDataModels.add(Data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        swipe.setRefreshing(false);
                    }
                }
                mAdapter.addModels(itemDataModels);
                setLoaded(false);
                swipe.setRefreshing(false);
                if (ListItems.size() <= 0){Toast.makeText(getApplicationContext(), "Tidak ditemukan", Toast.LENGTH_SHORT).show();}
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                swipe.setRefreshing(false);
                setLoaded(false);
                Toast.makeText(getApplicationContext(), MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }

    public void setLoaded(boolean loading) {
        IsLoading = loading;
    }


    public void RefreshRecyclerView(){
        totalItemCount  = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!IsLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
            if (loadMore != null){
                loadMore.onLoadMore();
                setLoaded(true);
            }
        }
    }

    private void SetJenisTampilan(){
        mAdapter = new FilterCustomerAdapter(FilterCustomer.this, ListItems,  R.layout.filter_customer_list_item,mode, FilterCustomer.this);
        RcvData.setLayoutManager(new GridLayoutManager(FilterCustomer.this, NoOfColumns, GridLayoutManager.VERTICAL, false));
        linearLayoutManager = (LinearLayoutManager)RcvData.getLayoutManager();
        RcvData.setAdapter(mAdapter);
    }

    public void SetResult(long seqCustomer, String sub_agen){
        Intent intent = getIntent();
        intent.putExtra("CUSTOMER_SEQ", seqCustomer);
        intent.putExtra("SUB_AGEN", sub_agen);
        setResult(RESULT_OK, intent);
        finish();
    }
}
