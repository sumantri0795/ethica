package bandung.orion.com.ethica.form.Ekspedisi;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import bandung.orion.com.ethica.R;

import static bandung.orion.com.ethica.utility.JConst.NAMA_EKSPEDISI_LAIN;

public class PilihEkspedisiChildExpAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<PilihEkspedisiModel> nameList;
    private Intent intent;
    private Activity mActivity;

    public PilihEkspedisiChildExpAdapter(List<PilihEkspedisiModel> nameList, Intent intent, Activity mActivity) {
        this.intent = intent;
        this.mActivity = mActivity;
        this.nameList = nameList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pilih_ekspedisi_child_list_item, parent, false);
        PilihEkspedisiChildExpAdapter.ItemHolder vh = new PilihEkspedisiChildExpAdapter.ItemHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PilihEkspedisiChildExpAdapter.ItemHolder){
            final PilihEkspedisiModel mCurrentItem = nameList.get(position);
            final PilihEkspedisiChildExpAdapter.ItemHolder itemHolder = (PilihEkspedisiChildExpAdapter.ItemHolder) holder;

            if (!mCurrentItem.getKode().equals(NAMA_EKSPEDISI_LAIN)){
                itemHolder.txtEkspedisi.setText((mCurrentItem.getKode() + " - " + mCurrentItem.getService()).toUpperCase());
            }else{
                itemHolder.txtEkspedisi.setText((mCurrentItem.getKode()));
            }

            itemHolder.crdView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent.putExtra("ekspedisi", mCurrentItem.getKode());
                    intent.putExtra("service", mCurrentItem.getService());
                    intent.putExtra("ekspedisi_seq", mCurrentItem.getSeq());
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    mActivity.finish();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtEkspedisi;
        CardView crdView;

        public ItemHolder(View itemView) {
            super(itemView);
            txtEkspedisi    = itemView.findViewById(R.id.txtEkspedisi);
            crdView = itemView.findViewById(R.id.crdView);
        }
    }


}
