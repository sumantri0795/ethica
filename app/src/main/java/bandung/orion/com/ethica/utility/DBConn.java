package bandung.orion.com.ethica.utility;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConn extends SQLiteOpenHelper {
    public DBConn(Context context) {
        super(context, "Ethica.db", null, 2);
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS login(customer_seq, nama varchar(50), api_key varchar(32), tipe varchar(1))");
        this.onUpgrade(sqLiteDatabase, 0, 1);
    }


    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql ="";

        sql = "ALTER TABLE login ADD user_id VARCHAR(100)";
        try {
            sqLiteDatabase.execSQL(sql);
        } catch (SQLException e) {

        }

        sql = "ALTER TABLE login ADD is_ethica VARCHAR(1)";
        try {
            sqLiteDatabase.execSQL(sql);
        } catch (SQLException e) {

        }

        sql = "ALTER TABLE login ADD is_seply VARCHAR(1)";
        try {
            sqLiteDatabase.execSQL(sql);
        } catch (SQLException e) {

        }

        sql = "ALTER TABLE login ADD is_ethica_hijab VARCHAR(1)";
        try {
            sqLiteDatabase.execSQL(sql);
        } catch (SQLException e) {

        }

        sql = "ALTER TABLE login ADD jenis VARCHAR(10)";
        try {
            sqLiteDatabase.execSQL(sql);
        } catch (SQLException e) {

        }
    }
}
