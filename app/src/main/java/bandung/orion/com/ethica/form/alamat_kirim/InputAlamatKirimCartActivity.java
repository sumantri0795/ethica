package bandung.orion.com.ethica.form.alamat_kirim;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.cart.HistoriAlamatKirim;
import bandung.orion.com.ethica.form.cart.InputAlamatCart;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;

public class InputAlamatKirimCartActivity extends AppCompatActivity {
    private Spinner spnProvinsi, spnKota, spnKecamatan;
    private Button btnTerapkan, btnBatal;
    private EditText txtInputNamaPengirim, txtInputAlamat, txtKelurahan, txtInputNoTelponPengirim;
    private ConstraintLayout cLPilihAlamat;

    private List<String> ListProvinsi;
    private HashMap<Integer, Integer> HashProvinsi;
    private ArrayAdapter<String> AdapterProvinsi;

    private List<String> ListKota;
    private HashMap<Integer, Integer> HashKota;
    private ArrayAdapter<String> AdapterKota;

    private List<String> ListKecamatan;
    private HashMap<Integer, Integer> HashKecamatan;
    private ArrayAdapter<String> AdapterKecamatan;

    private ProgressDialog Loading;
    private Integer idProvinsi, idKota, idKecamatan;
    private String provinsi, kota, kecamatan;
    private String tempProvinsi, tempKota, tempKecamatan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_alamat_kirim_cart);
        CreateVew();
        InitClass();
        EventClass();
    }

    private void CreateVew(){
        this.spnProvinsi = (Spinner) findViewById(R.id.spnProvinsi);
        this.spnKota = (Spinner) findViewById(R.id.spnKota);
        this.spnKecamatan = (Spinner) findViewById(R.id.spnKecamatan);
        this.btnTerapkan = (Button) findViewById(R.id.btnTerapkan);
        this.btnBatal = (Button) findViewById(R.id.btnBatal);
        this.txtInputAlamat = (EditText) findViewById(R.id.txtInputAlamat);
        this.txtInputNamaPengirim = (EditText) findViewById(R.id.txtInputNamaPengirim);
        this.txtKelurahan = (EditText) findViewById(R.id.txtKelurahan);
        this.txtInputNoTelponPengirim = (EditText) findViewById(R.id.txtInputNoTelponPengirim);
        this.cLPilihAlamat = (ConstraintLayout) findViewById(R.id.cLPilihAlamat);

        ListProvinsi = new ArrayList<>();
        HashProvinsi = new HashMap<Integer, Integer>();
        AdapterProvinsi = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListProvinsi);
        ListKota = new ArrayList<>();
        HashKota = new HashMap<Integer, Integer>();
        AdapterKota = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListKota);
        ListKecamatan = new ArrayList<>();
        HashKecamatan = new HashMap<Integer, Integer>();
        AdapterKecamatan = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ListKecamatan);
    }

    private void InitClass(){
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        setTitle("Alamat penerima");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        idProvinsi = 0;
        idKota = 0;
        idKecamatan = 0;

        AdapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProvinsi.setAdapter(AdapterProvinsi);

        AdapterKota.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnKota.setAdapter(AdapterKota);

        AdapterKecamatan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnKecamatan.setAdapter(AdapterKecamatan);

        this.Loading = new ProgressDialog(InputAlamatKirimCartActivity.this);

        Bundle extra   = this.getIntent().getExtras();
        this.kecamatan     = extra.getString("kecamatan");
        this.kota     = extra.getString("kota");
        this.provinsi     = extra.getString("provinsi");
        this.idKecamatan = extra.getInt("id_kecamatan");
        this.idKota     = extra.getInt("id_kota");
        this.idProvinsi     = extra.getInt("id_provinsi");
        this.tempProvinsi = provinsi;
        this.tempKota = kota;
        this.tempKecamatan = kecamatan;
        this.txtKelurahan.setText(extra.getString("kelurahan"));
        this.txtInputAlamat.setText(extra.getString("alamat_kirim_lengkap"));
        this.txtInputNamaPengirim.setText(extra.getString("nama_kirim"));
        this.txtInputNoTelponPengirim.setText(extra.getString("no_telepon_kirim"));
        SetProvinsi();
    }

    public void SetProvinsi(){
        String url = Routes.url_get_provinsi;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ListProvinsi.clear();
                        HashProvinsi.clear();

                        ListProvinsi.add("Pilih");
                        HashProvinsi.put(0, 0);
                        spnProvinsi.setSelection(0);
                        try {
                            JSONArray ArrResults = new JSONObject(response).getJSONObject("rajaongkir").getJSONArray("results");
                            for (int i = 0; i < ArrResults.length(); i++) {
                                try {
                                    JSONObject obj = ArrResults.getJSONObject(i);
                                    ListProvinsi.add(obj.getString("province"));
                                    HashProvinsi.put(i+1, obj.getInt("province_id"));
                                    AdapterProvinsi.notifyDataSetChanged();
                                    if ((tempProvinsi != null) && (tempProvinsi.equals(obj.getString("province")))){
                                        spnProvinsi.setSelection(i+1);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        EthicaApplication.getInstance().addToRequestQueue(stringRequest);
    }



    public void SetKota(int id_provinsi){
        if (id_provinsi > 0 ){

            Loading.setMessage("Loading...");
            Loading.setCancelable(false);
            Loading.show();

            String url = Routes.url_get_kota+"&province="+id_provinsi;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    ListKota.clear();
                    HashKota.clear();
                    ListKota.add("Pilih");
                    HashKota.put(0, 0);
                    spnKota.setSelection(0);
                    try {
                        JSONArray ArrResults = new JSONObject(response).getJSONObject("rajaongkir").getJSONArray("results");
                        for (int i = 0; i < ArrResults.length(); i++) {
                            try {
                                JSONObject obj = ArrResults.getJSONObject(i);
                                ListKota.add(obj.getString("type") + " " + obj.getString("city_name"));
                                HashKota.put(i+1, obj.getInt("city_id"));
                                AdapterKota.notifyDataSetChanged();
                                if ((tempKota != null) && (tempKota.equals(obj.getString("type") + " " + obj.getString("city_name")))){
                                    spnKota.setSelection(i+1);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Loading.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Loading.dismiss();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            EthicaApplication.getInstance().addToRequestQueue(stringRequest);
        }else{

            ListKota.clear();
            HashKota.clear();
            ListKota.add("Pilih");
            HashKota.put(0, 0);
        }
    }

    public void setKecamatan(int id_kota){

        ListKecamatan.clear();
        HashKecamatan.clear();
        ListKecamatan.add("Pilih");
        HashKecamatan.put(0, 0);
        spnKecamatan.setSelection(0);
        if (id_kota > 0) {
            String url = Routes.url_get_kecamatan + "&city=" + id_kota;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray ArrResults = new JSONObject(response).getJSONObject("rajaongkir").getJSONArray("results");
                                for (int i = 0; i < ArrResults.length(); i++) {
                                    try {
                                        JSONObject obj = ArrResults.getJSONObject(i);
                                        ListKecamatan.add(obj.getString("subdistrict_name"));
                                        HashKecamatan.put(i + 1, obj.getInt("subdistrict_id"));
                                        AdapterKecamatan.notifyDataSetChanged();
                                        if ((tempKota != null) && (tempKecamatan.equals(obj.getString("subdistrict_name")))){
                                            spnKecamatan.setSelection(i+1);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            EthicaApplication.getInstance().addToRequestQueue(stringRequest);
        }
    }


    private void EventClass(){
        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idProvinsi = HashProvinsi.get(position);
                idKota = 0;
                idKecamatan = 0;
                provinsi = ListProvinsi.get(position).toString();
                kota = "";
                kecamatan = "";

                ListKota.clear();
                HashKota.clear();
                ListKecamatan.clear();
                HashKecamatan.clear();
                SetKota(idProvinsi);
                AdapterKota.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idKota = HashKota.get(position);
                idKecamatan = 0;
                kota = ListKota.get(position).toString();
                ListKecamatan.clear();
                HashKecamatan.clear();
                setKecamatan(idKota);
                AdapterKecamatan.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spnKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idKecamatan = HashKecamatan.get(position);
                kecamatan = ListKecamatan.get(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnTerapkan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    final int id_provinsi = idProvinsi;
                    final int id_kota = idKota;
                    final int id_kecamatan = idKecamatan;
                    String url;
                    url = Routes.url_update_cart_master_alamat_kirim + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                    StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jObj = new JSONObject(response);

                                if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                    inform(InputAlamatKirimCartActivity.this, MSG_HARUS_UPDATE, "");
                                    return;
                                }


                                Intent intent = getIntent();
                                intent.putExtra("id_provinsi", idProvinsi);
                                intent.putExtra("id_kota", idKota);
                                intent.putExtra("id_kecamatan", idKecamatan);
                                intent.putExtra("kelurahan", txtKelurahan.getText().toString());
                                intent.putExtra("alamat_kirim_lengkap", txtInputAlamat.getText().toString());
                                intent.putExtra("no_telepon_kirim", txtInputNoTelponPengirim.getText().toString());
                                intent.putExtra("nama_kirim", txtInputNamaPengirim.getText().toString());
                                intent.putExtra("provinsi", provinsi);
                                intent.putExtra("kota", kota);
                                intent.putExtra("kecamatan", kecamatan);

                                setResult(RESULT_OK, intent);
                                finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(InputAlamatKirimCartActivity.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(InputAlamatKirimCartActivity.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            String alamatKirim =
                                    "Kepada \n"+
                                            "Yth : "+ txtInputNamaPengirim.getText().toString()+"\n"+
                                            "Alamat : "+txtInputAlamat.getText().toString() +", "+
                                            txtKelurahan.getText().toString() +", "+
                                            kecamatan +", "+
                                            kota +", "+
                                            provinsi +", "+
                                            "No. HP : "+txtInputNoTelponPengirim.getText().toString();

                            params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                            params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());

                            params.put("alamat_kirim", alamatKirim);
                            params.put("nama_kirim", txtInputNamaPengirim.getText().toString());
                            params.put("id_provinsi", String.valueOf(id_provinsi));
                            params.put("id_kota", String.valueOf(id_kota));
                            params.put("id_kecamatan", String.valueOf(id_kecamatan));
                            params.put("provinsi", provinsi);
                            params.put("kota", kota);
                            params.put("kecamatan", kecamatan);
                            params.put("kelurahan", txtKelurahan.getText().toString());
                            params.put("alamat_kirim_lengkap", txtInputAlamat.getText().toString());
                            params.put("no_telepon_kirim", txtInputNoTelponPengirim.getText().toString());
                            params.put("tipe_apps", TIPE_APPS_ANDROID);
                            return params;
                        }
                    };

                    EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);

                }
            }
        });

        cLPilihAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(InputAlamatKirimCartActivity.this, HistoriAlamatKirim.class);
                startActivityForResult(s, 1);
            }
        });
    }

    private boolean isValid(){
        if (txtInputNamaPengirim.getText().toString().equals("")){
            Toast.makeText(InputAlamatKirimCartActivity.this,"Nama penerima belum diisi", Toast.LENGTH_SHORT).show();
            txtInputNamaPengirim.requestFocus();
            return false;
        }
        if (txtInputAlamat.getText().toString().equals("")){
            Toast.makeText(InputAlamatKirimCartActivity.this,"Alamat lengkap belum diisi", Toast.LENGTH_SHORT).show();
            txtInputAlamat.requestFocus();
            return false;
        }
        if (spnProvinsi.getSelectedItemPosition() == 0){
            Toast.makeText(InputAlamatKirimCartActivity.this,"Provinsi belum dipilih", Toast.LENGTH_SHORT).show();
            spnProvinsi.requestFocus();
            return false;
        }
        if (spnKota.getSelectedItemPosition() == 0){
            Toast.makeText(InputAlamatKirimCartActivity.this,"Kota belum dipilih", Toast.LENGTH_SHORT).show();
            spnKota.requestFocus();
            return false;
        }
        if (spnKecamatan.getSelectedItemPosition() == 0){
            Toast.makeText(InputAlamatKirimCartActivity.this,"Kecamatan belum dipilih", Toast.LENGTH_SHORT).show();
            spnKecamatan.requestFocus();
            return false;
        }
        if (txtKelurahan.getText().toString().equals("")){
            Toast.makeText(InputAlamatKirimCartActivity.this,"Kelurahan belum diisi", Toast.LENGTH_SHORT).show();
            txtKelurahan.requestFocus();
            return false;
        }
        if (txtInputNoTelponPengirim.getText().toString().equals("")){
            Toast.makeText(InputAlamatKirimCartActivity.this,"No. HP belum dipilih", Toast.LENGTH_SHORT).show();
            txtInputNoTelponPengirim.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extra = data.getExtras();
                this.kecamatan      = extra.getString("KECAMATAN");
                this.kota           = extra.getString("KOTA");
                this.provinsi       = extra.getString("PROVINSI");
                this.idKecamatan    = extra.getInt("ID_KECAMATAN");
                this.idKota         = extra.getInt("ID_KOTA");
                this.idProvinsi     = extra.getInt("ID_PROVINSI");

                this.tempProvinsi   = provinsi;
                this.tempKota       = kota;
                this.tempKecamatan  = kecamatan;
                this.txtKelurahan.setText(extra.getString("KELURAHAN"));
                this.txtInputAlamat.setText(extra.getString("ALAMAT_KIRIM_LENGKAP"));
                this.txtInputNamaPengirim.setText(extra.getString("NAMA_KIRIM"));
                this.txtInputNoTelponPengirim.setText(extra.getString("NO_TELPON_KIRIM"));
                SetProvinsi();
                SetKota(idProvinsi);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return false;
    }

}
