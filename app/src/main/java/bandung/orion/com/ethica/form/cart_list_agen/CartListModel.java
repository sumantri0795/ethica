package bandung.orion.com.ethica.form.cart_list_agen;

public class CartListModel {
    int seq;
    private String nama, gambar;
    private int jumlah;
    private Double stok, harga;
    private String keterangan;

    public CartListModel(int seq, String nama, String gambar, int jumlah, Double stok, Double harga) {
        this.seq = seq;
        this.nama = nama;
        this.gambar = gambar;
        this.jumlah = jumlah;
        this.stok = stok;
        this.harga = harga;
        this.keterangan = "";
    }

    public CartListModel() {
        this.seq = 0;
        this.nama = "";
        this.gambar = "";
        this.jumlah = 0;
        this.stok = 0.0;
        this.harga = 0.0;
        this.keterangan = "";
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public Double getStok() {
        return stok;
    }

    public void setStok(Double stok) {
        this.stok = stok;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }
    
    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    
    
    
    
    
}
