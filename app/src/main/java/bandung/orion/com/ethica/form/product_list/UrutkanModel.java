package bandung.orion.com.ethica.form.product_list;

public class UrutkanModel {
    private String nama;
    private boolean pilih;

    public UrutkanModel(String nama, boolean pilih) {
        this.nama = nama;
        this.pilih = pilih;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setPilih(boolean pilih) {
        this.pilih = pilih;
    }

    public String getNama() {
        return nama;
    }

    public boolean isPilih() {
        return pilih;
    }
}
