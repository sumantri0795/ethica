package bandung.orion.com.ethica.form.cart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.utility.FungsiGeneral;
import bandung.orion.com.ethica.utility.Routes;

import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.KONDISI_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.MSG_HARUS_UPDATE;
import static bandung.orion.com.ethica.utility.JConst.TIPE_APPS_ANDROID;

public class InputAlamatCart extends AppCompatActivity {
    private Button btnSimpan;
    private TextView txtInputNamaPengirim, txtInputAlamat, txtInputNoTelponPengirim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_alamat_cart);
        CreateView();
        InitClass();
        EventClass();
        LoadData();
    }

    private void CreateView() {
        this.btnSimpan                = (Button) findViewById(R.id.btnSimpan);
        this.txtInputNamaPengirim     = (TextView) findViewById(R.id.txtInputNamaPengirim);
        this.txtInputAlamat           = (TextView) findViewById(R.id.txtInputAlamat);
        this.txtInputNoTelponPengirim = (TextView) findViewById(R.id.txtInputNoTelponPengirim);
    }

    private void InitClass() {
        this.setTitle("Alamat Pengirim");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void EventClass() {
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Isvalid()){
                    final String alamatPengirim = txtInputNamaPengirim.getText().toString() +"\n"+
                                                  txtInputAlamat.getText().toString() +"\n"+
                                                  "No. HP : "+txtInputNoTelponPengirim.getText().toString();

                    String url;
                    url = Routes.url_update_cart_master_alamat_pengirim + "?key=" + EthicaApplication.getInstance().ApiKey+"&versi="+EthicaApplication.getInstance().getVersionCode();
                    StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jObj = new JSONObject(response);

                                if (jObj.getString("status").equals(KONDISI_HARUS_UPDATE)){
                                    inform(InputAlamatCart.this, MSG_HARUS_UPDATE, "");
                                    return;
                                }

                                Intent intent = getIntent();
                                intent.putExtra("ALAMAT_PENGIRIM", alamatPengirim);
                                intent.putExtra("NAMA_PENGIRIM", txtInputNamaPengirim.getText().toString());
                                intent.putExtra("ALAMAT_PENGIRIM_LENGKAP", txtInputAlamat.getText().toString());
                                intent.putExtra("NO_TELEPON_PENGIRIM", txtInputNoTelponPengirim.getText().toString());

                                setResult(RESULT_OK, intent);
                                finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(InputAlamatCart.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(InputAlamatCart.this, "Tidak tersambung ke server, pastikan ada koneksi internet", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("customer_seq", String.valueOf(EthicaApplication.getInstance().getCustomerSeqGlobal()));
                            params.put("alamat_pengirim", alamatPengirim);
                            params.put("user_id", EthicaApplication.getInstance().getUserIdGlobal());
                            params.put("nama_pengirim", txtInputNamaPengirim.getText().toString());
                            params.put("alamat_pengirim_lengkap", txtInputAlamat.getText().toString());
                            params.put("no_telepon_pengirim", txtInputNoTelponPengirim.getText().toString());
                            params.put("tipe_apps", TIPE_APPS_ANDROID);
                            return params;
                        }
                    };
                    EthicaApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
                }
            }
        });
    }

    private void LoadData() {
        Bundle extra   = this.getIntent().getExtras();
        txtInputNamaPengirim.setText(extra.getString("NAMA_PENGIRIM"));
        txtInputAlamat.setText(extra.getString("ALAMAT_PENGIRIM"));
        txtInputNoTelponPengirim.setText(extra.getString("NO_TELPON_PENGIRIM"));

    }

    private boolean Isvalid() {
        if (txtInputNamaPengirim.getText().toString().trim().equals("")) {
            Toast.makeText(InputAlamatCart.this, "Nama pengirim belum diisi", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (txtInputAlamat.getText().toString().trim().equals("")) {
            Toast.makeText(InputAlamatCart.this, "Alamat lengkap belum diisi", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (txtInputNoTelponPengirim.getText().toString().trim().equals("")) {
            Toast.makeText(InputAlamatCart.this, "No. Hp belum diisi", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return false;
    }
}
