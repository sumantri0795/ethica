package bandung.orion.com.ethica.utility;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bandung.orion.com.ethica.EthicaApplication;
import bandung.orion.com.ethica.form.ZoomImage.ZoomImageActivity;

import static bandung.orion.com.ethica.EthicaApplication.fmt;
import static bandung.orion.com.ethica.utility.FungsiGeneral.inform;
import static bandung.orion.com.ethica.utility.JConst.MSG_NOT_CONNECTION;

public class JEngine {

    public static void GetInfoDiskon (final Context context, long barang_seq){
        String url = Routes.url_load_diskon_barang + "/"+barang_seq;
        JsonArrayRequest jArr = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                String keterangan = "";
                for (int i = 0; i < response.length(); i++) {
                    try { JSONObject obj = response.getJSONObject(i);
                        keterangan += "Beli "+fmt.format(obj.getDouble("range_mulai"))+" - "+fmt.format(obj.getDouble("range_sampai")) +
                                " diskon "+obj.getString("diskon_pct")+"%";
                        if (i < response.length()-1){
                            keterangan += "\n";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    }
                }
                inform(context, keterangan, "Keterangan Diskon");
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                Toast.makeText(context, MSG_NOT_CONNECTION, Toast.LENGTH_SHORT).show();
            }
        });
        EthicaApplication.getInstance().addToRequestQueue(jArr);


    }
}
