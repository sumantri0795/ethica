package bandung.orion.com.ethica.form.profile.profile_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import bandung.orion.com.ethica.R;
import bandung.orion.com.ethica.form.profile.login.Login;

public class profile_login extends AppCompatActivity {
    private TextView txtDeskripsi;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_login);
        CreateView();
        InitClass();
        EventClass();
    }

    private void CreateView(){
        txtDeskripsi = (TextView) findViewById(R.id.txtDeskripsi);
        btnLogin = (Button) findViewById(R.id.btnLogin);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void InitClass(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarAct);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        txtDeskripsi.setText("Selamat silahkan login terlebih dahulu");
    }

    private void EventClass(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent s = new Intent(profile_login.this, Login.class);
                startActivityForResult(s,1);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                finish();
            }
        }
    }

}
