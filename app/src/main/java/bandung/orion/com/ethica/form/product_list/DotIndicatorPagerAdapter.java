package bandung.orion.com.ethica.form.product_list;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bandung.orion.com.ethica.R;

public class DotIndicatorPagerAdapter extends PagerAdapter {
  private List<View> viewList;

  DotIndicatorPagerAdapter() {
    this.viewList = new ArrayList<>();
  }


  @NonNull @Override public Object instantiateItem(@NonNull ViewGroup container, int position) {
    View item = viewList.get(position);
    container.addView(item);
    return item;
  }

  @Override public int getCount() {
    return viewList.size();
  }

  @Override public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
    return view == object;
  }

  @Override public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
    container.removeView((View) object);
  }

  void setData(@Nullable List<View> list) {
    this.viewList.clear();
    if (list != null && !list.isEmpty()) {
      this.viewList.addAll(list);
    }

    notifyDataSetChanged();
  }


  @NonNull
  List<View> getData() {
    if (viewList == null) {
      viewList = new ArrayList<>();
    }

    return viewList;
  }
}
